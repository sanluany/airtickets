package com.epam.airtickets.entity;

/**
 * An object that implements the identifiable interface contains unique
 * ID field among the other instances of its class when represents database entry.
 * <p>
 * <p> The interface provides methods of getting and setting ID field.
 * The uniqueness of ID is guarantied by database. ID can be empty in cases
 * when object doesn't represent the database entry.
 *
 * @author Dmitriy Boychuk
 */
public interface Identifiable {
    Integer getId();

    void setId(Integer id);
}
