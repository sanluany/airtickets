package com.epam.airtickets.entity.ticket;

import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;

import java.time.LocalDate;

public class Ticket implements Identifiable {

    private Integer id;
    private final Timetable timetable;
    private final TravelClass travelClass;
    private final String passengerFirstName;
    private final String passengerLastName;
    private final String passengerPassportNumber;
    private final LocalDate passengerDateOfBirth;

    public Ticket(Integer id, Timetable timetable, TravelClass travelClass, String passengerFirstName,
                  String passengerLastName, String passengerPassportNumber, LocalDate passengerDateOfBirth) {
        this.id = id;
        this.timetable = timetable;
        this.travelClass = travelClass;
        this.passengerFirstName = passengerFirstName;
        this.passengerLastName = passengerLastName;
        this.passengerPassportNumber = passengerPassportNumber;
        this.passengerDateOfBirth = passengerDateOfBirth;
    }

    public Ticket(Timetable timetable, TravelClass travelClass, String passengerFirstName, String passengerLastName,
                  String passengerPassportNumber, LocalDate passengerDateOfBirth) {
        this.timetable = timetable;
        this.travelClass = travelClass;
        this.passengerFirstName = passengerFirstName;
        this.passengerLastName = passengerLastName;
        this.passengerPassportNumber = passengerPassportNumber;
        this.passengerDateOfBirth = passengerDateOfBirth;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public TravelClass getTravelClass() {
        return travelClass;
    }

    public String getPassengerFirstName() {
        return passengerFirstName;
    }

    public String getPassengerLastName() {
        return passengerLastName;
    }

    public String getPassengerPassportNumber() {
        return passengerPassportNumber;
    }

    public LocalDate getPassengerDateOfBirth() {
        return passengerDateOfBirth;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        Ticket ticket = (Ticket) object;
        return this.id.equals(ticket.id) &&
                this.timetable.equals(ticket.timetable) &&
                this.travelClass.equals(ticket.travelClass) &&
                this.passengerFirstName.equals(ticket.passengerFirstName) &&
                this.passengerLastName.equals(ticket.passengerLastName) &&
                this.passengerPassportNumber.equals(ticket.passengerPassportNumber) &&
                this.passengerDateOfBirth.equals(ticket.passengerDateOfBirth);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (timetable == null ? 0 : timetable.hashCode());
        hash = 31 * hash + (travelClass == null ? 0 : travelClass.hashCode());
        hash = 31 * hash + (passengerFirstName == null ? 0 : passengerFirstName.hashCode());
        hash = 31 * hash + (passengerLastName == null ? 0 : passengerLastName.hashCode());
        hash = 31 * hash + (passengerPassportNumber == null ? 0 : passengerPassportNumber.hashCode());
        hash = 31 * hash + (passengerDateOfBirth == null ? 0 : passengerDateOfBirth.hashCode());
        return hash;
    }
}