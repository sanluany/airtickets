package com.epam.airtickets.entity.order;

import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.user.User;

import java.time.LocalDate;

public class Order implements Identifiable {

    private Integer id;
    private final User user;
    private final LocalDate orderDate;

    public Order(User user, LocalDate orderDate) {
        this.user = user;
        this.orderDate = orderDate;
    }

    public Order(Integer id, User user, LocalDate orderDate) {
        this.id = id;
        this.user = user;
        this.orderDate = orderDate;
    }

    @Override
    public Integer getId() {
        return id;
    }


    public User getUser() {
        return user;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        Order order = (Order) object;
        return this.id.equals(order.id) &&
                this.user.equals(order.user) &&
                this.orderDate.equals(order.orderDate);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (user == null ? 0 : user.hashCode());
        hash = 31 * hash + (orderDate == null ? 0 : orderDate.hashCode());
        return hash;
    }
}
