package com.epam.airtickets.entity.user;

public enum UserRole {
    USER("User"), ADMIN("Admin");

    private final String name;

    UserRole(String name) {
        this.name = name;
    }

    public static UserRole getByName(String name) {
        for (UserRole userRole : UserRole.values()) {
            if (userRole.name.equalsIgnoreCase(name)) {
                return userRole;
            }
        }
        throw new IllegalArgumentException("No constant with name " + name + " found");
    }

}
