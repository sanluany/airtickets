package com.epam.airtickets.entity.user;

import com.epam.airtickets.entity.Identifiable;

import java.math.BigDecimal;

public class User implements Identifiable {

    private Integer id;
    private final String username;
    private final String password;
    private final UserRole userRole;
    private final BigDecimal balance;

    public User(Integer id, String username, String password, UserRole userRole, BigDecimal balance) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.balance = balance;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        User user = (User) object;
        return this.id.equals(user.id) &&
                this.username.equals(user.username) &&
                this.password.equals(user.password) &&
                this.userRole.equals(user.userRole) &&
                this.balance.equals(user.balance);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (username == null ? 0 : username.hashCode());
        hash = 31 * hash + (password == null ? 0 : password.hashCode());
        hash = 31 * hash + (userRole == null ? 0 : userRole.hashCode());
        hash = 31 * hash + (balance == null ? 0 : balance.hashCode());
        return hash;
    }
}
