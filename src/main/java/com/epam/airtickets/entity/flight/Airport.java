package com.epam.airtickets.entity.flight;

import com.epam.airtickets.entity.Identifiable;

import java.io.Serializable;

public class Airport implements Identifiable, Serializable {
    private Integer id;
    private final String code;
    private final String name;
    private final City city;

    public Airport(Integer id, String code, String name, City city) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.city = city;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public City getCity() {
        return city;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        Airport airport = (Airport) object;
        return this.id.equals(airport.id) &&
                this.code.equals(airport.code) &&
                this.name.equals(airport.name) &&
                this.city.equals(airport.city);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (code == null ? 0 : code.hashCode());
        hash = 31 * hash + (name == null ? 0 : name.hashCode());
        hash = 31 * hash + (city == null ? 0 : city.hashCode());
        return hash;
    }

    @Override
    public String toString() {
        return code + "(" + name + ")";
    }
}
