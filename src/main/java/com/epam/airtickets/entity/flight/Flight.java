package com.epam.airtickets.entity.flight;

import com.epam.airtickets.entity.Identifiable;

public class Flight implements Identifiable {

    private Integer id;
    private final String number;
    private final Airport originAirport;
    private final Airport destinationAirport;
    private final Integer economyClassTicketsAmount;
    private final Integer businessClassTicketsAmount;
    private final Integer firstClassTicketsAmount;
    private Boolean isDeleted;

    public Flight(String number, Airport originAirport, Airport destinationAirport, Integer economyClassTicketsAmount,
                  Integer businessClassTicketsAmount, Integer firstClassTicketsAmount) {
        this.number = number;
        this.originAirport = originAirport;
        this.destinationAirport = destinationAirport;
        this.economyClassTicketsAmount = economyClassTicketsAmount;
        this.businessClassTicketsAmount = businessClassTicketsAmount;
        this.firstClassTicketsAmount = firstClassTicketsAmount;
    }

    public Flight(Integer id, String number, Airport originAirport, Airport destinationAirport,
                  Integer economyClassTicketsAmount, Integer businessClassTicketsAmount, Integer firstClassTicketsAmount, Boolean isDeleted) {
        this.id = id;
        this.number = number;
        this.originAirport = originAirport;
        this.destinationAirport = destinationAirport;
        this.economyClassTicketsAmount = economyClassTicketsAmount;
        this.businessClassTicketsAmount = businessClassTicketsAmount;
        this.firstClassTicketsAmount = firstClassTicketsAmount;
        this.isDeleted = isDeleted;
    }

    @Override
    public Integer getId() {
        return id;
    }


    public String getNumber() {
        return number;
    }

    public Airport getOriginAirport() {
        return originAirport;
    }

    public Airport getDestinationAirport() {
        return destinationAirport;
    }

    public Integer getEconomyClassTicketsAmount() {
        return economyClassTicketsAmount;
    }

    public Integer getBusinessClassTicketsAmount() {
        return businessClassTicketsAmount;
    }

    public Integer getFirstClassTicketsAmount() {
        return firstClassTicketsAmount;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        Flight flight = (Flight) object;
        return this.id.equals(flight.id) &&
                this.number.equals(flight.number) &&
                this.originAirport.equals(flight.originAirport) &&
                this.destinationAirport.equals(flight.destinationAirport) &&
                this.economyClassTicketsAmount.equals(flight.economyClassTicketsAmount) &&
                this.businessClassTicketsAmount.equals(flight.businessClassTicketsAmount) &&
                this.firstClassTicketsAmount.equals(flight.firstClassTicketsAmount) &&
                this.isDeleted.equals(flight.isDeleted);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (number == null ? 0 : number.hashCode());
        hash = 31 * hash + (originAirport == null ? 0 : originAirport.hashCode());
        hash = 31 * hash + (destinationAirport == null ? 0 : destinationAirport.hashCode());
        hash = 31 * hash + (economyClassTicketsAmount == null ? 0 : economyClassTicketsAmount.hashCode());
        hash = 31 * hash + (businessClassTicketsAmount == null ? 0 : businessClassTicketsAmount.hashCode());
        hash = 31 * hash + (firstClassTicketsAmount == null ? 0 : firstClassTicketsAmount.hashCode());
        hash = 31 * hash + (isDeleted == null ? 0 : isDeleted.hashCode());
        return hash;
    }


    @Override
    public void setId(Integer id) {
        this.id = id;
    }
}
