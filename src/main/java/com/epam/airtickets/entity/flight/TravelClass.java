package com.epam.airtickets.entity.flight;

public enum TravelClass {
    ECONOMY("ECONOMY"), BUSINESS("Business"), FIRST("First");

    private final String name;

    TravelClass(String name) {
        this.name = name;
    }

    public static TravelClass getByName(String name) {
        for (TravelClass travelClass : TravelClass.values()) {
            if (travelClass.name.equalsIgnoreCase(name)) {
                return travelClass;
            }
        }
        throw new IllegalArgumentException("No constant with name " + name + " found");
    }
}
