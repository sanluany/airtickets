package com.epam.airtickets.entity.flight;

import com.epam.airtickets.entity.Identifiable;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FlightPrice implements Identifiable {

    private Integer id;
    private final Flight flight;
    private final TravelClass travelClass;
    private final BigDecimal price;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private Boolean isDeleted;

    public FlightPrice(Integer id, Flight flight, TravelClass travelClass, BigDecimal price, LocalDate startDate, LocalDate endDate, Boolean isDeleted) {
        this.id = id;
        this.flight = flight;
        this.travelClass = travelClass;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isDeleted = isDeleted;
    }

    public FlightPrice(Flight flight, TravelClass travelClass, BigDecimal price, LocalDate startDate, LocalDate endDate) {
        this.flight = flight;
        this.travelClass = travelClass;
        this.price = price;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Flight getFlight() {
        return flight;
    }

    public TravelClass getTravelClass() {
        return travelClass;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        FlightPrice flightPrice = (FlightPrice) object;
        return this.id.equals(flightPrice.id) &&
                this.flight.equals(flightPrice.flight) &&
                this.travelClass.equals(flightPrice.travelClass) &&
                this.price.equals(flightPrice.price) &&
                this.startDate.equals(flightPrice.startDate) &&
                this.endDate.equals(flightPrice.endDate) &&
                this.isDeleted.equals(flightPrice.isDeleted);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (flight == null ? 0 : flight.hashCode());
        hash = 31 * hash + (travelClass == null ? 0 : travelClass.hashCode());
        hash = 31 * hash + (price == null ? 0 : price.hashCode());
        hash = 31 * hash + (startDate == null ? 0 : startDate.hashCode());
        hash = 31 * hash + (endDate == null ? 0 : endDate.hashCode());
        hash = 31 * hash + (isDeleted == null ? 0 : isDeleted.hashCode());
        return hash;
    }
}
