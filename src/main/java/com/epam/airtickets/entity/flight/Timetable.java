package com.epam.airtickets.entity.flight;

import com.epam.airtickets.entity.Identifiable;

import java.time.LocalDateTime;

public class Timetable implements Identifiable {
    private Integer id;
    private final Flight flight;
    private final LocalDateTime departureDate;
    private final LocalDateTime arrivalDate;
    private Boolean isDeleted;

    public Timetable(Integer id, Flight flight, LocalDateTime departureDate, LocalDateTime arrivalDate, Boolean isDeleted) {
        this.id = id;
        this.flight = flight;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.isDeleted = isDeleted;
    }

    public Timetable(Flight flight, LocalDateTime departureDate, LocalDateTime arrivalDate) {
        this.flight = flight;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Flight getFlight() {
        return flight;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public LocalDateTime getArrivalDate() {
        return arrivalDate;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        Timetable timetable = (Timetable) object;
        return this.id.equals(timetable.id) &&
                this.flight.equals(timetable.flight) &&
                this.departureDate.equals(timetable.departureDate) &&
                this.arrivalDate.equals(timetable.arrivalDate) &&
                this.isDeleted.equals(timetable.isDeleted);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (flight == null ? 0 : flight.hashCode());
        hash = 31 * hash + (departureDate == null ? 0 : departureDate.hashCode());
        hash = 31 * hash + (arrivalDate == null ? 0 : arrivalDate.hashCode());
        hash = 31 * hash + (isDeleted == null ? 0 : isDeleted.hashCode());
        return hash;
    }
}
