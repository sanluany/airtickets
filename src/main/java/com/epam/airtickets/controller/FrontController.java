package com.epam.airtickets.controller;

import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.factory.CommandFactory;
import com.epam.airtickets.command.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FrontController extends HttpServlet {
    private final static Logger logger = LogManager.getFormatterLogger(FrontController.class);

    private final static String PREFIX = "WEB-INF/jsp/";
    private final static String POSTFIX = ".jsp";
    private final static String ERROR_PAGE = "/error";
    private final static String COMMAND = "command";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String viewName;
        try {
            viewName = getViewName(request, response);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(), e);
            viewName = ERROR_PAGE;
        }
        String path = PREFIX + viewName + POSTFIX;
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(path);
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String viewName;
        try {
            viewName = getViewName(request, response);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(), e);
            viewName = ERROR_PAGE;
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(viewName);
            requestDispatcher.forward(request, response);
        }
        response.sendRedirect(viewName);
    }

    private String getViewName(HttpServletRequest request, HttpServletResponse response) throws ServiceException {
        String commandName = request.getParameter(COMMAND);
        if (commandName == null) {
            commandName = "";
        }
        CommandFactory commandFactory = new CommandFactory();
        Command command = commandFactory.getCommandByName(commandName);
        return command.execute(request);
    }
}



