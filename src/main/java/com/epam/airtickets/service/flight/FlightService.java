package com.epam.airtickets.service.flight;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;

public interface FlightService {
    int save(Flight flight) throws ServiceException;
    Flight getById(int id) throws ServiceException;
    List<Flight> getAll() throws ServiceException;
    int delete(Flight flight) throws ServiceException;
    int recover(Flight flight) throws ServiceException;
    Flight create(List<Airport> airports,String number,String airportOfOriginIdAsString,
                  String destinationAirportIdAsString, String economyClassTicketsAmountAsString,
                  String businessClassTicketsAmountAsString, String firstClassTicketsAmountAsString);
}
