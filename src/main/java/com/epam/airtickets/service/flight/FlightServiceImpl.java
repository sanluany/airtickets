package com.epam.airtickets.service.flight;

import com.epam.airtickets.builder.flight.FlightBuilder;
import com.epam.airtickets.builder.flight.FlightBuilderImpl;
import com.epam.airtickets.dao.flight.FlightDao;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.dao.flight.FlightDaoImpl;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.utils.IdentifiableHelper;

import java.util.List;

public class FlightServiceImpl implements FlightService {

    @Override
    public int save(Flight flight) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightDao flightDao = daoFactory.getFlightDao();
            result = flightDao.save(flight);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public Flight getById(int id) throws ServiceException {
        Flight flight;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightDaoImpl flightDaoImpl = daoFactory.getFlightDao();
            flight = flightDaoImpl.getById(id);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return flight;
    }

    @Override
    public List<Flight> getAll() throws ServiceException {
        List<Flight> flights;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightDaoImpl flightDaoImpl = daoFactory.getFlightDao();
            flights = flightDaoImpl.getAll();
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return flights;
    }

    @Override
    public int delete(Flight flight) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightDaoImpl flightDaoImpl = daoFactory.getFlightDao();
            result = flightDaoImpl.delete(flight);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public int recover(Flight flight) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightDaoImpl flightDaoImpl = daoFactory.getFlightDao();
            result = flightDaoImpl.recover(flight);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public Flight create(List<Airport> airports, String number, String airportOfOriginIdAsString,
                         String destinationAirportIdAsString, String economyClassTicketsAmountAsString,
                         String businessClassTicketsAmountAsString, String firstClassTicketsAmountAsString) {
        Integer airportOfOriginId = Integer.valueOf(airportOfOriginIdAsString);
        Integer destinationAirportId = Integer.valueOf(destinationAirportIdAsString);

        Airport airportOfOrigin = (Airport) IdentifiableHelper.getByIdFromList(airports, airportOfOriginId);
        Airport destinationAirport = (Airport) IdentifiableHelper.getByIdFromList(airports, destinationAirportId);

        Integer economyClassTicketsAmount = Integer.valueOf(economyClassTicketsAmountAsString);
        Integer businessClassTicketsAmount = Integer.valueOf(businessClassTicketsAmountAsString);
        Integer firstClassTicketsAmount = Integer.valueOf(firstClassTicketsAmountAsString);

        FlightBuilder flightBuilder = new FlightBuilderImpl();
        return flightBuilder.build(number, airportOfOrigin, destinationAirport, economyClassTicketsAmount,
                businessClassTicketsAmount, firstClassTicketsAmount);
    }
}
