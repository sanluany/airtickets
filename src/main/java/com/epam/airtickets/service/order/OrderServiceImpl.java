package com.epam.airtickets.service.order;

import com.epam.airtickets.builder.order.OrderBuilder;
import com.epam.airtickets.builder.order.OrderBuilderImpl;
import com.epam.airtickets.dao.order.OrderDaoImpl;
import com.epam.airtickets.dao.ticket.TicketDaoImpl;
import com.epam.airtickets.dao.user.UserDaoImpl;
import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.NotEnoughFreeTicketsException;
import com.epam.airtickets.exception.service.NotEnoughMoneyException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.exception.service.TimetableNotAvailableException;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.service.flightprice.FlightPriceService;
import com.epam.airtickets.service.flightprice.FlightPriceServiceImpl;
import com.epam.airtickets.service.timetable.TimetableService;
import com.epam.airtickets.service.timetable.TimetableServiceImpl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class OrderServiceImpl implements OrderService {

    private final static String EXCEPTION_NOT_AVAILABLE_TIMETABLE = "exception.not.available.timetable";
    private final static String EXCEPTION_NOT_ENOUGH_MONEY = "exception.not.enough.money";
    private final static String EXCEPTION_NOT_ENOUGH_TICKETS = "exception.not.enough.tickets";

    @Override
    public int save(Order order, List<Ticket> tickets, OfferDto offerDto) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        Timetable timetable = offerDto.getTimetable();
        Integer timetableId = timetable.getId();
        try {
            TimetableService timetableService = new TimetableServiceImpl();
            Timetable currentTimetable = timetableService.getById(timetableId);
            checkTimetableAvailability(currentTimetable);// check if timetable is not deleted at the moment

            TicketDaoImpl ticketDaoImpl = daoFactory.getTicketDao();
            checkTicketsAvailability(tickets, ticketDaoImpl);
            BigDecimal orderTotal = getOrderTotal(offerDto, tickets);

            UserDaoImpl userDaoImpl = daoFactory.getUserDao();
            User user = order.getUser();
            writeOffMoneyFromUserBalance(orderTotal, user, userDaoImpl);

            OrderDaoImpl orderDaoImpl = daoFactory.getOrderDao();
            result = orderDaoImpl.save(order);

            addTickets(tickets, ticketDaoImpl, order);

            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public List<Order> getAllByUserId(int userId) throws ServiceException {
        List<Order> orders;
        try (DaoFactory daoFactory = new DaoFactory()) {
            OrderDaoImpl orderDaoImpl = daoFactory.getOrderDao();
            orders = orderDaoImpl.getAllByUserId(userId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return orders;
    }

    private BigDecimal getOrderTotal(OfferDto offerDto, List<Ticket> tickets) {
        BigDecimal total = BigDecimal.ZERO;
        Map<FlightPrice, Integer> flightPricesRemainedTicketsMap = offerDto.getFlightPricesRemainedTicketsMap();
        Set<FlightPrice> flightPrices = flightPricesRemainedTicketsMap.keySet();
        for (Ticket ticket : tickets) {
            BigDecimal ticketPrice = getTicketPrice(flightPrices, ticket);
            total = total.add(ticketPrice);
        }
        return total;
    }

    private BigDecimal getTicketPrice(Set<FlightPrice> flightPrices, Ticket ticket) {
        TravelClass travelClass = ticket.getTravelClass();
        for (FlightPrice flightPrice : flightPrices) {
            if (flightPrice.getTravelClass() == travelClass) {
                return flightPrice.getPrice();
            }
        }
        return null;
    }

    private void checkTimetableAvailability(Timetable timetable) throws TimetableNotAvailableException {
        if (timetable.getDeleted()) {
            throw new TimetableNotAvailableException(EXCEPTION_NOT_AVAILABLE_TIMETABLE);
        }
    }

    private void checkTicketsAvailability(List<Ticket> tickets, TicketDaoImpl ticketDaoImpl) throws DaoException, ServiceException {
        int economyClassTicketsDemand = 0;
        int businessClassTicketsDemand = 0;
        int firstClassTicketsDemand = 0;
        for (Ticket ticket : tickets) {
            TravelClass travelClass = ticket.getTravelClass();
            if (travelClass == TravelClass.ECONOMY) {
                economyClassTicketsDemand++;
            }
            if (travelClass == TravelClass.BUSINESS) {
                businessClassTicketsDemand++;
            }
            if (travelClass == TravelClass.FIRST) {
                firstClassTicketsDemand++;
            }
        }
        Timetable timetable = tickets.get(0).getTimetable();
        Flight flight = timetable.getFlight();

        if (economyClassTicketsDemand > 0) {
            int soldTicketsAmount = ticketDaoImpl.getSoldTicketsAmountByTravelClassAndTimetable(TravelClass.ECONOMY, timetable);
            int planedTicketsAmount = flight.getEconomyClassTicketsAmount();
            isFreeTicketsAmountEnough(planedTicketsAmount, soldTicketsAmount, economyClassTicketsDemand);
        }
        if (businessClassTicketsDemand > 0) {
            int soldTicketsAmount = ticketDaoImpl.getSoldTicketsAmountByTravelClassAndTimetable(TravelClass.BUSINESS, timetable);
            int planedTicketsAmount = flight.getBusinessClassTicketsAmount();
            isFreeTicketsAmountEnough(planedTicketsAmount, soldTicketsAmount, businessClassTicketsDemand);
        }
        if (firstClassTicketsDemand > 0) {
            int soldTicketsAmount = ticketDaoImpl.getSoldTicketsAmountByTravelClassAndTimetable(TravelClass.FIRST, timetable);
            int planedTicketsAmount = flight.getFirstClassTicketsAmount();
            isFreeTicketsAmountEnough(planedTicketsAmount, soldTicketsAmount, firstClassTicketsDemand);
        }
    }

    private void isFreeTicketsAmountEnough(int planedTicketsAmount, int soldTicketsAmount, int demand) throws NotEnoughFreeTicketsException {
        int freeTicketsAmount = planedTicketsAmount - soldTicketsAmount;
        if (freeTicketsAmount < demand) {
            throw new NotEnoughFreeTicketsException(EXCEPTION_NOT_ENOUGH_TICKETS);
        }
    }

    private void writeOffMoneyFromUserBalance(BigDecimal orderTotal, User user, UserDaoImpl userDaoImpl) throws DaoException, NotEnoughMoneyException {
        Integer writeOffResult = userDaoImpl.writeOffMoneyFromUserBalance(orderTotal, user);
        if (writeOffResult == 0) {
            throw new NotEnoughMoneyException(EXCEPTION_NOT_ENOUGH_MONEY);
        }
    }

    private void addTickets(List<Ticket> tickets, TicketDaoImpl ticketDaoImpl, Order order) throws DaoException {
        for (Ticket ticket : tickets) {
            ticketDaoImpl.save(ticket, order);
        }
    }

    @Override
    public Order createOrder(User user) {
        LocalDate localDate = LocalDate.now();
        OrderBuilder orderBuilder = new OrderBuilderImpl();
        return orderBuilder.build(user, localDate);
    }

    @Override
    public Map<Order, BigDecimal> getOrderTotalMap(Map<Order, List<Ticket>> orderTicketsMap) throws ServiceException {
        Map<Order, BigDecimal> orderTotalMap = new HashMap<>();
        FlightPriceService flightPriceService = new FlightPriceServiceImpl();

        for (Map.Entry<Order, List<Ticket>> orderTicketEntry : orderTicketsMap.entrySet()) {
            List<Ticket> tickets = orderTicketEntry.getValue();
            Ticket ticket = tickets.get(0);

            Timetable timetable = ticket.getTimetable();
            Flight flight = timetable.getFlight();

            Integer flightId = flight.getId();
            LocalDateTime departureDate = timetable.getDepartureDate();

            List<FlightPrice> flightPrices = flightPriceService.getByFlightIdAndDepartureDate(flightId, departureDate);

            BigDecimal total = getTotal(tickets, flightPrices);
            Order order = orderTicketEntry.getKey();
            orderTotalMap.put(order, total);
        }
        return orderTotalMap;
    }

    private BigDecimal getTotal(List<Ticket> tickets, List<FlightPrice> flightPrices) {
        BigDecimal total = BigDecimal.ZERO;
        for (Ticket ticket : tickets) {
            TravelClass travelClass = ticket.getTravelClass();
            Optional<FlightPrice> optionalFlightPrice = flightPrices
                    .stream()
                    .filter(price -> price.getTravelClass() == travelClass)
                    .findFirst();
            if (optionalFlightPrice.isPresent()) {
                FlightPrice flightPrice = optionalFlightPrice.get();
                BigDecimal price = flightPrice.getPrice();
                total = total.add(price);
            }
        }
        return total;
    }
}
