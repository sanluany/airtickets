package com.epam.airtickets.service.order;

import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.service.ServiceException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface OrderService {
    int save(Order order, List<Ticket> tickets, OfferDto offerDto) throws ServiceException;
    List<Order> getAllByUserId(int userId) throws ServiceException;
    Order createOrder(User user);
    Map<Order, BigDecimal> getOrderTotalMap(Map<Order, List<Ticket>> orderTicketsMap) throws ServiceException;
}
