package com.epam.airtickets.service.timetable;

import com.epam.airtickets.dto.TimetableDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;

public interface TimetableService {
    int save(List<Timetable> timetables) throws ServiceException;

    Timetable getById(Integer id) throws ServiceException;

    List<Timetable> getAll() throws ServiceException;

    List<Timetable> getAllByCitiesAndDepartureDate(City originCity, City destinationCity,
                                                   String departureDate) throws ServiceException;

    int delete(Timetable timetable) throws ServiceException;

    int recover(Timetable timetable) throws ServiceException;

    List<Timetable> getAllByFlightId(int id) throws ServiceException;

    List<Timetable> getGeneratedTimetable(Flight flight, String firstDateAsString, String lastDateAsString, String departureTimeAsString,
                                          String arrivalTimeAsString, String[] daysOfWeek);

    List<TimetableDto> getTimetableDtoList(List<Timetable> timetables, Flight flight) throws ServiceException;

}
