package com.epam.airtickets.service.timetable;

import com.epam.airtickets.builder.timetable.TimetableBuilder;
import com.epam.airtickets.builder.timetable.TimetableBuilderImpl;
import com.epam.airtickets.dao.timetable.TimetableDao;
import com.epam.airtickets.dto.TimetableDto;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.dao.timetable.TimetableDaoImpl;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.ticket.TicketService;
import com.epam.airtickets.service.ticket.TicketServiceImpl;
import com.epam.airtickets.utils.DateUtils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimetableServiceImpl implements TimetableService {

    private final static int SECONDS_IN_DAY = 86400;

    @Override
    public int save(List<Timetable> timetables) throws ServiceException {
        int result = 0;
        DaoFactory daoFactory = new DaoFactory();
        try {
            TimetableDao timetableDao = daoFactory.getTimetableDao();
            for(Timetable timetable: timetables){
                result = timetableDao.save(timetable);
            }
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public Timetable getById(Integer id) throws ServiceException {
        Timetable timetable;
        DaoFactory daoFactory = new DaoFactory();
        try{
            TimetableDao timetableDao = daoFactory.getTimetableDao();
            timetable = timetableDao.getById(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }finally {
            daoFactory.close();
        }
        return timetable;
    }

    @Override
    public List<Timetable> getAll() throws ServiceException {
        List<Timetable> timetables;
        try (DaoFactory daoFactory = new DaoFactory()) {
            TimetableDaoImpl timetableDaoImpl = daoFactory.getTimetableDao();
            timetables = timetableDaoImpl.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return timetables;
    }

    @Override
    public List<Timetable> getAllByCitiesAndDepartureDate(City originCity, City destinationCity, String departureDate) throws ServiceException {
        Integer originCityId = originCity.getId();
        Integer destinationCityId = destinationCity.getId();
        List<Timetable> timetables;
        try (DaoFactory daoFactory = new DaoFactory()) {
            TimetableDaoImpl timetableDaoImpl = daoFactory.getTimetableDao();
            timetables = timetableDaoImpl.getAllByCitiesAndDepartureDate(originCityId, destinationCityId, departureDate);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return timetables;
    }

    @Override
    public int delete(Timetable timetable) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            TimetableDaoImpl timetableDaoImpl = daoFactory.getTimetableDao();
            result = timetableDaoImpl.delete(timetable);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public int recover(Timetable timetable) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            TimetableDaoImpl timetableDaoImpl = daoFactory.getTimetableDao();
            result = timetableDaoImpl.recover(timetable);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public List<Timetable> getAllByFlightId(int id) throws ServiceException {
        List<Timetable> timetables;
        try (DaoFactory daoFactory = new DaoFactory()) {
            TimetableDaoImpl timetableDaoImpl = daoFactory.getTimetableDao();
            timetables = timetableDaoImpl.getAllByFlightId(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return timetables;
    }

    @Override
    public List<Timetable> getGeneratedTimetable(Flight flight, String firstDateAsString, String lastDateAsString,
                                                 String departureTimeAsString, String arrivalTimeAsString,
                                                 String[] daysOfWeek) {
        List<Timetable> timetables = new ArrayList<>();

        LocalDate firstDate = LocalDate.parse(firstDateAsString);
        LocalDate LastDate = LocalDate.parse(lastDateAsString);
        long daysAmount = ChronoUnit.DAYS.between(firstDate, LastDate);

        LocalTime departureTime = DateUtils.parseTime(departureTimeAsString);
        LocalTime arrivalTime = DateUtils.parseTime(arrivalTimeAsString);

        for (int i = 0; i < daysAmount; i++) {
            LocalDate offsetDate = firstDate.plusDays(i);
            boolean isSuitableDay = isSuitableDay(offsetDate, daysOfWeek);
            if (isSuitableDay) {
                Timetable timetable = getTimetable(flight, offsetDate, departureTime, arrivalTime);
                timetables.add(timetable);
            }
        }
        return timetables;
    }

    private boolean isSuitableDay(LocalDate offsetDate, String[] days) {
        DayOfWeek dayOfWeek = offsetDate.getDayOfWeek();
        int dayIndexInWeek = dayOfWeek.getValue();
        for (String day : days) {
            int dayNumber = Integer.parseInt(day);
            if (dayIndexInWeek == dayNumber) {
                return true;
            }
        }
        return false;
    }

    private Timetable getTimetable(Flight flight, LocalDate offsetDate, LocalTime departureTime, LocalTime arrivalTime) {
        LocalDateTime departureDateTime = getLocalDateTime(offsetDate, departureTime);
        long flightDuration = calculateFlightDuration(departureTime, arrivalTime);
        LocalDateTime arrivalDateTime = departureDateTime.plusSeconds(flightDuration);

        TimetableBuilder timetableBuilder = new TimetableBuilderImpl();
        return timetableBuilder.build(flight, departureDateTime, arrivalDateTime);
    }

    private LocalDateTime getLocalDateTime(LocalDate date, LocalTime time) {
        int departureYear = date.getYear();
        int departureMonth = date.getMonthValue();
        int departureDay = date.getDayOfMonth();
        int departureHour = time.getHour();
        int departureMinute = time.getMinute();
        return LocalDateTime.of(departureYear, departureMonth, departureDay, departureHour, departureMinute);
    }

    private long calculateFlightDuration(LocalTime departureTime, LocalTime arrivalTime) {
        long departureTimeInSecondsOfDay = departureTime.toSecondOfDay();
        long arrivalTimeInSecondsOfDay = arrivalTime.toSecondOfDay();
        if (departureTimeInSecondsOfDay > arrivalTimeInSecondsOfDay) {
            return SECONDS_IN_DAY - departureTimeInSecondsOfDay + arrivalTimeInSecondsOfDay;
        }
        return arrivalTimeInSecondsOfDay - departureTimeInSecondsOfDay;
    }

    @Override
    public List<TimetableDto> getTimetableDtoList(List<Timetable> timetables, Flight flight) throws ServiceException {
        List<TravelClass> travelClasses = new ArrayList<>();

        Integer economyClassTicketsAmount = flight.getEconomyClassTicketsAmount();
        if (economyClassTicketsAmount > 0) {
            travelClasses.add(TravelClass.ECONOMY);
        }

        Integer businessClassTicketsAmount = flight.getBusinessClassTicketsAmount();
        if (businessClassTicketsAmount > 0) {
            travelClasses.add(TravelClass.BUSINESS);
        }

        Integer firstClassTicketsAmount = flight.getFirstClassTicketsAmount();
        if (firstClassTicketsAmount > 0) {
            travelClasses.add(TravelClass.FIRST);
        }

        List<TimetableDto> timetableDtoList = new ArrayList<>();
        for (Timetable timetable : timetables) {
            TimetableDto timetableDto = getTimetableDto( timetable, travelClasses);
            timetableDtoList.add(timetableDto);
        }
        return timetableDtoList;
    }

    private TimetableDto getTimetableDto(Timetable timetable, List<TravelClass> travelClasses) throws ServiceException {
        Map<String, Integer> soldTicketsAmountByTravelClassMap = new HashMap<>();
        TicketService ticketService = new TicketServiceImpl();

        for (TravelClass travelClass : travelClasses) {
            Integer amount = ticketService.getSoldTicketsAmountByTravelClassAndTimetable(travelClass, timetable);
            soldTicketsAmountByTravelClassMap.put(travelClass.name(), amount);
        }
        return new TimetableDto(soldTicketsAmountByTravelClassMap, timetable);
    }
}
