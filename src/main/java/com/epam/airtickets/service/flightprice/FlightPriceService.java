package com.epam.airtickets.service.flightprice;

import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.service.ServiceException;

import java.time.LocalDateTime;
import java.util.List;

public interface FlightPriceService {
    List<FlightPrice> getAllByFlightId(int flightId) throws ServiceException;
    int save(FlightPrice flightPrice) throws ServiceException;
    int delete(FlightPrice flightPrice) throws ServiceException;
    int recover(FlightPrice flightPrice) throws ServiceException;
    List<FlightPrice> getByFlightIdAndDepartureDate(Integer flightId, LocalDateTime departureDate) throws ServiceException;
    FlightPrice create(Flight flight, String travelClassAsString, String priceAsString, String startDateAsString,
                 String endDateAsString);
}
