package com.epam.airtickets.service.flightprice;

import com.epam.airtickets.builder.flightprice.FlightPriceBuilder;
import com.epam.airtickets.builder.flightprice.FlightPriceBuilderImpl;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.dao.flightprice.FlightPriceDaoImpl;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.utils.DateUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class FlightPriceServiceImpl implements FlightPriceService {
    @Override
    public List<FlightPrice> getAllByFlightId(int id) throws ServiceException {
        List<FlightPrice> flightPrices;
        try (DaoFactory daoFactory = new DaoFactory()) {
            FlightPriceDaoImpl flightPriceDaoImpl = daoFactory.getFlightPriceDao();
            flightPrices = flightPriceDaoImpl.getAllByFlightId(id);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return flightPrices;
    }

    @Override
    public int save(FlightPrice flightPrice) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightPriceDaoImpl flightPriceDaoImpl = daoFactory.getFlightPriceDao();
            result = flightPriceDaoImpl.save(flightPrice);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public int delete(FlightPrice flightPrice) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightPriceDaoImpl flightPriceDaoImpl = daoFactory.getFlightPriceDao();
            result = flightPriceDaoImpl.delete(flightPrice);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public int recover(FlightPrice flightPrice) throws ServiceException {
        int result;
        DaoFactory daoFactory = new DaoFactory();
        try {
            FlightPriceDaoImpl flightPriceDaoImpl = daoFactory.getFlightPriceDao();
            result = flightPriceDaoImpl.recover(flightPrice);
            daoFactory.commit();
        } catch (DaoException e) {
            daoFactory.rollback();
            throw new ServiceException(e.getMessage(), e);
        } finally {
            daoFactory.close();
        }
        return result;
    }

    @Override
    public List<FlightPrice> getByFlightIdAndDepartureDate(Integer flightId, LocalDateTime departureDate) throws ServiceException {
        List<FlightPrice> flightPrices;
        try (DaoFactory daoFactory = new DaoFactory()) {
            FlightPriceDaoImpl flightPriceDaoImpl = daoFactory.getFlightPriceDao();
            flightPrices = flightPriceDaoImpl.getByFlightIdAndDepartureDate(flightId, departureDate);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return flightPrices;
    }

    @Override
    public FlightPrice create(Flight flight, String travelClassAsString, String priceAsString, String startDateAsString,
                        String endDateAsString){
        TravelClass travelClass = TravelClass.getByName(travelClassAsString);
        BigDecimal price = new BigDecimal(priceAsString);

        LocalDate startDate = DateUtils.parseDate(startDateAsString);
        LocalDate endDate = DateUtils.parseDate(endDateAsString);

        FlightPriceBuilder flightPriceBuilder = new FlightPriceBuilderImpl();
        return flightPriceBuilder.build(flight,travelClass,price,startDate,endDate);
    }
}
