package com.epam.airtickets.service.airport;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;

public interface AirportService {
    List<Airport> getAll() throws ServiceException;
}
