package com.epam.airtickets.service.airport;

import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;

public class AirportServiceImpl implements AirportService {
    @Override
    public List<Airport> getAll() throws ServiceException {
        List<Airport> airports;
        try (DaoFactory daoFactory = new DaoFactory()) {
            AbstractDao airportDao = daoFactory.getAirportDao();
            airports = (List<Airport>) airportDao.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return airports;
    }
}
