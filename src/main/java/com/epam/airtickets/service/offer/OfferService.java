package com.epam.airtickets.service.offer;

import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;

public interface OfferService {
    List<OfferDto> getOffers(List<City> cities, String originCityIdAsString, String destinationCityIdAsString,
                             String departureDate) throws ServiceException;
}
