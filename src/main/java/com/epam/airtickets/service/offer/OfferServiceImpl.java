package com.epam.airtickets.service.offer;

import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.flightprice.FlightPriceService;
import com.epam.airtickets.service.flightprice.FlightPriceServiceImpl;
import com.epam.airtickets.service.ticket.TicketService;
import com.epam.airtickets.service.ticket.TicketServiceImpl;
import com.epam.airtickets.service.timetable.TimetableService;
import com.epam.airtickets.service.timetable.TimetableServiceImpl;
import com.epam.airtickets.utils.IdentifiableHelper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OfferServiceImpl implements OfferService {

    @Override
    public List<OfferDto> getOffers(List<City> cities, String originCityIdAsString, String destinationCityIdAsString,
                                    String departureDate) throws ServiceException {
        Integer originCityId = Integer.valueOf(originCityIdAsString);
        City originCity = (City) IdentifiableHelper.getByIdFromList(cities, originCityId);

        Integer destinationCityId = Integer.valueOf(destinationCityIdAsString);
        City destinationCity = (City) IdentifiableHelper.getByIdFromList(cities, destinationCityId);

        TimetableService timetableService = new TimetableServiceImpl();
        List<Timetable> timetables = timetableService.getAllByCitiesAndDepartureDate(originCity, destinationCity, departureDate);

        if (timetables == null) {
            return null;
        }

        List<OfferDto> offerDtoList = new ArrayList<>();
        int id = 0;
        for (Timetable timetable : timetables) {
            OfferDto offerDto = getOffer(timetable, id);
            offerDtoList.add(offerDto);
            id++;
        }
        return offerDtoList;
    }

    private OfferDto getOffer(Timetable timetable, int id) throws ServiceException {
        FlightPriceService flightPriceService = new FlightPriceServiceImpl();
        Flight flight = timetable.getFlight();
        Integer flightId = flight.getId();

        LocalDateTime departureDate = timetable.getDepartureDate();
        List<FlightPrice> flightPrices = flightPriceService.getByFlightIdAndDepartureDate(flightId, departureDate);

        if (flightPrices == null) {
            return null;
        }

        TicketService ticketService = new TicketServiceImpl();
        Map<FlightPrice, Integer> flightPricesRemainedTicketsMap = new LinkedHashMap<>();

        for (FlightPrice flightPrice : flightPrices) {
            TravelClass travelClass = flightPrice.getTravelClass();
            Integer soldTicketsAmount = ticketService.getSoldTicketsAmountByTravelClassAndTimetable(travelClass, timetable);

            int plannedTicketsAmount = getPlannedTickets(travelClass, flight);
            Integer remainingTicketsAmount = plannedTicketsAmount - soldTicketsAmount;
            flightPricesRemainedTicketsMap.put(flightPrice, remainingTicketsAmount);
        }
        return new OfferDto(id, timetable, flightPricesRemainedTicketsMap);
    }

    private Integer getPlannedTickets(TravelClass travelClass, Flight flight) {
        switch (travelClass) {
            case ECONOMY: {
                return flight.getEconomyClassTicketsAmount();
            }
            case BUSINESS: {
                return flight.getBusinessClassTicketsAmount();
            }
            case FIRST: {
                return flight.getFirstClassTicketsAmount();
            }
        }
        return null;
    }
}
