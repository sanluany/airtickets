package com.epam.airtickets.service.city;

import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;

public interface CityService {
    List<City> getAll() throws ServiceException;
}
