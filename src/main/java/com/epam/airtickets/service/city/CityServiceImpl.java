package com.epam.airtickets.service.city;

import com.epam.airtickets.dao.city.CityDaoImpl;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.DaoException;

import java.util.List;

public class CityServiceImpl implements CityService {
    @Override
    public List<City> getAll() throws ServiceException {
        List<City> cities;
        try (DaoFactory daoFactory = new DaoFactory()) {
            CityDaoImpl cityDaoImpl = daoFactory.getCityDao();
            cities = cityDaoImpl.getAll();
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return cities;
    }
}
