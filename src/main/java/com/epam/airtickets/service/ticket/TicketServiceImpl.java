package com.epam.airtickets.service.ticket;

import com.epam.airtickets.builder.ticket.TicketBuilder;
import com.epam.airtickets.builder.ticket.TicketBuilderImpl;
import com.epam.airtickets.dao.ticket.TicketDaoImpl;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.utils.DateUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TicketServiceImpl implements TicketService {

    @Override
    public int getSoldTicketsAmountByTravelClassAndTimetable(TravelClass travelClass, Timetable timetable) throws ServiceException {
        int ticketsAmount;
        try (DaoFactory daoFactory = new DaoFactory()) {
            TicketDaoImpl ticketDaoImpl = daoFactory.getTicketDao();
            ticketsAmount = ticketDaoImpl.getSoldTicketsAmountByTravelClassAndTimetable(travelClass, timetable);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return ticketsAmount;
    }

    @Override
    public List<Ticket> getAllByOrderId(Integer orderId) throws ServiceException {
        List<Ticket> tickets;
        try (DaoFactory daoFactory = new DaoFactory()) {
            TicketDaoImpl ticketDaoImpl = daoFactory.getTicketDao();
            tickets = ticketDaoImpl.getAllByOrderId(orderId);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return tickets;
    }

    @Override
    public List<Ticket> getTickets(Timetable timetable, String[] travelClassesAsString, String[] passengerFirstNames,
                                   String[] passengerLastNames, String[] passengerPassportNumbers, String[] passengerDatesOfBirthAsString) {
        List<Ticket> tickets = new ArrayList<>();
        int length = travelClassesAsString.length;
        TicketBuilder ticketBuilder = new TicketBuilderImpl();

        for (int i = 0; i < length; i++) {
            String travelClassAsString = travelClassesAsString[i];
            TravelClass travelClass = TravelClass.getByName(travelClassAsString);

            String passengerFirstName = passengerFirstNames[i];
            String passengerLastName = passengerLastNames[i];
            String passengerPassportNumber = passengerPassportNumbers[i];

            String passengerDateOfBirthAsString = passengerDatesOfBirthAsString[i];
            LocalDate passengerDateOfBirth = DateUtils.parseDate(passengerDateOfBirthAsString);

            Ticket ticket = ticketBuilder.build(timetable, travelClass, passengerFirstName, passengerLastName
                    , passengerPassportNumber, passengerDateOfBirth);
            tickets.add(ticket);
        }
        return tickets;
    }

    @Override
    public Map<Order, List<Ticket>> getOrderTicketsMap(List<Order> orders) throws ServiceException {
        Map<Order, List<Ticket>> orderTicketsMap = new HashMap<>();
        for (Order order : orders) {
            Integer orderId = order.getId();
            List<Ticket> tickets = getAllByOrderId(orderId);
            orderTicketsMap.put(order, tickets);
        }
        return orderTicketsMap;
    }
}
