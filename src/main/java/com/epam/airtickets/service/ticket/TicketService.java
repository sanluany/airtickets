package com.epam.airtickets.service.ticket;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.exception.service.ServiceException;

import java.util.List;
import java.util.Map;

public interface TicketService {
    int getSoldTicketsAmountByTravelClassAndTimetable(TravelClass travelClass, Timetable timetable) throws ServiceException;

    List<Ticket> getAllByOrderId(Integer orderId) throws ServiceException;

    List<Ticket> getTickets(Timetable timetable, String[] travelClassesAsString, String[] passengerFirstNames,
                            String[] passengerLastNames, String[] passengerPassportNumbers, String[] passengerDatesOfBirthAsString);
    Map<Order, List<Ticket>> getOrderTicketsMap(List<Order> orders) throws ServiceException;

}
