package com.epam.airtickets.service.user;

import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.factory.DaoFactory;
import com.epam.airtickets.dao.user.UserDaoImpl;
import com.epam.airtickets.entity.user.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.DatatypeConverter;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;

public class UserServiceImpl implements UserService {
    private final static Logger logger = LogManager.getFormatterLogger(UserServiceImpl.class);
    private final static String MD5 = "MD5";

    @Override
    public User getUserByUsername(String username) throws ServiceException {
        User user;
        try (DaoFactory daoFactory = new DaoFactory()) {
            UserDaoImpl userDaoImpl = daoFactory.getUserDao();
            user = userDaoImpl.getByUsername(username);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return user;
    }

    @Override
    public User logIn(String username, String password) throws ServiceException {
        User user = getUserByUsername(username);
        boolean isValidPassword = false;
        if(user!=null){
            isValidPassword = isValidPassword(user,password);
        }
        if(isValidPassword){
            return user;
        }
        return null;
    }

    private boolean isValidPassword(User user, String providedPassword) {
        String validPassword = user.getPassword();
        boolean isValidPassword = false;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(MD5);
            byte[] passwordAsBytesArray = providedPassword.getBytes();
            messageDigest.update(passwordAsBytesArray);

            byte[] digest = messageDigest.digest();
            String hash = DatatypeConverter.printHexBinary(digest);
            isValidPassword = validPassword.equalsIgnoreCase(hash);
        } catch (NoSuchAlgorithmException e) {
           logger.warn(e.getMessage(),e);
        }
        return isValidPassword;
    }
}
