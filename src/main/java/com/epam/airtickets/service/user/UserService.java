package com.epam.airtickets.service.user;

import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.service.ServiceException;

public interface UserService {
    User getUserByUsername(String username) throws ServiceException;
    User logIn(String username, String password) throws ServiceException;
}
