package com.epam.airtickets.dto;

import com.epam.airtickets.entity.flight.Timetable;

import java.util.Map;

public class TimetableDto {

    private final Map<String, Integer> soldTicketsAmountByTravelClassMap;
    private final Timetable timetable;

    public TimetableDto(Map<String, Integer> soldTicketsAmountByTravelClassMap, Timetable timetable) {
        this.soldTicketsAmountByTravelClassMap = soldTicketsAmountByTravelClassMap;
        this.timetable = timetable;
    }

    public Map<String, Integer> getSoldTicketsAmountByTravelClassMap() {
        return soldTicketsAmountByTravelClassMap;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        TimetableDto timetableDto = (TimetableDto) object;
        return this.soldTicketsAmountByTravelClassMap.equals(timetableDto.soldTicketsAmountByTravelClassMap) &&
                this.timetable.equals(timetableDto.timetable);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (soldTicketsAmountByTravelClassMap == null ? 0 : soldTicketsAmountByTravelClassMap.hashCode());
        hash = 31 * hash + (timetable == null ? 0 : timetable.hashCode());
        return hash;
    }
}
