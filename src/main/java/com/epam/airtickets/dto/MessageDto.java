package com.epam.airtickets.dto;

public class MessageDto {
    private final Boolean isError;
    private final String message;

    public MessageDto(Boolean isError, String message) {
        this.isError = isError;
        this.message = message;
    }

    public Boolean getError() {
        return isError;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        MessageDto messageDto = (MessageDto) object;
        return this.isError.equals(messageDto.isError) && this.message.equals(messageDto.message);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (isError == null ? 0 : isError.hashCode());
        hash = 31 * hash + (message == null ? 0 : message.hashCode());
        return hash;
    }
}

