package com.epam.airtickets.dto;

import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.FlightPrice;

import java.util.Map;

public class OfferDto implements Identifiable {

    private Integer id;
    private final Timetable timetable;
    private final Map<FlightPrice,Integer> flightPricesRemainedTicketsMap;

    public OfferDto(Integer id, Timetable timetable, Map<FlightPrice, Integer> flightPricesRemainedTicketsMap) {
        this.id = id;
        this.timetable = timetable;
        this.flightPricesRemainedTicketsMap = flightPricesRemainedTicketsMap;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }
    public Timetable getTimetable() {
        return timetable;
    }

    public Map<FlightPrice, Integer> getFlightPricesRemainedTicketsMap() {
        return flightPricesRemainedTicketsMap;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (getClass() != object.getClass()) {
            return false;
        }
        OfferDto offerDto = (OfferDto) object;
        return this.id.equals(offerDto.id) &&
                this.timetable.equals(offerDto.timetable)&&
                this.flightPricesRemainedTicketsMap.equals(offerDto.flightPricesRemainedTicketsMap);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (id == null ? 0 : id.hashCode());
        hash = 31 * hash + (timetable == null ? 0 : timetable.hashCode());
        hash = 31 * hash + (flightPricesRemainedTicketsMap == null ? 0 : flightPricesRemainedTicketsMap.hashCode());
        return hash;
    }
}
