package com.epam.airtickets.exception.service;

public class NotEnoughFreeTicketsException extends ServiceException {
    public NotEnoughFreeTicketsException(String message) {
        super(message);
    }
}
