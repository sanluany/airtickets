package com.epam.airtickets.exception.service;

public class ServiceException extends Exception {
    public ServiceException(String message, Exception exception){
        super(message, exception);
    }

    public ServiceException(String message) {
        super(message);
    }
}
