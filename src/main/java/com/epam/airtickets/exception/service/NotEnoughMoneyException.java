package com.epam.airtickets.exception.service;

public class NotEnoughMoneyException extends ServiceException {

    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
