package com.epam.airtickets.exception.service;

public class TimetableNotAvailableException extends ServiceException {

    public TimetableNotAvailableException(String message) {
        super(message);
    }
}
