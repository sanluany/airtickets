package com.epam.airtickets.dao.flight;

import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.handler.resultset.ResultSetHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class FlightDaoImpl extends AbstractDao implements FlightDao {


    private final static String SQL_INSERT_FLIGHT =
            "INSERT INTO flights (number,origin_airport_id,destination_airport_id,economy_class_tickets_amount, " +
                    "business_class_tickets_amount,first_class_tickets_amount) VALUES (?,?,?,?,?,?)";
    private final static String SQL_SELECT_ALL = "SELECT flight.id AS flight_id,\n" +
            "       flight.number AS flight_number,\n" +
            "       origin_airport.id AS origin_airport_id,\n" +
            "       origin_airport.code AS origin_airport_code,\n" +
            "       origin_airport.name AS origin_airport_name,\n" +
            "       origin_city.id AS origin_city_id,\n" +
            "       origin_city.name AS origin_city_name,\n" +
            "       destination_airport.id AS destination_airport_id,\n" +
            "       destination_airport.code AS destination_airport_code,\n" +
            "       destination_airport.name AS destination_airport_name,\n" +
            "       destination_city.id AS destination_city_id,\n" +
            "       destination_city.name AS destination_city_name,\n" +
            "       flight.economy_class_tickets_amount AS flight_economy_class_tickets_amount,\n" +
            "       flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "       flight.first_class_tickets_amount AS flight_first_class_tickets_amount,\n" +
            "       flight.is_deleted AS flight_is_deleted\n" +
            "FROM flights AS flight\n" +
            "INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n";

    private final static String SQL_SELECT_BY_ID = SQL_SELECT_ALL + "WHERE flight.id = ?";

    private final static String SQL_UPDATE_SET_DELETED = "UPDATE flights SET is_deleted = 1 WHERE id = ?";
    private final static String SQL_UPDATE_SET_NOT_DELETED = "UPDATE flights SET is_deleted = 0 WHERE id = ?";

    public FlightDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public int save(Flight flight) throws DaoException {
        setQuery(SQL_INSERT_FLIGHT);
        String number = flight.getNumber();

        Integer originAirportId = flight.getOriginAirport().getId();
        Integer destinationAirportId = flight.getDestinationAirport().getId();

        Integer economyClassTicketsAmount = flight.getEconomyClassTicketsAmount();
        Integer businessClassTicketsAmount = flight.getBusinessClassTicketsAmount();
        Integer firstClassTicketsAmount = flight.getFirstClassTicketsAmount();

        setParameterObjects(number, originAirportId, destinationAirportId, economyClassTicketsAmount,
                businessClassTicketsAmount, firstClassTicketsAmount);
        return super.update(flight);
    }

    @Override
    public Flight getById(int id) throws DaoException {
        setQuery(SQL_SELECT_BY_ID);
        setParameterObjects(id);
        return (Flight) super.read();
    }

    @Override
    public List<Flight> getAll() throws DaoException {
        setQuery(SQL_SELECT_ALL);
        return (List<Flight>) super.getAll();
    }

    @Override
    public int delete(Flight flight) throws DaoException {
        setQuery(SQL_UPDATE_SET_DELETED);
        setParameterObjects(flight.getId());
        return super.update(flight);
    }

    @Override
    public int recover(Flight flight) throws DaoException {
        setQuery(SQL_UPDATE_SET_NOT_DELETED);
        setParameterObjects(flight.getId());
        return super.update(flight);
    }

    @Override
    protected Flight build(ResultSet resultSet) throws SQLException {
        return (Flight) resultSetHandler.handle(resultSet);
    }


}
