package com.epam.airtickets.dao.flight;

import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.DaoException;

public interface FlightDao  {
    int save(Flight flight) throws DaoException;
    Flight getById(int id) throws DaoException;
    int delete(Flight flight) throws DaoException;
    int recover(Flight flight) throws DaoException;
}
