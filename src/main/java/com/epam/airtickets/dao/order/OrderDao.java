package com.epam.airtickets.dao.order;

import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.exception.DaoException;

import java.util.List;

public interface OrderDao {
    int save(Order order) throws DaoException;
    List<Order> getAllByUserId(int id) throws DaoException;
}
