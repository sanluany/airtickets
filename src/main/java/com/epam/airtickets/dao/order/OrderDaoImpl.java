package com.epam.airtickets.dao.order;

import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.handler.resultset.ResultSetHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class OrderDaoImpl extends AbstractDao implements OrderDao {

    private final static String SQL_INSERT = "INSERT INTO orders (user_id,order_date) values(?,?)";
    private final static String SQL_SELECT_ALL_BY_USER_ID = "SELECT orders.id AS order_id,\n" +
            "       user.id AS user_id,\n" +
            "       user.username AS user_username,\n" +
            "       user.password AS user_password,\n" +
            "       user.role AS user_role,\n" +
            "       user.balance AS user_balance,\n" +
            "       orders.order_date AS order_date\n" +
            "FROM orders\n" +
            "INNER JOIN users AS USER ON user.id = user_id\n" +
            "WHERE user_id = ? ;\n";

    public OrderDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public int save(Order order) throws DaoException {
        User user = order.getUser();
        Integer userId = user.getId();
        LocalDate orderDate = order.getOrderDate();
        setQuery(SQL_INSERT);
        setParameterObjects(userId, orderDate);
        return super.update(order);
    }

    @Override
    public List<Order> getAllByUserId(int id) throws DaoException {
        setQuery(SQL_SELECT_ALL_BY_USER_ID);
        setParameterObjects(id);
        return super.getAllByParameters();
    }

    @Override
    protected Identifiable build(ResultSet resultset) throws SQLException {
        return resultSetHandler.handle(resultset);
    }
}
