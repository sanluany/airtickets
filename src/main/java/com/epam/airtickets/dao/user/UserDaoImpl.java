package com.epam.airtickets.dao.user;

import com.epam.airtickets.handler.resultset.ResultSetHandler;
import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.DaoException;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl extends AbstractDao implements UserDao {

    private final static String SQL_SELECT_BY_USERNAME = "SELECT user.id AS user_id,\n" +
            "       user.username AS user_username,\n" +
            "       user.password AS user_password,\n" +
            "       user.role AS user_role,\n" +
            "       user.balance AS user_balance\n" +
            "FROM users AS USER\n" +
            "WHERE username = ?;";

    private final static String SQL_UPDATE_BALANCE = "UPDATE users\n" +
            "SET balance = balance - ? \n" +
            "WHERE id= ? \n" +
            "  AND balance - ? >=0;";

    public UserDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public User getByUsername(String username) throws DaoException {
        setQuery(SQL_SELECT_BY_USERNAME);
        setParameterObjects(username);
        return (User) super.read();
    }

    @Override
    public int writeOffMoneyFromUserBalance(BigDecimal total, User user) throws DaoException {
        Integer userId = user.getId();
        setQuery(SQL_UPDATE_BALANCE);
        setParameterObjects(total, userId, total);
        return super.update(user);
    }

    @Override
    protected User build(ResultSet resultset) throws SQLException {
        return (User) resultSetHandler.handle(resultset);
    }
}
