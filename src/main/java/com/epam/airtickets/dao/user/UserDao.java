package com.epam.airtickets.dao.user;

import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.DaoException;

import java.math.BigDecimal;

public interface UserDao {
    User getByUsername(String username) throws DaoException;
    int writeOffMoneyFromUserBalance(BigDecimal total, User user) throws DaoException;
}
