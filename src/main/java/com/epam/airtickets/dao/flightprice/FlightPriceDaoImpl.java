package com.epam.airtickets.dao.flightprice;

import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.handler.resultset.ResultSetHandler;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class FlightPriceDaoImpl extends AbstractDao implements FlightPriceDao {

    private static final String SQL_INSERT_PRICE = "INSERT INTO flight_prices (flight_id,travel_class,price,start_date,end_date) values (?,?,?,?,?)";
    private static final String SQL_SELECT_ALL_BY_FLIGHT_ID = "SELECT flight_price.id AS flight_price_id,\n" +
            "       flight.id AS flight_id,\n" +
            "       flight.number AS flight_number,\n" +
            "       origin_airport.id AS origin_airport_id,\n" +
            "       origin_airport.code AS origin_airport_code,\n" +
            "       origin_airport.name AS origin_airport_name,\n" +
            "       origin_city.id AS origin_city_id,\n" +
            "       origin_city.name AS origin_city_name,\n" +
            "       destination_airport.id AS destination_airport_id,\n" +
            "       destination_airport.code AS destination_airport_code,\n" +
            "       destination_airport.name AS destination_airport_name,\n" +
            "       destination_city.id AS destination_city_id,\n" +
            "       destination_city.name AS destination_city_name,\n" +
            "       flight.economy_class_tickets_amount AS flight_economy_class_tickets_amount,\n" +
            "       flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "       flight.first_class_tickets_amount AS flight_first_class_tickets_amount," +
            "       flight.is_deleted AS flight_is_deleted,\n" +
            "       flight_price.travel_class AS flight_price_travel_class,\n" +
            "       flight_price.price AS flight_price_price,\n" +
            "       flight_price.start_date AS flight_price_start_date,\n" +
            "       flight_price.end_date AS flight_price_end_date,\n" +
            "       flight_price.is_deleted AS flight_price_is_deleted\n" +
            "FROM flight_prices AS flight_price\n" +
            "INNER JOIN flights AS flight ON flight_price.flight_id = flight.id\n" +
            "INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n" +
            "WHERE flight_price.flight_id = ?\n" +
            "ORDER BY flight_price.price ASC";
    private final static String SQL_SELECT_ALL_BY_FLIGHT_ID_AND_DEPARTURE_DATE = "SELECT flight_price.id AS flight_price_id,\n" +
            "       flight.id AS flight_id,\n" +
            "       flight.number AS flight_number,\n" +
            "       origin_airport.id AS origin_airport_id,\n" +
            "       origin_airport.code AS origin_airport_code,\n" +
            "       origin_airport.name AS origin_airport_name,\n" +
            "       origin_city.id AS origin_city_id,\n" +
            "       origin_city.name AS origin_city_name,\n" +
            "       destination_airport.id AS destination_airport_id,\n" +
            "       destination_airport.code AS destination_airport_code,\n" +
            "       destination_airport.name AS destination_airport_name,\n" +
            "       destination_city.id AS destination_city_id,\n" +
            "       destination_city.name AS destination_city_name,\n" +
            "       flight.economy_class_tickets_amount AS flight_economy_class_tickets_amount,\n" +
            "       flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "       flight.first_class_tickets_amount AS flight_first_class_tickets_amount,       flight.is_deleted AS flight_is_deleted,\n" +
            "       flight_price.travel_class AS flight_price_travel_class,\n" +
            "       flight_price.price AS flight_price_price,\n" +
            "       flight_price.start_date AS flight_price_start_date,\n" +
            "       flight_price.end_date AS flight_price_end_date,\n" +
            "       flight_price.is_deleted AS flight_price_is_deleted\n" +
            "FROM flight_prices AS flight_price\n" +
            "INNER JOIN flights AS flight ON flight_price.flight_id = flight.id\n" +
            "INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n" +
            "WHERE flight_price.flight_id = ? AND flight.is_deleted = 0\n" +
            "   AND EXISTS\n" +
            "    (SELECT *\n" +
            "     FROM timetables\n" +
            "     JOIN flights ON flights.id = flight_id\n" +
            "     WHERE ? BETWEEN start_date AND end_date AND flight_price.price > 0)\n" +
            "ORDER BY flight_price.price ASC";
    private final static String SQL_UPDATE_SET_DELETED = "UPDATE flight_prices SET is_deleted = 1 WHERE id = ?";
    private final static String SQL_UPDATE_SET_NOT_DELETED = "UPDATE flight_prices SET is_deleted = 0 WHERE id = ?";


    public FlightPriceDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public List<FlightPrice> getAllByFlightId(int id) throws DaoException {
        setQuery(SQL_SELECT_ALL_BY_FLIGHT_ID);
        setParameterObjects(id);
        return (List<FlightPrice>) super.getAllByParameters();
    }

    @Override
    public List<FlightPrice> getByFlightIdAndDepartureDate(Integer flightId, LocalDateTime departureDate) throws DaoException {
        setQuery(SQL_SELECT_ALL_BY_FLIGHT_ID_AND_DEPARTURE_DATE);
        setParameterObjects(flightId, departureDate);
        return super.getAllByParameters();
    }

    @Override
    public int save(FlightPrice flightPrice) throws DaoException {
        Flight flight = flightPrice.getFlight();

        Integer flightId = flight.getId();
        TravelClass travelClass = flightPrice.getTravelClass();
        String travelClassAsString = travelClass.name();
        BigDecimal price = flightPrice.getPrice();
        LocalDate startDate = flightPrice.getStartDate();
        LocalDate endDate = flightPrice.getEndDate();

        setQuery(SQL_INSERT_PRICE);
        setParameterObjects(flightId, travelClassAsString, price, startDate, endDate);
        return super.update(flightPrice);
    }

    @Override
    public int delete(FlightPrice flightPrice) throws DaoException {
        setQuery(SQL_UPDATE_SET_DELETED);
        setParameterObjects(flightPrice.getId());
        return super.update(flightPrice);
    }

    @Override
    public int recover(FlightPrice flightPrice) throws DaoException {
        setQuery(SQL_UPDATE_SET_NOT_DELETED);
        setParameterObjects(flightPrice.getId());
        return super.update(flightPrice);
    }

    @Override
    protected Identifiable build(ResultSet resultset) throws SQLException {
        return resultSetHandler.handle(resultset);
    }

}
