package com.epam.airtickets.dao.flightprice;

import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.DaoException;

import java.time.LocalDateTime;
import java.util.List;

public interface FlightPriceDao {
    List<FlightPrice> getAllByFlightId(int id) throws DaoException;
    List<FlightPrice> getByFlightIdAndDepartureDate(Integer flightId, LocalDateTime departureDate) throws DaoException;
    int save(FlightPrice flightPrice) throws DaoException;
    int delete(FlightPrice flightPrice) throws DaoException;
    int recover(FlightPrice flightPrice) throws DaoException;
}
