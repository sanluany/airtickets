package com.epam.airtickets.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

    private final static Logger logger = LogManager.getFormatterLogger(ConnectionPool.class);

    private static ConnectionPool connectionPool;
    private final static int SIZE = 10;
    private final List<Connection> availableConnections = new ArrayList<>(SIZE);
    private final List<Connection> usedConnections = new ArrayList<>(SIZE);

    private final static String DATABASE_PROPERTIES = "config/database.properties";
    private final static String JDBC_DRIVER = "jdbc.driver";
    private final static String JDBC_URL = "jdbc.url";
    private final static String JDBC_USER = "jdbc.user";
    private final static String JDBC_PASSWORD = "jdbc.password";

    private final static AtomicBoolean isCreated = new AtomicBoolean(false);
    private final static ReentrantLock outerLock = new ReentrantLock();
    private final static ReentrantLock connectionLock = new ReentrantLock();
    private final static Semaphore connectionSemaphore = new Semaphore(SIZE);

    public static ConnectionPool getConnectionPool() {
        if (!isCreated.get()) {
            outerLock.lock();
            if (connectionPool == null) {
                connectionPool = new ConnectionPool();
                connectionPool.initialize();
                isCreated.getAndSet(true);
            }
            outerLock.unlock();
        }
        return connectionPool;
    }

    private void initialize() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = ConnectionPool.class.getClassLoader().getResourceAsStream(DATABASE_PROPERTIES);
            properties.load(inputStream);

            String driver = properties.getProperty(JDBC_DRIVER);
            Class.forName(driver).newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IOException e) {
            logger.warn(e.getMessage(),e);
        }
        for (int i = 0; i < SIZE; i++) {
            Connection connection = createConnection(properties);
            availableConnections.add(connection);
        }
    }

    private Connection createConnection(Properties properties) {
        Connection connection = null;
        try {
            String URL = properties.getProperty(JDBC_URL);
            String user = properties.getProperty(JDBC_USER);
            String password = properties.getProperty(JDBC_PASSWORD);

            connection = DriverManager.getConnection(URL, user, password);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            logger.warn(e.getMessage(),e);
        }
        return connection;
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connectionSemaphore.acquire();
            connectionLock.lock();
            int availableConnectionsAmount = availableConnections.size();
            connection = availableConnections.remove(availableConnectionsAmount - 1);
            usedConnections.add(connection);
        } catch (InterruptedException e) {
            logger.warn(e.getMessage(),e);
        } finally {
            connectionLock.unlock();
        }
        return connection;
    }

    public void releaseConnection(Connection connection) {
        connectionLock.lock();
        usedConnections.remove(connection);
        availableConnections.add(connection);
        connectionLock.unlock();
        connectionSemaphore.release();
    }
}
