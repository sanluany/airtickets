package com.epam.airtickets.dao.ticket;

import com.epam.airtickets.handler.resultset.ResultSetHandler;
import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.exception.DaoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class TicketDaoImpl extends AbstractDao implements TicketDao {

    private final static String COUNT_ID = "count(id)";
    private final static String SQL_SELECT_COUNT_BY_TRAVEL_CLASS_AND_TIMETABLE_ID = "SELECT count(id)\n" +
            "FROM tickets\n" +
            "WHERE travel_class = ?\n" +
            "  AND timetable_id = ?";

    private final static String SQL_INSERT = "INSERT INTO tickets (order_id,timetable_id,travel_class," +
            "passenger_first_name, passenger_last_name, passenger_passport_number, passenger_date_of_birth)" +
            "values(?,?,?,?,?,?,?)";

    private final static String SQL_SELECT_ALL_BY_ORDER_ID = "SELECT ticket.id AS ticket_id,\n" +
            "       orders.id AS order_id,\n" +
            "       user.id AS user_id,\n" +
            "       user.username AS user_username,\n" +
            "       user.password AS user_password,\n" +
            "       user.role AS user_role,\n" +
            "       user.balance AS user_balance,\n" +
            "       orders.order_date AS order_date,\n" +
            "       timetable.id AS timetable_id,\n" +
            "       flight.id AS flight_id,\n" +
            "       flight.number AS flight_number,\n" +
            "       origin_airport.id AS origin_airport_id,\n" +
            "       origin_airport.code AS origin_airport_code,\n" +
            "       origin_airport.name AS origin_airport_name,\n" +
            "       origin_city.id AS origin_city_id,\n" +
            "       origin_city.name AS origin_city_name,\n" +
            "       destination_airport.id AS destination_airport_id,\n" +
            "       destination_airport.code AS destination_airport_code,\n" +
            "       destination_airport.name AS destination_airport_name,\n" +
            "       destination_city.id AS destination_city_id,\n" +
            "       destination_city.name AS destination_city_name,\n" +
            "       flight.economy_class_tickets_amount AS flight_economy_class_tickets_amount,\n" +
            "       flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "       flight.first_class_tickets_amount AS flight_first_class_tickets_amount,\n" +
            "       flight.is_deleted AS flight_is_deleted,\n" +
            "       timetable.departure_date AS timetable_departure_date,\n" +
            "       timetable.arrival_date AS timetable_arrival_date,\n" +
            "       timetable.is_deleted AS timetable_is_deleted,\n" +
            "       ticket.travel_class AS ticket_travel_class,\n" +
            "       ticket.passenger_first_name AS ticket_passenger_first_name,\n" +
            "       ticket.passenger_last_name AS ticket_passenger_last_name,\n" +
            "       ticket.passenger_passport_number AS ticket_passenger_passport_number,\n" +
            "       ticket.passenger_date_of_birth AS ticket_passenger_date_of_birth\n" +
            "FROM tickets AS ticket\n" +
            "INNER JOIN orders ON ticket.order_id = orders.id\n" +
            "INNER JOIN users AS USER ON user.id = user_id\n" +
            "INNER JOIN timetables AS timetable ON ticket.timetable_id = timetable.id\n" +
            "INNER JOIN flights AS flight ON timetable.flight_id = flight.id\n" +
            "INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n" +
            "WHERE order_id = ? ;";

    public TicketDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public int save(Ticket ticket, Order order) throws DaoException {
        Integer orderId = order.getId();

        Timetable timetable = ticket.getTimetable();
        Integer timetableId = timetable.getId();

        TravelClass travelClass = ticket.getTravelClass();
        String passengerFirstName = ticket.getPassengerFirstName();
        String passengerLastName = ticket.getPassengerLastName();
        String passengerPassportNumber = ticket.getPassengerPassportNumber();
        LocalDate passengerDateOfBirth = ticket.getPassengerDateOfBirth();

        setQuery(SQL_INSERT);
        setParameterObjects(orderId, timetableId, travelClass.name(), passengerFirstName, passengerLastName, passengerPassportNumber,
                passengerDateOfBirth);
        return super.update(ticket);
    }

    @Override
    public int getSoldTicketsAmountByTravelClassAndTimetable(TravelClass travelClass, Timetable timetable) throws DaoException {
        setQuery(SQL_SELECT_COUNT_BY_TRAVEL_CLASS_AND_TIMETABLE_ID);
        Integer timetableId = timetable.getId();
        setParameterObjects(travelClass.name(), timetableId);
        Integer ticketsAmount;
        try {
            ResultSet resultSet = getResultSet();
            resultSet.next();
            ticketsAmount = resultSet.getInt(COUNT_ID);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return ticketsAmount;
    }

    @Override
    public List<Ticket> getAllByOrderId(int id) throws DaoException {
        setQuery(SQL_SELECT_ALL_BY_ORDER_ID);
        setParameterObjects(id);
        return super.getAllByParameters();
    }

    @Override
    protected Identifiable build(ResultSet resultset) throws SQLException {
        return resultSetHandler.handle(resultset);
    }
}
