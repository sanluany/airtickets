package com.epam.airtickets.dao.ticket;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.exception.DaoException;

import java.util.List;

public interface TicketDao {
    int save(Ticket ticket, Order order) throws DaoException;
    int getSoldTicketsAmountByTravelClassAndTimetable(TravelClass travelClass, Timetable timetable) throws DaoException;
    List<Ticket> getAllByOrderId(int id) throws DaoException;
}
