package com.epam.airtickets.dao.airport;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.exception.DaoException;

public interface AirportDao {
    Airport getById(int id) throws DaoException;
}
