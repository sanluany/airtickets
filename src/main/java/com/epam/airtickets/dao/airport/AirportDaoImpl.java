package com.epam.airtickets.dao.airport;

import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.handler.resultset.ResultSetHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AirportDaoImpl extends AbstractDao implements AirportDao{

    private final static String SQL_SELECT_BY_ID = "SELECT airport.id AS origin_airport_id,\n" +
            "       airport.code AS origin_airport_code,\n" +
            "       airport.name AS origin_airport_name,\n" +
            "       city.id AS origin_city_id,\n" +
            "       city.name AS origin_city_name\n" +
            "FROM airports AS airport\n" +
            "INNER JOIN cities AS city ON airport.city_id = city.id\n" +
            "WHERE airport.id = ?";

    private final static String SQL_SELECT_ALL = "SELECT airport.id AS origin_airport_id,\n" +
            "       airport.code AS origin_airport_code,\n" +
            "       airport.name AS origin_airport_name,\n" +
            "       city.id AS origin_city_id,\n" +
            "       city.name AS origin_city_name\n" +
            "FROM airports AS airport\n" +
            "INNER JOIN cities AS city ON airport.city_id = city.id\n" +
            "ORDER BY airport.id ASC";

    public AirportDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public List<Airport> getAll() throws DaoException {
        setQuery(SQL_SELECT_ALL);
        return (List<Airport>) super.getAll();
    }

    @Override
    public Airport getById(int id) throws DaoException {
        setQuery(SQL_SELECT_BY_ID);
        setParameterObjects(id);
        return (Airport) super.read();
    }

    @Override
    protected Airport build(ResultSet resultSet) throws SQLException {
        return (Airport) resultSetHandler.handle(resultSet);
    }


}
