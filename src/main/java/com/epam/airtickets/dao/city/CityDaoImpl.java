package com.epam.airtickets.dao.city;

import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.handler.resultset.ResultSetHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CityDaoImpl extends AbstractDao implements CityDao {

    private final static String SQL_SELECT_BY_ID = "SELECT city.id AS origin_city_id, city.name AS origin_city_name\n" +
            "FROM cities AS city\n" +
            "WHERE id = ?";
    private final static String SQL_SELECT_ALL = "SELECT city.id AS origin_city_id, city.name AS origin_city_name\n" +
            "FROM cities AS city ORDER BY city.id";

    public CityDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public List<City> getAll() throws DaoException {
        setQuery(SQL_SELECT_ALL);
        return super.getAll();
    }

    @Override
    public City getById(int id) throws DaoException {
        setQuery(SQL_SELECT_BY_ID);
        return (City) super.read();
    }

    @Override
    protected City build(ResultSet resultset) throws SQLException{
        return (City) resultSetHandler.handle(resultset);
    }
}
