package com.epam.airtickets.dao.city;

import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.DaoException;

public interface CityDao {
    City getById(int id) throws DaoException;
}
