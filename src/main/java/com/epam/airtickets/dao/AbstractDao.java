package com.epam.airtickets.dao;

import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.exception.DaoException;
import com.epam.airtickets.handler.resultset.ResultSetHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractDao<T extends Identifiable> implements Dao {

    private final static Logger logger = LogManager.getFormatterLogger(AbstractDao.class);

    private final Connection connection;
    protected final ResultSetHandler resultSetHandler;
    private String query;
    private List<Object> objects;

    public AbstractDao(Connection connection, ResultSetHandler resultSetHandler) {
        this.connection = connection;
        this.resultSetHandler = resultSetHandler;
    }

    @Override
    public T read() throws DaoException {
        T entity = null;
        ResultSet resultSet = null;
        try {
            resultSet = getResultSet();
            if (resultSet != null && resultSet.next()) {
                entity = build(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        } finally {
            close(resultSet);
        }
        return entity;
    }

    @Override
    public List<T> getAll() throws DaoException {
        List<T> list;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            list = getResultList(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(statement);
        }
        return list;
    }

    @Override
    public List<T> getAllByParameters() throws DaoException {
        List<T> list;
        ResultSet resultSet = null;
        try {
            resultSet = getResultSet();
            list = getResultList(resultSet);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        } finally {
            close(resultSet);
        }
        return list;
    }

    @Override
    public int update(Identifiable entity) throws DaoException {
        int result;
        try {
            result = executePreparedStatementUpdate(entity);
        } catch (SQLException e) {
            throw new DaoException(e.getMessage(), e);
        }
        return result;
    }

    protected abstract T build(ResultSet resultset) throws SQLException;

    private int executePreparedStatementUpdate(Identifiable entity) throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement();
        int result = preparedStatement.executeUpdate();

        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        setIdAfterUpdate(entity, generatedKeys);
        return result;
    }

    private void setIdAfterUpdate(Identifiable entity, ResultSet generatedKeys) throws SQLException {
        if (entity.getId() == null) {
            generatedKeys.next();
            Integer id = generatedKeys.getInt(1);
            entity.setId(id);
        }
    }

    private void fillPreparedStatementWithObjects(PreparedStatement preparedStatement) throws SQLException {
        if (objects != null) {
            for (int i = 1; i < objects.size() + 1; i++) {
                preparedStatement.setObject(i, objects.get(i - 1));
            }
        }
    }

    protected ResultSet getResultSet() throws SQLException {
        PreparedStatement preparedStatement = getPreparedStatement();
        return preparedStatement.executeQuery();
    }

    private PreparedStatement getPreparedStatement() throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        fillPreparedStatementWithObjects(preparedStatement);
        return preparedStatement;
    }

    private List<T> getResultList(ResultSet resultSet) throws SQLException {
        List<T> list = new ArrayList<>();
        while (resultSet.next()) {
            T object = build(resultSet);
            list.add(object);
        }
        return list;
    }

    private void close(AutoCloseable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        }
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setParameterObjects(Object... objects) {
        this.objects = Arrays.asList(objects);
    }

}
