package com.epam.airtickets.dao.timetable;

import com.epam.airtickets.handler.resultset.ResultSetHandler;
import com.epam.airtickets.dao.AbstractDao;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.DaoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class TimetableDaoImpl extends AbstractDao implements TimetableDao {

    private final static String SQL_INSERT_TIMETABLE = "INSERT INTO timetables (flight_id,departure_date,arrival_date)  values(?,?,?)";
    private final static String SQL_SELECT_ALL = "SELECT timetable.id AS timetable_id,\n" +
            "       flight.id AS flight_id,\n" +
            "       flight.number AS flight_number,\n" +
            "       origin_airport.id AS origin_airport_id,\n" +
            "       origin_airport.code AS origin_airport_code,\n" +
            "       origin_airport.name AS origin_airport_name,\n" +
            "       origin_city.id AS origin_city_id,\n" +
            "       origin_city.name AS origin_city_name,\n" +
            "       destination_airport.id AS destination_airport_id,\n" +
            "       destination_airport.code AS destination_airport_code,\n" +
            "       destination_airport.name AS destination_airport_name,\n" +
            "       destination_city.id AS destination_city_id,\n" +
            "       destination_city.name AS destination_city_name,\n" +
            "       flight.economy_class_tickets_amount AS flight_economy_class_tickets_amount,\n" +
            "       flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "       flight.first_class_tickets_amount AS flight_first_class_tickets_amount," +
            "       flight.is_deleted AS flight_is_deleted,\n" +
            "       timetable.departure_date AS timetable_departure_date,\n" +
            "       timetable.arrival_date AS timetable_arrival_date,\n" +
            "       timetable.is_deleted AS timetable_is_deleted\n" +
            "FROM timetables AS timetable\n" +
            "INNER JOIN flights AS flight ON timetable.flight_id = flight.id\n" +
            "INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n";
    private final static String SQL_SELECT_BY_ID = SQL_SELECT_ALL + " WHERE timetable.id = ?";
    private final static String SQL_SELECT_ALL_BY_FLIGHT_ID = "SELECT timetable.id AS timetable_id,\n" +
            "       flight.id AS flight_id,\n" +
            "       flight.number AS flight_number,\n" +
            "       origin_airport.id AS origin_airport_id,\n" +
            "       origin_airport.code AS origin_airport_code,\n" +
            "       origin_airport.name AS origin_airport_name,\n" +
            "       origin_city.id AS origin_city_id,\n" +
            "       origin_city.name AS origin_city_name,\n" +
            "       destination_airport.id AS destination_airport_id,\n" +
            "       destination_airport.code AS destination_airport_code,\n" +
            "       destination_airport.name AS destination_airport_name,\n" +
            "       destination_city.id AS destination_city_id,\n" +
            "       destination_city.name AS destination_city_name,\n" +
            "       flight.economy_class_tickets_amount AS flight_economy_class_tickets_amount,\n" +
            "       flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "       flight.first_class_tickets_amount AS flight_first_class_tickets_amount," +
            "       flight.is_deleted AS flight_is_deleted,\n" +
            "       timetable.departure_date AS timetable_departure_date,\n" +
            "       timetable.arrival_date AS timetable_arrival_date,\n" +
            "       timetable.is_deleted AS timetable_is_deleted\n" +
            "FROM timetables AS timetable\n" +
            "INNER JOIN flights AS flight ON timetable.flight_id = flight.id\n" +
            "INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n" +
            " WHERE flight_id = ?";
    private final static String SQL_UPDATE_SET_DELETED = "UPDATE timetables SET is_deleted = 1 WHERE id = ?";
    private final static String SQL_UPDATE_SET_NOT_DELETED = "UPDATE timetables SET is_deleted = 0 WHERE id = ?";
    private final static String SQL_SELECT_ALL_BY_CITIES_AND_DEPARTURE_DATE = "SELECT\n" +
            "  timetable.id                         AS timetable_id,\n" +
            "  flight.id                            AS flight_id,\n" +
            "  flight.number                        AS flight_number,\n" +
            "  origin_airport.id                    AS origin_airport_id,\n" +
            "  origin_airport.code                  AS origin_airport_code,\n" +
            "  origin_airport.name                  AS origin_airport_name,\n" +
            "  origin_city.id                       AS origin_city_id,\n" +
            "  origin_city.name                     AS origin_city_name,\n" +
            "  destination_airport.id               AS destination_airport_id,\n" +
            "  destination_airport.code             AS destination_airport_code,\n" +
            "  destination_airport.name             AS destination_airport_name,\n" +
            "  destination_city.id                  AS destination_city_id,\n" +
            "  destination_city.name                AS destination_city_name,\n" +
            "  flight.economy_class_tickets_amount  AS flight_economy_class_tickets_amount,\n" +
            "  flight.business_class_tickets_amount AS flight_business_class_tickets_amount,\n" +
            "  flight.first_class_tickets_amount    AS flight_first_class_tickets_amount,\n" +
            "  flight.is_deleted                    AS flight_is_deleted,\n" +
            "  timetable.departure_date             AS timetable_departure_date,\n" +
            "  timetable.arrival_date               AS timetable_arrival_date,\n" +
            "  timetable.is_deleted                 AS timetable_is_deleted\n" +
            "FROM timetables AS timetable\n" +
            "  INNER JOIN flights AS flight ON timetable.flight_id = flight.id\n" +
            "  INNER JOIN airports AS origin_airport ON flight.origin_airport_id = origin_airport.id\n" +
            "  INNER JOIN cities AS origin_city ON origin_airport.city_id = origin_city.id\n" +
            "  INNER JOIN airports AS destination_airport ON flight.destination_airport_id = destination_airport.id\n" +
            "  INNER JOIN cities AS destination_city ON destination_airport.city_id = destination_city.id\n" +
            "WHERE origin_airport.city_id = ?\n" +
            "      AND destination_airport.city_id = ?\n" +
            "      AND cast(departure_date AS DATE) = ?\n" +
            "      AND timetable.is_deleted = 0\n" +
            "      AND flight.is_deleted = 0 \n" +
            "      AND EXISTS\n" +
            "      (SELECT *\n" +
            "       FROM flight_prices\n" +
            "         JOIN flights ON flights.id = flight_id\n" +
            "       WHERE ? BETWEEN start_date AND end_date)";

    public TimetableDaoImpl(Connection connection, ResultSetHandler resultSetHandler) {
        super(connection, resultSetHandler);
    }

    @Override
    public int save(Timetable timetable) throws DaoException {
        setQuery(SQL_INSERT_TIMETABLE);

        Flight flight = timetable.getFlight();
        int flightId = flight.getId();
        LocalDateTime departureDate = timetable.getDepartureDate();
        LocalDateTime arrivalDate = timetable.getArrivalDate();

        setParameterObjects(flightId, departureDate, arrivalDate);
        return super.update(timetable);
    }

    @Override
    public Timetable getById(Integer id) throws DaoException {
        setQuery(SQL_SELECT_BY_ID);
        setParameterObjects(id);
        return (Timetable) read();
    }

    @Override
    public List<Timetable> getAllByCitiesAndDepartureDate(int originCityId, int destinationCityId,
                                                          String departureDate) throws DaoException {
        setQuery(SQL_SELECT_ALL_BY_CITIES_AND_DEPARTURE_DATE);
        setParameterObjects(originCityId, destinationCityId, departureDate, departureDate);
        return (List<Timetable>) super.getAllByParameters();
    }

    @Override
    public List<Timetable> getAll() throws DaoException {
        setQuery(SQL_SELECT_ALL);
        return (List<Timetable>) super.getAll();
    }

    @Override
    public List<Timetable> getAllByFlightId(int id) throws DaoException {
        setQuery(SQL_SELECT_ALL_BY_FLIGHT_ID);
        setParameterObjects(id);
        return (List<Timetable>) super.getAllByParameters();
    }

    @Override
    public int delete(Timetable timetable) throws DaoException {
        setQuery(SQL_UPDATE_SET_DELETED);
        setParameterObjects(timetable.getId());
        return super.update(timetable);
    }

    @Override
    public int recover(Timetable timetable) throws DaoException {
        setQuery(SQL_UPDATE_SET_NOT_DELETED);
        setParameterObjects(timetable.getId());
        return super.update(timetable);
    }

    @Override
    protected Timetable build(ResultSet resultset) throws SQLException {
        return (Timetable) resultSetHandler.handle(resultset);
    }

}
