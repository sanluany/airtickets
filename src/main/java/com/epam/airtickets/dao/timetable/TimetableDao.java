package com.epam.airtickets.dao.timetable;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.exception.DaoException;

import java.util.List;

public interface TimetableDao {
    int save(Timetable timetable) throws DaoException;

    Timetable getById(Integer id) throws DaoException;

    List<Timetable> getAllByCitiesAndDepartureDate(int originCityId, int destinationCityId,
                                                   String departureDate) throws DaoException;

    List<Timetable> getAllByFlightId(int id) throws DaoException;

    int delete(Timetable timetable) throws DaoException;

    int recover(Timetable timetable) throws DaoException;


}
