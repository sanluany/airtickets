package com.epam.airtickets.dao;


import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.exception.DaoException;

import java.util.List;

/**
 * An object that implements the Dao interface is able to execute sql queries towards database.
 *
 * <p> Dao interface provides basic methods of interactions with database.
 *
 * @param <T> the type of object interface works with.
 * @author Boychuk Dmitriy
 * @see Identifiable
 */
public interface Dao<T extends Identifiable> {
    T read() throws DaoException;

    int update(T entity) throws DaoException;

    List<T> getAll() throws DaoException;

    List<T> getAllByParameters() throws DaoException;
}
