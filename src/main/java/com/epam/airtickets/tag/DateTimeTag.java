package com.epam.airtickets.tag;

import com.epam.airtickets.utils.DateUtils;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.time.temporal.Temporal;

public class DateTimeTag extends SimpleTagSupport {

    public enum PatternType {DATE, TIME, DATE_TIME}

    private Temporal temporal;
    private PatternType patternType;

    public void setTemporal(Temporal temporal) {
        this.temporal = temporal;
    }

    public void setPatternType(PatternType patternType) {
        this.patternType = patternType;
    }

    @Override
    public void doTag() throws IOException {
        String date = null;
        switch (patternType){
            case DATE:{
                date = DateUtils.getFormattedDate(temporal);
                break;
            }
            case TIME:{
                date = DateUtils.getFormattedTime(temporal);
                break;
            }
            case DATE_TIME:{
                date = DateUtils.getFormattedDateTime(temporal);
                break;
            }
        }
        JspWriter jspWriter = getJspContext().getOut();
        jspWriter.println(date);
    }
}
