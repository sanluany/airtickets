package com.epam.airtickets.validator;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public class TimetableValidator extends AbstractValidator {

    private final static String TIMETABLE = "timetable";

    private final static String TIMETABLE_FIRST_FLIGHT = "timetable.flight.first";
    private final static String TIMETABLE_LAST_FLIGHT = "timetable.last.flight";
    private final static String TIMETABLE_DEPARTURE_TIME = "timetable.departure.time";
    private final static String TIMETABLE_ARRIVAL_TIME = "timetable.arrival.time";

    private final static String TIMETABLE_OVERLAP = "timetable.overlap";
    private final static String TIMETABLE_SAME_DATES = "timetable.same.dates";
    private final static String TIMETABLE_NO_SELECTED_DAYS = "timetable.no.selected.days";


    private final static String FIRST_FLIGHT_DATE = "first_flight_date";
    private final static String LAST_FLIGHT_DATE = "last_flight_date";
    private final static String DEPARTURE_TIME = "departure_time";
    private final static String ARRIVAL_TIME = "arrival_time";
    private final static String DAY = "day";

    @Override
    public boolean validate(ServletRequest request, ValidationError validationError) {

        String firstFlightDate = request.getParameter(FIRST_FLIGHT_DATE);
        if (!isValidDateParameter(firstFlightDate, validationError)) {
            validationError.setParameterBundleKey(TIMETABLE_FIRST_FLIGHT);
            return false;
        }

        String lastFlightDate = request.getParameter(LAST_FLIGHT_DATE);
        if (!isValidDateParameter(lastFlightDate, validationError)) {
            validationError.setParameterBundleKey(TIMETABLE_LAST_FLIGHT);
            return false;
        }

        String departureTime = request.getParameter(DEPARTURE_TIME);
        if (!isValidTimeParameter(departureTime, validationError)) {
            validationError.setParameterBundleKey(TIMETABLE_DEPARTURE_TIME);
            return false;
        }

        String arrivalTime = request.getParameter(ARRIVAL_TIME);
        if (!isValidTimeParameter(arrivalTime, validationError)) {
            validationError.setParameterBundleKey(TIMETABLE_ARRIVAL_TIME);
            return false;
        }

        String[] days = request.getParameterValues(DAY);
        if (days == null || days.length == 0) {
            validationError.setValidationBundleKey(TIMETABLE_NO_SELECTED_DAYS);
            return false;
        }

        if (firstFlightDate.equalsIgnoreCase(lastFlightDate)) {
            validationError.setValidationBundleKey(TIMETABLE_SAME_DATES);
            return false;
        }
        if (!isTimetableUnique(request, firstFlightDate, lastFlightDate, departureTime)) {
            validationError.setValidationBundleKey(TIMETABLE_OVERLAP);
            return false;
        }
        return true;
    }

    private boolean isTimetableUnique(ServletRequest servletRequest, String firstFlightDateAsString, String lastFlightDateAsString,
                                      String departureTimeAsString) {
        LocalDate providedFirstFlightDate = LocalDate.parse(firstFlightDateAsString);
        LocalDate providedLastFlightDate = LocalDate.parse(lastFlightDateAsString);
        LocalTime providedDepartureTime = LocalTime.parse(departureTimeAsString);

        Object timetableAttribute = SessionHelper.getSessionAttribute((HttpServletRequest) servletRequest, TIMETABLE);
        if (timetableAttribute == null) {
            return true;
        }

        List<Timetable> timetables = (List<Timetable>) timetableAttribute;
        for (Timetable timetable : timetables) {
            boolean isDeleted = timetable.getDeleted();
            if (isDeleted) {
                continue;
            }

            LocalDateTime departureDateTime = timetable.getDepartureDate();
            LocalDate departureDate = departureDateTime.toLocalDate();
            LocalTime departureTime = departureDateTime.toLocalTime();

            if (providedFirstFlightDate.equals(departureDate) || providedLastFlightDate.equals(departureDate)
                    && providedDepartureTime.equals(departureTime)) {
                return false;
            }
        }
        return true;
    }
}
