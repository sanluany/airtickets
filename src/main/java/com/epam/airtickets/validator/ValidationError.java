package com.epam.airtickets.validator;

public class ValidationError {
    private String parameterBundleKey;
    private String validationBundleKey;

    public String getParameterBundleKey() {
        return parameterBundleKey;
    }

    public void setParameterBundleKey(String parameterBundleKey) {
        this.parameterBundleKey = parameterBundleKey;
    }

    public String getValidationBundleKey() {
        return validationBundleKey;
    }

    public void setValidationBundleKey(String validationBundleKey) {
        this.validationBundleKey = validationBundleKey;
    }
}
