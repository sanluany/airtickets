package com.epam.airtickets.validator;


import javax.servlet.ServletRequest;


public interface Validator {
    boolean validate(ServletRequest request, ValidationError validationError);
}
