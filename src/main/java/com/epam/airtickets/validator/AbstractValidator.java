package com.epam.airtickets.validator;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

public abstract class AbstractValidator implements Validator {

    final static String ZERO_STRING = "0";
    private final static String EMPTY = "error.empty";
    private final static String TYPE = "error.type.integer";
    private final static String NEGATIVE = "error.negative";


    boolean isValidDateParameter(String parameter, ValidationError validationError) {// package-private
        if (isNullOrEmpty(parameter, validationError)) {
            return false;
        }
        try {
            LocalDate.parse(parameter);
        } catch (DateTimeParseException e) {
            validationError.setValidationBundleKey(TYPE);
            return false;
        }
        return true;
    }

    boolean isValidTimeParameter(String parameter, ValidationError validationError) {// package-private
        if (isNullOrEmpty(parameter, validationError)) {
            return false;
        }
        try {
            LocalTime.parse(parameter);
        } catch (DateTimeParseException e) {
            validationError.setValidationBundleKey(TYPE);
            return false;
        }
        return true;
    }

    boolean isValidIntegerParameter(String parameter, ValidationError validationError) {// package-private
        if (isNullOrEmpty(parameter, validationError)) {
            return false;
        }
        Integer integer;
        try {
            integer = Integer.valueOf(parameter);
        } catch (NumberFormatException e) {
            validationError.setValidationBundleKey(TYPE);
            return false;
        }
        if (integer < 0) {
            validationError.setValidationBundleKey(NEGATIVE);
            return false;
        }
        return true;
    }

    boolean isNullOrEmpty(String parameter, ValidationError validationError) {// package-private
        if (parameter == null || parameter.isEmpty()) {
            validationError.setValidationBundleKey(EMPTY);
            return true;
        }
        return false;
    }


}
