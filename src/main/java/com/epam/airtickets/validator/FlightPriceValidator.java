package com.epam.airtickets.validator;

import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

public class FlightPriceValidator extends AbstractValidator {

    private final static String FLIGHT_PRICES = "flightPrices";

    private final static String FLIGHT_PRICE_START_DATE = "flight.price.start.date";
    private final static String FLIGHT_PRICE_END_DATE = "flight.price.end.date";
    private final static String FLIGHT_PRICE_TRAVEL_CLASS = "flight.price.travel.class";
    private final static String FLIGHT_PRICE_PRICE = "flight.price.price";

    private final static String FLIGHT_PRICE_ILLEGAL_TRAVEL_CLASS = "flight.price.illegal.travel.class";
    private final static String FLIGHT_PRICE_PRICE_ZERO = "flight.price.price.zero";
    private final static String FLIGHT_PRICE_OVERLAP = "flight.price.overlap";
    private final static String FLIGHT_PRICE_DATES_SAME = "flight.price.dates.same";

    private final static String START_DATE = "start_date";
    private final static String END_DATE = "end_date";
    private final static String TRAVEL_CLASS = "travel_class";
    private final static String PRICE = "price";

    @Override
    public boolean validate(ServletRequest request, ValidationError validationError) {

        String startDate = request.getParameter(START_DATE);
        if (!isValidDateParameter(startDate, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_PRICE_START_DATE);
            return false;
        }

        String endDate = request.getParameter(END_DATE);
        if (!isValidDateParameter(endDate, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_PRICE_END_DATE);
            return false;
        }

        String travelClassAsString = request.getParameter(TRAVEL_CLASS);
        if (isNullOrEmpty(travelClassAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_PRICE_TRAVEL_CLASS);
            return false;
        }

        if (!isValidTravelClass(travelClassAsString)) {
            validationError.setValidationBundleKey(FLIGHT_PRICE_ILLEGAL_TRAVEL_CLASS);
            return false;
        }

        String priceAsString = request.getParameter(PRICE);
        if (!isValidIntegerParameter(priceAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_PRICE_PRICE);
            return false;
        }

        Integer price = Integer.valueOf(priceAsString);
        if (price <= 0) {
            validationError.setValidationBundleKey(FLIGHT_PRICE_PRICE_ZERO);
            return false;
        }

        if (!isFlightPriceUnique(request, startDate, endDate, travelClassAsString)) {
            validationError.setValidationBundleKey(FLIGHT_PRICE_OVERLAP);
            return false;
        }

        if (startDate.equalsIgnoreCase(endDate)) {
            validationError.setValidationBundleKey(FLIGHT_PRICE_DATES_SAME);
            return false;
        }
        return true;
    }

    private boolean isValidTravelClass(String travelClassAsString) {
        try {
            TravelClass.getByName(travelClassAsString);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    private boolean isFlightPriceUnique(ServletRequest request, String providedStartDateAsString,
                                        String providedEndDateAsString, String providedTravelClassAsString) {
        Object flightPricesAttribute = SessionHelper.getSessionAttribute((HttpServletRequest) request, FLIGHT_PRICES);
        if (flightPricesAttribute == null) {
            return true;
        }
        List<FlightPrice> flightPrices = (List<FlightPrice>) flightPricesAttribute;

        LocalDate providedStartDate = LocalDate.parse(providedStartDateAsString);
        LocalDate providedEndDate = LocalDate.parse(providedEndDateAsString);

        TravelClass providedTravelClass = TravelClass.getByName(providedTravelClassAsString);
        for (FlightPrice flightPrice : flightPrices) {
            TravelClass travelClass = flightPrice.getTravelClass();
            boolean isDeleted = flightPrice.getDeleted();

            if (isDeleted || providedTravelClass != travelClass) {
                continue;
            }

            LocalDate startDate = flightPrice.getStartDate();
            LocalDate endDate = flightPrice.getEndDate();

            if (providedStartDate.isBefore(startDate) && providedEndDate.isAfter(startDate)) {
                return false;
            }
            if (providedStartDate.isAfter(startDate) && providedStartDate.isBefore(endDate)) {
                return false;
            }
            if (providedStartDate.isBefore(startDate) && providedEndDate.isAfter(endDate)) {
                return false;
            }
            if(providedStartDate.equals(startDate) || providedStartDate.equals(endDate)){
                return false;
            }
            if(providedEndDate.equals(startDate) || providedEndDate.equals(endDate)){
                return false;
            }
        }
        return true;
    }

}
