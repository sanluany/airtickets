package com.epam.airtickets.validator;

import javax.servlet.ServletRequest;

public class LoginValidator extends AbstractValidator {
    private final static String LOGIN_USERNAME = "login.username";
    private final static String LOGIN_PASSWORD = "login.password";

    private final static String USERNAME = "username";
    private final static String PASSWORD = "password";

    @Override
    public boolean validate(ServletRequest request, ValidationError validationError) {

        String username = request.getParameter(USERNAME);
        if (isNullOrEmpty(username, validationError)) {
            validationError.setParameterBundleKey(LOGIN_USERNAME);
            return false;
        }

        String password = request.getParameter(PASSWORD);
        if (isNullOrEmpty(password, validationError)) {
            validationError.setParameterBundleKey(LOGIN_PASSWORD);
            return false;
        }

        return true;
    }
}
