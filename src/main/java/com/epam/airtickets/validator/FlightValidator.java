package com.epam.airtickets.validator;

import com.epam.airtickets.handler.resultset.FlightResultSetHandler;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


public class FlightValidator extends AbstractValidator {

    private final static String FLIGHT_NUMBER = "flight.number";
    private final static String FLIGHT_ORIGIN_AIRPORT = "flight.origin.airport";
    private final static String FLIGHT_DESTINATION_AIRPORT = "flight.destination.airport";
    private final static String FLIGHT_ECONOMY_CLASS_TICKETS = "flight.economy.class.tickets";
    private final static String FLIGHT_BUSINESS_CLASS_TICKETS = "flight.business.class.tickets";
    private final static String FLIGHT_FIRST_CLASS_TICKETS = "flight.first.class.tickets";
    private final static String FLIGHT_NO_TICKETS = "flight.no.tickets";
    private final static String FLIGHT_SAME_AIRPORTS = "flight.same.airports";
    private final static String FLIGHT_NUMBER_NOT_UNIQUE = "flight.number.not.unique";

    private final static String FLIGHTS = "flights";

    @Override
    public boolean validate(ServletRequest request, ValidationError validationError) {

        String flightNumber = request.getParameter(FlightResultSetHandler.FLIGHT_NUMBER);
        if (isNullOrEmpty(flightNumber, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_NUMBER);
            return false;
        }

        if(!isProvidedFlightNumberUnique(request,flightNumber)){
            validationError.setValidationBundleKey(FLIGHT_NUMBER_NOT_UNIQUE);
            return false;
        }

        String airportOfOriginIdAsString = request.getParameter(FlightResultSetHandler.ORIGIN_AIRPORT);
        if (!isValidIntegerParameter(airportOfOriginIdAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_ORIGIN_AIRPORT);
            return false;
        }

        String airportOfDestinationIdAsString = request.getParameter(FlightResultSetHandler.DESTINATION_AIRPORT);
        if (!isValidIntegerParameter(airportOfDestinationIdAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_DESTINATION_AIRPORT);
            return false;
        }

        String economyClassTicketsAmountAsString = request.getParameter(FlightResultSetHandler.FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT);
        if (!isValidIntegerParameter(economyClassTicketsAmountAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_ECONOMY_CLASS_TICKETS);
            return false;
        }

        String businessClassTicketsAmountAsString = request.getParameter(FlightResultSetHandler.FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT);
        if (!isValidIntegerParameter(businessClassTicketsAmountAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_BUSINESS_CLASS_TICKETS);
            return false;
        }

        String firstClassTicketsAmountAsString = request.getParameter(FlightResultSetHandler.FLIGHT_FIRST_CLASS_TICKETS_AMOUNT);
        if (!isValidIntegerParameter(firstClassTicketsAmountAsString, validationError)) {
            validationError.setParameterBundleKey(FLIGHT_FIRST_CLASS_TICKETS);
            return false;
        }

        if (economyClassTicketsAmountAsString.equals(ZERO_STRING) && businessClassTicketsAmountAsString.equals(ZERO_STRING)
                && firstClassTicketsAmountAsString.equals(ZERO_STRING)) {
            validationError.setValidationBundleKey(FLIGHT_NO_TICKETS);
            return false;
        }

        if (airportOfOriginIdAsString.equals(airportOfDestinationIdAsString)) {
            validationError.setValidationBundleKey(FLIGHT_SAME_AIRPORTS);
            return false;
        }

        return true;
    }

    private boolean isProvidedFlightNumberUnique(ServletRequest servletRequest, String providedFlightNumber) {
        Object flightsAttribute = SessionHelper.getSessionAttribute((HttpServletRequest) servletRequest, FLIGHTS);
        if (flightsAttribute == null) {
            return true;
        }
        List<Flight> flights = (List<Flight>) flightsAttribute;
        for (Flight flight : flights) {
            String flightNumber = flight.getNumber();
            if (providedFlightNumber.equalsIgnoreCase(flightNumber)) {
                return false;
            }
        }
        return true;
    }

}
