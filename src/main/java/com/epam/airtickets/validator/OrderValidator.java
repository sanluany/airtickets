package com.epam.airtickets.validator;

import javax.servlet.ServletRequest;


public class OrderValidator extends AbstractValidator {


    private final static String ORDER_PASSENGERS_AMOUNT = "order.passengers.amount";
    private final static String ORDER_PASSENGER_NAME_FIRST = "order.passenger.name.first";
    private final static String ORDER_PASSENGER_NAME_LAST = "order.passenger.name.last";
    private final static String ORDER_PASSENGER_PASSPORT_NUMBER = "order.passenger.passport.number";

    private final static String PASSENGER_FIRST_NAME_PARAMETERS = "passenger_first_name";
    private final static String PASSENGER_LAST_NAME_PARAMETERS = "passenger_last_name";
    private final static String PASSENGER_PASSPORT_NUMBER_PARAMETERS = "passenger_passport_number";

    private final static String AMOUNT_OF_TICKETS = "amount_of_tickets";


    @Override
    public boolean validate(ServletRequest request, ValidationError validationError) {
        String amountOfTicketsAsString = request.getParameter(AMOUNT_OF_TICKETS);
        if (!isValidIntegerParameter(amountOfTicketsAsString, validationError)) {
            validationError.setParameterBundleKey(ORDER_PASSENGERS_AMOUNT);
            return false;
        }

        int amountOfTickets = Integer.valueOf(amountOfTicketsAsString);

        String[] passengerFirstNames = request.getParameterValues(PASSENGER_FIRST_NAME_PARAMETERS);
        boolean isValidPassengerFirstNames = isValidStringParametersArray(amountOfTickets, passengerFirstNames,
                validationError, ORDER_PASSENGER_NAME_FIRST);
        if (!isValidPassengerFirstNames) {
            return false;
        }

        String[] passengerLastNames = request.getParameterValues(PASSENGER_LAST_NAME_PARAMETERS);
        boolean isValidPassengerLastNames = isValidStringParametersArray(amountOfTickets, passengerLastNames,
                validationError, ORDER_PASSENGER_NAME_LAST);
        if (!isValidPassengerLastNames) {
            return false;
        }

        String[] passengerPassportNumbers = request.getParameterValues(PASSENGER_PASSPORT_NUMBER_PARAMETERS);
        boolean isValidPassengerPassportNumbers = isValidStringParametersArray(amountOfTickets, passengerPassportNumbers,
                validationError, ORDER_PASSENGER_PASSPORT_NUMBER);
        if (!isValidPassengerPassportNumbers) {
            return false;
        }

        return true;
    }

    private boolean isValidStringParametersArray(Integer amountOfTickets, String[] stringParameters,
                                                 ValidationError validationError, String bundleKey) {
        for (int i = 0; i < amountOfTickets; i++) {
            String stringParameter = stringParameters[i];
            if (isNullOrEmpty(stringParameter, validationError)) {
                validationError.setParameterBundleKey(bundleKey);
                return false;
            }
        }
        return true;
    }
}
