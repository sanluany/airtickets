package com.epam.airtickets.builder.airport;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.City;

public class AirportBuilderImpl implements AirportBuilder {
    @Override
    public Airport build(Integer id, String code, String name, City city) {
        return new Airport(id,code,name,city);
    }
}
