package com.epam.airtickets.builder.airport;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.City;

public interface AirportBuilder {

    Airport build(Integer id,String code,String name,City city);
}
