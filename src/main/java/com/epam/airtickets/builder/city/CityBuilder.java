package com.epam.airtickets.builder.city;

import com.epam.airtickets.entity.flight.City;

public interface CityBuilder {
    City build(Integer id, String name);
}
