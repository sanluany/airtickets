package com.epam.airtickets.builder.city;

import com.epam.airtickets.entity.flight.City;

public class CityBuilderImpl implements CityBuilder {
    @Override
    public City build(Integer id, String name) {
        return new City(id,name);
    }
}
