package com.epam.airtickets.builder.timetable;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Flight;

import java.time.LocalDateTime;

public class TimetableBuilderImpl implements TimetableBuilder {

    @Override
    public Timetable build(Flight flight, LocalDateTime departureDate, LocalDateTime arrivalDate) {
        return new Timetable(flight,departureDate,arrivalDate);
    }

    @Override
    public Timetable build(Integer id, Flight flight, LocalDateTime departureDate, LocalDateTime arrivalDate, Boolean isDeleted) {
        return new Timetable(id,flight,departureDate,arrivalDate,isDeleted);
    }
}
