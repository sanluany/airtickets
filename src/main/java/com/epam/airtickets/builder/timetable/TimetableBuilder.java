package com.epam.airtickets.builder.timetable;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Flight;

import java.time.LocalDateTime;

public interface TimetableBuilder {
    Timetable build(Flight flight, LocalDateTime departureDate, LocalDateTime arrivalDate);
    Timetable build(Integer id, Flight flight, LocalDateTime departureDate, LocalDateTime arrivalDate, Boolean isDeleted);
}
