package com.epam.airtickets.builder.ticket;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.ticket.Ticket;

import java.time.LocalDate;

public class TicketBuilderImpl implements TicketBuilder {

    @Override
    public Ticket build(Timetable timetable, TravelClass travelClass, String passengerFirstName,
                        String passengerLastName, String passengerPassportNumber, LocalDate passengerDateOfBirth) {
        return new Ticket(timetable, travelClass, passengerFirstName, passengerLastName, passengerPassportNumber,
                passengerDateOfBirth);
    }

    @Override
    public Ticket build(Integer id, Timetable timetable, TravelClass travelClass, String passengerFirstName,
                        String passengerLastName, String passengerPassportNumber, LocalDate passengerDateOfBirth) {
        return new Ticket(id, timetable, travelClass, passengerFirstName, passengerLastName, passengerPassportNumber,
                passengerDateOfBirth);
    }
}
