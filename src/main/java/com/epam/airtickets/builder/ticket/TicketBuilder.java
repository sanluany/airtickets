package com.epam.airtickets.builder.ticket;

import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.ticket.Ticket;

import java.time.LocalDate;

public interface TicketBuilder {
    Ticket build(Timetable timetable, TravelClass travelClass, String passengerFirstName, String passengerLastName,
                 String passengerPassportNumber, LocalDate passengerDateOfBirth);
    Ticket build(Integer id, Timetable timetable, TravelClass travelClass, String passengerFirstName,
                 String passengerLastName, String passengerPassportNumber, LocalDate passengerDateOfBirth);
}
