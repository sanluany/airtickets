package com.epam.airtickets.builder.user;

import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.entity.user.UserRole;

import java.math.BigDecimal;

public interface UserBuilder {
    User build(Integer id, String username, String password, UserRole userRole, BigDecimal balance);
}
