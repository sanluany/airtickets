package com.epam.airtickets.builder.user;

import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.entity.user.UserRole;

import java.math.BigDecimal;

public class UserBuilderImpl implements UserBuilder {
    @Override
    public User build(Integer id, String username, String password, UserRole userRole, BigDecimal balance) {
        return new User(id,username,password,userRole,balance);
    }
}
