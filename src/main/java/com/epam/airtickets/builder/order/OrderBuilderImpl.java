package com.epam.airtickets.builder.order;

import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.user.User;

import java.time.LocalDate;

public class OrderBuilderImpl implements OrderBuilder {
    @Override
    public Order build(User user, LocalDate orderDate) {
        return new Order(user,orderDate);
    }

    @Override
    public Order build(Integer id, User user, LocalDate orderDate) {
        return new Order(id,user,orderDate);
    }
}
