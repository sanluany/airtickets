package com.epam.airtickets.builder.order;

import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.user.User;

import java.time.LocalDate;

public interface OrderBuilder {
    Order build(User user, LocalDate orderDate);
    Order build(Integer id, User user, LocalDate orderDate);
}
