package com.epam.airtickets.builder.flightprice;

import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FlightPriceBuilderImpl implements FlightPriceBuilder {

    @Override
    public FlightPrice build(Integer id, Flight flight, TravelClass travelClass, BigDecimal price, LocalDate startDate,
                             LocalDate endDate, Boolean isDeleted) {
        return new FlightPrice(id, flight, travelClass, price, startDate, endDate, isDeleted);
    }

    @Override
    public FlightPrice build(Flight flight, TravelClass travelClass, BigDecimal price, LocalDate startDate, LocalDate endDate) {
        return new FlightPrice(flight, travelClass, price, startDate, endDate);
    }
}
