package com.epam.airtickets.builder.flightprice;

import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface FlightPriceBuilder {
    FlightPrice build(Integer id, Flight flight, TravelClass travelClass, BigDecimal price, LocalDate startDate,
                      LocalDate endDate, Boolean isDeleted);

    FlightPrice build(Flight flight, TravelClass travelClass, BigDecimal price, LocalDate startDate, LocalDate endDate);
}
