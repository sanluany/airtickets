package com.epam.airtickets.builder.flight;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.Flight;

public interface FlightBuilder {
    Flight build(String number, Airport originAirport, Airport destinationAirport, Integer economyClassTicketsAmount,
                 Integer businessClassTicketsAmount, Integer firstClassTicketsAmount);

    Flight build(Integer id, String number, Airport originAirport, Airport destinationAirport,
                 Integer economyClassTicketsAmount, Integer businessClassTicketsAmount, Integer firstClassTicketsAmount,
                 Boolean isDeleted);

}
