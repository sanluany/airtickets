package com.epam.airtickets.builder.flight;

import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.Flight;

public class FlightBuilderImpl implements FlightBuilder {

    @Override
    public Flight build(String number, Airport originAirport, Airport destinationAirport,
                        Integer economyClassTicketsAmount, Integer businessClassTicketsAmount, Integer firstClassTicketsAmount) {
        return new Flight(number, originAirport, destinationAirport, economyClassTicketsAmount,
                businessClassTicketsAmount, firstClassTicketsAmount);
    }

    @Override
    public Flight build(Integer id, String number, Airport originAirport, Airport destinationAirport,
                        Integer economyClassTicketsAmount, Integer businessClassTicketsAmount,
                        Integer firstClassTicketsAmount, Boolean isDeleted) {
        return new Flight(id,  number, originAirport, destinationAirport, economyClassTicketsAmount,
                businessClassTicketsAmount, firstClassTicketsAmount, isDeleted);
    }
}
