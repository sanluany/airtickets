package com.epam.airtickets.command.admin.flight;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.flight.FlightResultMessageCreator;
import com.epam.airtickets.creator.flight.FlightResultMessageCreatorImpl;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.flight.FlightService;
import com.epam.airtickets.service.flight.FlightServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class AddFlightCommand implements Command {
    private final static String AIRPORTS = "airports";

    private final static String FLIGHT_NUMBER = "flight_number";
    private final static String FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT = "flight_economy_class_tickets_amount";
    private final static String FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT = "flight_business_class_tickets_amount";
    private final static String FLIGHT_FIRST_CLASS_TICKETS_AMOUNT = "flight_first_class_tickets_amount";
    private final static String ORIGIN_AIRPORT = "origin_airport";
    private final static String DESTINATION_AIRPORT = "destination_airport";

    private final static String URL = "/flights?command=showFlights";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        List<Airport> airports = (List<Airport>) SessionHelper.getSessionAttribute(request, AIRPORTS);
        String number = request.getParameter(FLIGHT_NUMBER);

        String airportOfOriginIdAsString = request.getParameter(ORIGIN_AIRPORT);
        String destinationAirportIdAsString = request.getParameter(DESTINATION_AIRPORT);

        String economyClassTicketsAmountAsString = request.getParameter(FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT);
        String businessClassTicketsAmountAsString = request.getParameter(FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT);
        String firstClassTicketsAmountAsString = request.getParameter(FLIGHT_FIRST_CLASS_TICKETS_AMOUNT);

        FlightService flightService = new FlightServiceImpl();
        Flight flight = flightService.create(airports, number, airportOfOriginIdAsString, destinationAirportIdAsString,
                economyClassTicketsAmountAsString, businessClassTicketsAmountAsString, firstClassTicketsAmountAsString);
        int result = flightService.save(flight);

        Locale locale = SessionHelper.getUserLocale(request);
        FlightResultMessageCreator flightResultMessageCreator = new FlightResultMessageCreatorImpl();
        MessageDto resultMessage = flightResultMessageCreator.createForAddCommand(result,locale,flight);
        SessionHelper.setResultMessage(request, resultMessage);

        return URL;
    }


}

