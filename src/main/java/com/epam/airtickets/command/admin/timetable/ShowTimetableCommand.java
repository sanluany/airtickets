package com.epam.airtickets.command.admin.timetable;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.dto.TimetableDto;
import com.epam.airtickets.entity.flight.Timetable;

import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;

import com.epam.airtickets.service.flight.FlightService;
import com.epam.airtickets.service.flight.FlightServiceImpl;
import com.epam.airtickets.service.timetable.TimetableService;
import com.epam.airtickets.service.timetable.TimetableServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

public class ShowTimetableCommand implements Command {

    private final static String FLIGHT_ID = "flight_id";
    private final static String FLIGHT = "flight";
    private final static String TIMETABLES = "timetables";
    private final static String TIMETABLE_DTO_LIST = "timetableDtoList";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String flightIdAsString = request.getParameter(FLIGHT_ID);
        Integer flightId = Integer.parseInt(flightIdAsString);

        FlightService flightService = new FlightServiceImpl();
        Flight flight = flightService.getById(flightId);
        SessionHelper.setSessionAttribute(request, FLIGHT, flight);

        TimetableService timetableService = new TimetableServiceImpl();
        List<Timetable> timetables = timetableService.getAllByFlightId(flightId);
        SessionHelper.setSessionAttribute(request, TIMETABLES, timetables);

        List<TimetableDto> timetableDtoList = timetableService.getTimetableDtoList(timetables, flight);
        SessionHelper.setSessionAttribute(request, TIMETABLE_DTO_LIST, timetableDtoList);

        return request.getRequestURI();
    }


}
