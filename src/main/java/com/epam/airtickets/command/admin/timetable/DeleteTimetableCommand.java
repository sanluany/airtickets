package com.epam.airtickets.command.admin.timetable;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.timetable.TimetableResultMessageCreator;
import com.epam.airtickets.creator.timetable.TimetableResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.timetable.TimetableService;
import com.epam.airtickets.service.timetable.TimetableServiceImpl;
import com.epam.airtickets.utils.IdentifiableHelper;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class DeleteTimetableCommand implements Command {

    private final static Logger logger = LogManager.getFormatterLogger(DeleteTimetableCommand.class);

    private final static String ID = "id";
    private final static String TIMETABLES = "timetables";
    private final static String FLIGHT_ID = "flight_id";
    private final static String URL = "/timetable?command=showTimetable&flight_id=";

    @Override
    public String execute(HttpServletRequest request) {
        TimetableService timetableService = new TimetableServiceImpl();
        Timetable timetable = getSelectedTimetable(request);
        int result = 0;
        try {
            result = timetableService.delete(timetable);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(), e);
        }

        Locale locale = SessionHelper.getUserLocale(request);
        TimetableResultMessageCreator timetableResultMessageCreator = new TimetableResultMessageCreatorImpl();
        MessageDto resultMessage = timetableResultMessageCreator.createForDeleteCommand(result,locale,timetable);
        SessionHelper.setResultMessage(request,resultMessage);

        String flightIdAsString = request.getParameter(FLIGHT_ID);
        return URL + flightIdAsString;
    }

    private Timetable getSelectedTimetable(HttpServletRequest request) {
        String idAsString = request.getParameter(ID);
        Integer id = Integer.valueOf(idAsString);

        List<Timetable> timetables = (List<Timetable>) SessionHelper.getSessionAttribute(request, TIMETABLES);
        return (Timetable) IdentifiableHelper.getByIdFromList(timetables, id);
    }

}
