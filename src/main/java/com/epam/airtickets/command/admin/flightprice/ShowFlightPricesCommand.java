package com.epam.airtickets.command.admin.flightprice;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.flight.FlightService;
import com.epam.airtickets.service.flightprice.FlightPriceService;
import com.epam.airtickets.service.flightprice.FlightPriceServiceImpl;
import com.epam.airtickets.service.flight.FlightServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

public class ShowFlightPricesCommand implements Command {

    private final static String FLIGHT_ID = "flight_id";
    private final static String FLIGHT_PRICES = "flightPrices";
    private final static String FLIGHT = "flight";
    private final static String TRAVEL_CLASSES = "travelClasses";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String flightIdAsString = request.getParameter(FLIGHT_ID);
        Integer flightId = Integer.parseInt(flightIdAsString);

        FlightPriceService flightPriceService = new FlightPriceServiceImpl();
        List<FlightPrice> flightPrices = flightPriceService.getAllByFlightId(flightId);
        SessionHelper.setSessionAttribute(request, FLIGHT_PRICES, flightPrices);

        FlightService flightService = new FlightServiceImpl();
        Flight flight = flightService.getById(flightId);
        SessionHelper.setSessionAttribute(request, FLIGHT, flight);

        List<TravelClass> travelClasses = Arrays.asList(TravelClass.values());
        SessionHelper.setSessionAttribute(request, TRAVEL_CLASSES, travelClasses);

        return request.getRequestURI();
    }

}
