package com.epam.airtickets.command.admin.timetable;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.timetable.TimetableResultMessageCreator;
import com.epam.airtickets.creator.timetable.TimetableResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.timetable.TimetableService;
import com.epam.airtickets.service.timetable.TimetableServiceImpl;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class AddTimetableCommand implements Command {

    private final static Logger logger = LogManager.getFormatterLogger(AddTimetableCommand.class);

    private final static String FLIGHT = "flight";
    private final static String FIRST_FLIGHT_DATE = "first_flight_date";
    private final static String LAST_FLIGHT_DATE = "last_flight_date";
    private final static String DEPARTURE_TIME = "departure_time";
    private final static String ARRIVAL_TIME = "arrival_time";
    private final static String DAY = "day";
    private final static String FLIGHT_ID = "flight_id";

    private final static String URL = "/timetable?command=showTimetable&flight_id=";

    @Override
    public String execute(HttpServletRequest request) {

        Flight flight = (Flight) SessionHelper.getSessionAttribute(request, FLIGHT);
        String firstDateAsString = request.getParameter(FIRST_FLIGHT_DATE);
        String lastDateAsString = request.getParameter(LAST_FLIGHT_DATE);

        String departureTimeAsString = request.getParameter(DEPARTURE_TIME);
        String arrivalTimeAsString = request.getParameter(ARRIVAL_TIME);

        String[] days = request.getParameterValues(DAY);

        TimetableService timetableService = new TimetableServiceImpl();
        List<Timetable> timetables = timetableService.getGeneratedTimetable(flight, firstDateAsString,
                lastDateAsString, departureTimeAsString, arrivalTimeAsString, days);
        int result = 0;
        try {
            result = timetableService.save(timetables);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(), e);
        }

        Locale locale = SessionHelper.getUserLocale(request);
        TimetableResultMessageCreator timetableResultMessageCreator = new TimetableResultMessageCreatorImpl();
        MessageDto resultMessage = timetableResultMessageCreator.createForAddCommand(result,locale,flight,timetables);
        SessionHelper.setResultMessage(request, resultMessage);

        String flightIdAsString = request.getParameter(FLIGHT_ID);
        return URL + flightIdAsString;
    }


}
