package com.epam.airtickets.command.admin.flight;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.flight.FlightResultMessageCreator;
import com.epam.airtickets.creator.flight.FlightResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.flight.FlightService;
import com.epam.airtickets.service.flight.FlightServiceImpl;
import com.epam.airtickets.utils.IdentifiableHelper;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class DeleteFlightCommand implements Command {

    private final static Logger logger = LogManager.getFormatterLogger(DeleteFlightCommand.class);

    private final static String ID = "id";
    private final static String FLIGHTS = "flights";
    private final static String URL = "/flights?command=showFlights";

    @Override
    public String execute(HttpServletRequest request) {
        Flight flight = getSelectedFlight(request);
        int result = 0;
        FlightService flightService = new FlightServiceImpl();
        try {
            result = flightService.delete(flight);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(),e);
        }
        Locale locale = SessionHelper.getUserLocale(request);
        FlightResultMessageCreator flightResultMessageCreator = new FlightResultMessageCreatorImpl();
        MessageDto resultMessage = flightResultMessageCreator.createForDeleteCommand(result,locale,flight);
        SessionHelper.setResultMessage(request, resultMessage);

        return URL;
    }

    private Flight getSelectedFlight(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter(ID));
        List<Flight> flights = (List<Flight>) SessionHelper.getSessionAttribute(request, FLIGHTS);
        return (Flight) IdentifiableHelper.getByIdFromList(flights,id);
    }



}
