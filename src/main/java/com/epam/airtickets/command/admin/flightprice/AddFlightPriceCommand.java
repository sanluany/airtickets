package com.epam.airtickets.command.admin.flightprice;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.flightprice.FlightPriceResultMessageCreator;
import com.epam.airtickets.creator.flightprice.FlightPriceResultMessageCreatorImpl;
import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.flightprice.FlightPriceService;
import com.epam.airtickets.service.flightprice.FlightPriceServiceImpl;
import com.epam.airtickets.utils.IdentifiableHelper;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class AddFlightPriceCommand implements Command {

    private final static Logger logger = LogManager.getFormatterLogger(AddFlightPriceCommand.class);

    private final static String FLIGHT_ID = "flight_id";
    private final static String FLIGHTS = "flights";
    private final static String TRAVEL_CLASS = "travel_class";
    private final static String PRICE = "price";
    private final static String START_DATE = "start_date";
    private final static String END_DATE = "end_date";

    private final static String URL = "/flightprice?command=showFlightPrice&flight_id=";

    @Override
    public String execute(HttpServletRequest request) {
        Integer flightId = Integer.valueOf(request.getParameter(FLIGHT_ID));
        List<Identifiable> flights = (List<Identifiable>) SessionHelper.getSessionAttribute(request, FLIGHTS);
        Flight flight = (Flight) IdentifiableHelper.getByIdFromList(flights, flightId);

        String travelClassAsString = request.getParameter(TRAVEL_CLASS);
        String priceAsString = request.getParameter(PRICE);

        String startDateAsString = request.getParameter(START_DATE);
        String endDateAsString = request.getParameter(END_DATE);

        FlightPriceService flightPriceService = new FlightPriceServiceImpl();
        FlightPrice flightPrice = flightPriceService.create(flight, travelClassAsString, priceAsString, startDateAsString,
                endDateAsString);

        int result = 0;
        try {
            result = flightPriceService.save(flightPrice);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(), e);
        }

        Locale locale = SessionHelper.getUserLocale(request);
        FlightPriceResultMessageCreator flightPriceResultMessageCreator = new FlightPriceResultMessageCreatorImpl();
        MessageDto resultMessage = flightPriceResultMessageCreator.createForAddCommand(result,locale,flightPrice);
        SessionHelper.setResultMessage(request, resultMessage);

        return URL + flightId;
    }



}
