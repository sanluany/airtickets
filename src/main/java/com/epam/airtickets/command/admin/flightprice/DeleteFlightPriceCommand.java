package com.epam.airtickets.command.admin.flightprice;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.flightprice.FlightPriceResultMessageCreator;
import com.epam.airtickets.creator.flightprice.FlightPriceResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.flightprice.FlightPriceService;
import com.epam.airtickets.service.flightprice.FlightPriceServiceImpl;
import com.epam.airtickets.utils.IdentifiableHelper;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class DeleteFlightPriceCommand implements Command {
    private final static Logger logger = LogManager.getFormatterLogger(DeleteFlightPriceCommand.class);

    private final static String ID = "id";
    private final static String FLIGHT_PRICES = "flightPrices";
    private final static String FLIGHT_ID = "flight_id";
    private final static String URL = "/flightprice?command=showFlightPrice&flight_id=";

    @Override
    public String execute(HttpServletRequest request) {
        FlightPrice flightPrice = getSelectedFlightPrice(request);
        FlightPriceService flightPriceService = new FlightPriceServiceImpl();
        int result = 0;
        try {
            result = flightPriceService.delete(flightPrice);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(), e);
        }

        Locale locale = SessionHelper.getUserLocale(request);
        FlightPriceResultMessageCreator flightPriceResultMessageCreator = new FlightPriceResultMessageCreatorImpl();
        MessageDto resultMessage = flightPriceResultMessageCreator.createForDeleteCommand(result,locale,flightPrice);
        SessionHelper.setResultMessage(request, resultMessage);

        String flightId = request.getParameter(FLIGHT_ID);
        return URL + flightId;
    }

    private FlightPrice getSelectedFlightPrice(HttpServletRequest request) {
        Integer id = Integer.valueOf(request.getParameter(ID));
        List<FlightPrice> flightPrices = (List<FlightPrice>) SessionHelper.getSessionAttribute(request, FLIGHT_PRICES);
        return (FlightPrice) IdentifiableHelper.getByIdFromList(flightPrices, id);
    }

}
