package com.epam.airtickets.command.admin.flight;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.airport.AirportService;
import com.epam.airtickets.service.airport.AirportServiceImpl;
import com.epam.airtickets.service.flight.FlightServiceImpl;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowFlightsCommand implements Command {
    private final static Logger logger = LogManager.getFormatterLogger(ShowFlightsCommand.class);

    private final static String AIRPORTS = "airports";
    private final static String FLIGHTS = "flights";

    @Override
    public String execute(HttpServletRequest request) {
        addAirportsAttribute(request);
        addFlightsAttribute(request);
        return request.getRequestURI();
    }

    private void addAirportsAttribute(HttpServletRequest request) {
        AirportService airportService = new AirportServiceImpl();
        List<Airport> airports = null;
        try {
            airports = airportService.getAll();
        } catch (ServiceException e) {
            logger.warn(e.getMessage(),e);
        }
        SessionHelper.setSessionAttribute(request, AIRPORTS, airports);
    }

    private void addFlightsAttribute(HttpServletRequest request) {
        FlightServiceImpl flightServiceImpl = new FlightServiceImpl();
        List<Flight> flights = null;
        try {
            flights = flightServiceImpl.getAll();
        } catch (ServiceException e) {
            logger.warn(e.getMessage(),e);
        }
        SessionHelper.setSessionAttribute(request, FLIGHTS, flights);
    }

}
