package com.epam.airtickets.command.user.order;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.order.OrderResultMessageCreator;
import com.epam.airtickets.creator.order.OrderResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.service.NotEnoughFreeTicketsException;
import com.epam.airtickets.exception.service.NotEnoughMoneyException;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.exception.service.TimetableNotAvailableException;
import com.epam.airtickets.service.order.OrderService;
import com.epam.airtickets.service.order.OrderServiceImpl;
import com.epam.airtickets.service.ticket.TicketService;
import com.epam.airtickets.service.ticket.TicketServiceImpl;
import com.epam.airtickets.service.user.UserService;
import com.epam.airtickets.service.user.UserServiceImpl;
import com.epam.airtickets.utils.SessionHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

public class MakeOrderCommand implements Command {
    private final static Logger logger = LogManager.getFormatterLogger(MakeOrderCommand.class);

    private final static String OFFER_DTO = "offerDto";
    private final static String TICKET_TRAVEL_CLASS = "ticket_travel_class";
    private final static String PASSENGER_FIRST_NAME = "passenger_first_name";
    private final static String PASSENGER_LAST_NAME = "passenger_last_name";
    private final static String PASSENGER_PASSPORT_NUMBER = "passenger_passport_number";
    private final static String PASSENGER_DATE_OF_BIRTH = "passenger_date_of_birth";
    private final static String USER = "user";

    private final static String FAIL_URL = "/order?offer_id=";
    private final static String COMMAND_SHOW_ORDER = "&command=showOrder";
    private final static String SUCCESS_URL = "/account?command=showAccount";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        OfferDto offerDto = (OfferDto) SessionHelper.getSessionAttribute(request, OFFER_DTO);
        Timetable timetable = offerDto.getTimetable();

        String[] travelClassesAsString = request.getParameterValues(TICKET_TRAVEL_CLASS);
        String[] passengerFirstNames = request.getParameterValues(PASSENGER_FIRST_NAME);
        String[] passengerLastNames = request.getParameterValues(PASSENGER_LAST_NAME);
        String[] passengerPassportNumbers = request.getParameterValues(PASSENGER_PASSPORT_NUMBER);
        String[] passengerDatesOfBirthAsString = request.getParameterValues(PASSENGER_DATE_OF_BIRTH);

        TicketService ticketService = new TicketServiceImpl();
        List<Ticket> tickets = ticketService.getTickets(timetable, travelClassesAsString, passengerFirstNames, passengerLastNames,
                passengerPassportNumbers, passengerDatesOfBirthAsString);

        User user = (User) SessionHelper.getSessionAttribute(request, USER);
        OrderService orderService = new OrderServiceImpl();
        Order order = orderService.createOrder(user);
        Integer result = null;
        OrderResultMessageCreator orderResultMessageCreator = new OrderResultMessageCreatorImpl();
        MessageDto resultMessage;
        Locale locale = SessionHelper.getUserLocale(request);
        try {
            result = orderService.save(order, tickets, offerDto);
        } catch (NotEnoughFreeTicketsException | NotEnoughMoneyException |TimetableNotAvailableException e) {
            String key = e.getMessage();
            resultMessage = orderResultMessageCreator.createForMakeOrderCommand(key, locale);
            SessionHelper.setResultMessage(request, resultMessage);
        }
        if (result != null && result != 0) {
            updateUserAttribute(request);
            resultMessage = orderResultMessageCreator.createForSuccessfullyPaidOrder(locale);
            SessionHelper.setResultMessage(request, resultMessage);
        } else {
            int offerId = offerDto.getId();
            return FAIL_URL + offerId + COMMAND_SHOW_ORDER;
        }
        return SUCCESS_URL;
    }


    private void updateUserAttribute(HttpServletRequest request) {
        User user = (User) SessionHelper.getSessionAttribute(request, USER);
        String username = user.getUsername();
        UserService userService = new UserServiceImpl();
        try {
            User updatedUser = userService.getUserByUsername(username);
            SessionHelper.setSessionAttribute(request, USER, updatedUser);
        } catch (ServiceException e) {
            logger.warn(e.getMessage(),e);
        }
    }
}
