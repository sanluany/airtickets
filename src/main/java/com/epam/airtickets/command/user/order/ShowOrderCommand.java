package com.epam.airtickets.command.user.order;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.utils.IdentifiableHelper;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowOrderCommand implements Command {

    private final static String OFFER_ID = "offer_id";
    private final static String OFFER_DTO_LIST= "offerDtoList";
    private final static String OFFER_DTO = "offerDto";

    @Override
    public String execute(HttpServletRequest request) {
        String offerIdAsString = request.getParameter(OFFER_ID);
        Integer offerId = Integer.valueOf(offerIdAsString);

        List<OfferDto> offerDtoList = (List<OfferDto>) SessionHelper.getSessionAttribute(request,OFFER_DTO_LIST);
        OfferDto offerDto = (OfferDto) IdentifiableHelper.getByIdFromList(offerDtoList,offerId);
        SessionHelper.setSessionAttribute(request,OFFER_DTO,offerDto);

        return request.getRequestURI();
    }
}
