package com.epam.airtickets.command.user;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.ticket.Ticket;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.order.OrderService;
import com.epam.airtickets.service.order.OrderServiceImpl;
import com.epam.airtickets.service.ticket.TicketService;
import com.epam.airtickets.service.ticket.TicketServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;


public class ShowAccount implements Command {

    private final static String USER = "user";
    private final static String ORDER_TICKETS_MAP = "orderTicketsMap";
    private final static String ORDER_TOTAL_MAP = "orderTotalMap";

    private final static String URL = "/account";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        User user = (User) SessionHelper.getSessionAttribute(request, USER);
        Integer userId = user.getId();

        OrderService orderService = new OrderServiceImpl();
        List<Order> orders = orderService.getAllByUserId(userId);

        TicketService ticketService = new TicketServiceImpl();
        Map<Order, List<Ticket>> orderTicketsMap = ticketService.getOrderTicketsMap(orders);

        Map<Order, BigDecimal> orderTotalMap = orderService.getOrderTotalMap(orderTicketsMap);

        SessionHelper.setSessionAttribute(request, ORDER_TICKETS_MAP, orderTicketsMap);
        SessionHelper.setSessionAttribute(request, ORDER_TOTAL_MAP, orderTotalMap);
        return URL ;
    }


}
