package com.epam.airtickets.command.user.flight;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.flight.FlightResultMessageCreator;
import com.epam.airtickets.creator.flight.FlightResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.dto.OfferDto;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.offer.OfferService;
import com.epam.airtickets.service.offer.OfferServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class SearchFlightsCommand implements Command {

    private final static String CITIES = "cities";
    private final static String ORIGIN_CITY= "origin_city";
    private final static String DESTINATION_CITY= "destination_city";
    private final static String DEPARTURE_DATE_= "departure_date";

    private final static String OFFER_DTO_LIST= "offerDtoList";
    private final static String OFFER_DTO_LIST_FOR_REQUEST= "offerDtoListForRequest";

    private final static String URL= "/main";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        List<City> cities = (List<City>) SessionHelper.getSessionAttribute(request, CITIES);

        String originCityIdAsString = request.getParameter(ORIGIN_CITY);
        String destinationCityIdAsString = request.getParameter(DESTINATION_CITY);
        String departureDateAsString = request.getParameter(DEPARTURE_DATE_);

        OfferService offerService = new OfferServiceImpl();
        List<OfferDto> offerDtoList = offerService.getOffers(cities, originCityIdAsString, destinationCityIdAsString, departureDateAsString);

        if (offerDtoList == null || offerDtoList.size() == 0) {
            Locale locale = SessionHelper.getUserLocale(request);
            FlightResultMessageCreator flightResultMessageCreator = new FlightResultMessageCreatorImpl();
            MessageDto resultMessage = flightResultMessageCreator.createForSearchCommand(locale);
            SessionHelper.setResultMessage(request, resultMessage);
        } else {
            SessionHelper.setSessionAttribute(request, OFFER_DTO_LIST, offerDtoList);
            SessionHelper.setFlashAttribute(request, OFFER_DTO_LIST_FOR_REQUEST, offerDtoList);
        }

        return URL;
    }




}
