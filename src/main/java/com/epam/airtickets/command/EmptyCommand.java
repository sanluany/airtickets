package com.epam.airtickets.command;

import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.city.CityServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class EmptyCommand implements Command {

    private final static String CITIES = "cities";
    private final static String IS_LOGGED_IN = "isLoggedIn";
    private final static String URL = "/main";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        CityServiceImpl cityServiceImpl = new CityServiceImpl();
        List<City> cities = cityServiceImpl.getAll();

        SessionHelper.setSessionAttribute(request, CITIES, cities);

        boolean isLoggedIn = SessionHelper.isLoggedIn(request);
        SessionHelper.setSessionAttribute(request, IS_LOGGED_IN, isLoggedIn);

        return URL;
    }
}
