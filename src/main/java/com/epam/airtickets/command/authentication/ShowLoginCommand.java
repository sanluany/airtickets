package com.epam.airtickets.command.authentication;

import com.epam.airtickets.command.Command;

import javax.servlet.http.HttpServletRequest;

public class ShowLoginCommand implements Command {

    private final static String LOGIN = "/login";

    @Override
    public String execute(HttpServletRequest request) {
        return LOGIN ;
    }
}
