package com.epam.airtickets.command.authentication;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

public class LogOutCommand implements Command {

    private final static String USER = "user";
    private final static String URL = "/main";

    @Override
    public String execute(HttpServletRequest request) {
        User user = (User) SessionHelper.getSessionAttribute(request,USER);
        if(user!=null){
            SessionHelper.removeSessionAttribute(request,USER);
        }
        return URL;
    }
}
