package com.epam.airtickets.command.authentication;

import com.epam.airtickets.command.Command;
import com.epam.airtickets.creator.login.LoginResultMessageCreator;
import com.epam.airtickets.creator.login.LoginResultMessageCreatorImpl;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.user.User;

import com.epam.airtickets.exception.service.ServiceException;
import com.epam.airtickets.service.user.UserService;
import com.epam.airtickets.service.user.UserServiceImpl;
import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class LogInCommand implements Command {

    private final static String DEFAULT = "/";
    private final static String USERNAME = "username";
    private final static String PASSWORD = "password";
    private final static String USER = "user";

    private final static String URL = "/login?command=showLogin";

    public String execute(HttpServletRequest request) throws ServiceException {
        String username = request.getParameter(USERNAME);
        String password = request.getParameter(PASSWORD);

        UserService userService = new UserServiceImpl();
        User user = userService.logIn(username, password);

        if (user != null) {
            SessionHelper.setSessionAttribute(request,USER,user);
            return DEFAULT;
        }

        Locale locale = SessionHelper.getUserLocale(request);
        LoginResultMessageCreator loginResultMessageCreator = new LoginResultMessageCreatorImpl();
        MessageDto resultMessage = loginResultMessageCreator.createForLogInCommand(locale);
        SessionHelper.setResultMessage(request, resultMessage);

        return URL;
    }


}
