package com.epam.airtickets.command;

import com.epam.airtickets.exception.service.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    String execute(HttpServletRequest request) throws ServiceException;
}
