package com.epam.airtickets.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;

public class DateUtils {

    private final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private final static String DATE_PATTERN = "yyyy-MM-dd";
    private final static String TIME_PATTERN = "HH:mm";
    private final static String FORMATTED_DATE_TIME_PATTERN = "dd-MM-yyyy HH:mm";

    public static LocalDateTime parseDateTime(String dateAsString) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
        return LocalDateTime.parse(dateAsString, dateFormat);
    }

    public static LocalDate parseDate(String dateAsString) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(DATE_PATTERN);
        return LocalDate.parse(dateAsString, dateFormat);
    }

    public static LocalTime parseTime(String dateAsString) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(TIME_PATTERN);
        return LocalTime.parse(dateAsString, dateFormat);
    }

    public static String getFormattedDate(Temporal temporal) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDate date = LocalDate.from(temporal);
        return date.format(dateFormat);
    }

    public static String getFormattedTime(Temporal temporal) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(TIME_PATTERN);
        LocalTime date = LocalTime.from(temporal);
        return date.format(dateFormat);
    }

    public static String getFormattedDateTime(Temporal temporal) {
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(FORMATTED_DATE_TIME_PATTERN);
        LocalDateTime date = LocalDateTime.from(temporal);
        return date.format(dateFormat);
    }

}
