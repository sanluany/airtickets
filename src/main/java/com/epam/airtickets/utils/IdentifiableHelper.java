package com.epam.airtickets.utils;

import com.epam.airtickets.entity.Identifiable;

import java.util.List;

public class IdentifiableHelper {

    public static Identifiable getByIdFromList(List<? extends Identifiable> identifiableList, int id) {
        for (Identifiable identifiable : identifiableList) {
            if (identifiable.getId().equals(id)) {
                return identifiable;
            }
        }
        return null;
    }
}
