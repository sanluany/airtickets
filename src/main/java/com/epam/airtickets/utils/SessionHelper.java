package com.epam.airtickets.utils;

import com.epam.airtickets.dto.MessageDto;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.Map;

public class SessionHelper {

    private final static String USER = "user";
    private final static String RESULT_MESSAGE = "resultMessage";
    private final static String LANGUAGE = "language";
    private final static String FLASH_SCOPE = "flashScope";

    public static boolean isLoggedIn(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        return session != null && session.getAttribute(USER) != null;
    }

    public static Object getSessionAttribute(HttpServletRequest request, String name) {
        HttpSession session = request.getSession(false);
        return session.getAttribute(name);
    }

    public static void setSessionAttribute(ServletRequest request, String name, Object object) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute(name, object);
    }

    public static void setResultMessage(HttpServletRequest request,MessageDto message){
        setFlashAttribute(request,RESULT_MESSAGE,message);
    }

    public static void removeSessionAttribute(HttpServletRequest request, String name) {
        HttpSession session = request.getSession();
        session.removeAttribute(name);
    }


    public static Locale getUserLocale(HttpServletRequest request) {
        String lang = (String) getSessionAttribute(request, LANGUAGE);
        return Locale.forLanguageTag(lang);
    }

   public static void setFlashAttribute(HttpServletRequest request, String name, Object object){
       HttpSession session = request.getSession();
       Object flashAttributes = session.getAttribute(FLASH_SCOPE);
       Map<String, Object> flashAttributesMap = (Map<String, Object>) flashAttributes;
       flashAttributesMap.put(name,object);
   }
}
