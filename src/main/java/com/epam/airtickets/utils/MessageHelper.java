package com.epam.airtickets.utils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class MessageHelper {

    public static final String BUNDLE_MESSAGES = "messages";
    public static final String BUNDLE_VALIDATION = "validation";
    public static final String BUNDLE_PARAMETERS = "parameters";

    public static String getMessage(String bundleName, String lineName, Locale locale, String... parameters) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(bundleName, locale);
        String message = resourceBundle.getString(lineName);
        if (parameters != null && parameters.length > 0) {
            return MessageFormat.format(message, parameters);
        }
        return message;
    }

}
