package com.epam.airtickets.factory;

import com.epam.airtickets.dao.*;
import com.epam.airtickets.dao.airport.AirportDaoImpl;
import com.epam.airtickets.dao.city.CityDaoImpl;
import com.epam.airtickets.dao.flight.FlightDaoImpl;
import com.epam.airtickets.dao.flightprice.FlightPriceDaoImpl;
import com.epam.airtickets.dao.order.OrderDaoImpl;
import com.epam.airtickets.dao.ticket.TicketDaoImpl;
import com.epam.airtickets.dao.timetable.TimetableDaoImpl;
import com.epam.airtickets.dao.user.UserDaoImpl;
import com.epam.airtickets.handler.resultset.ResultSetHandler;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;

public class DaoFactory implements Closeable {

    private final Connection connection;
    private final ResultSetHandlerFactory resultSetHandlerFactory = new ResultSetHandlerFactory();

    public DaoFactory() {
        connection = ConnectionPool.getConnectionPool().getConnection();
    }

    public UserDaoImpl getUserDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getUserResultSetHandler();
        return new UserDaoImpl(connection, resultSetHandler);
    }

    public FlightDaoImpl getFlightDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getFlightResultSetHandler();
        return new FlightDaoImpl(connection, resultSetHandler);
    }

    public AirportDaoImpl getAirportDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getOriginAirportResultSetHandler();
        return new AirportDaoImpl(connection, resultSetHandler);
    }

    public TimetableDaoImpl getTimetableDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getTimetableResultSetHandler();
        return new TimetableDaoImpl(connection, resultSetHandler);
    }

    public FlightPriceDaoImpl getFlightPriceDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getFlightPriceResultSetHandler();
        return new FlightPriceDaoImpl(connection, resultSetHandler);
    }

    public CityDaoImpl getCityDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getOriginCityResultSetHandler();
        return new CityDaoImpl(connection, resultSetHandler);
    }

    public TicketDaoImpl getTicketDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getTicketResultSetHandler();
        return new TicketDaoImpl(connection, resultSetHandler);
    }

    public OrderDaoImpl getOrderDao() {
        ResultSetHandler resultSetHandler = resultSetHandlerFactory.getOrderResultSetHandler();
        return new OrderDaoImpl(connection, resultSetHandler);
    }

    @Override
    public void close() {
        ConnectionPool.getConnectionPool().releaseConnection(connection);
    }

    public void commit() {
        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
