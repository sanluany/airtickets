package com.epam.airtickets.factory;

import com.epam.airtickets.handler.flash.*;

public class FlashAttributeHandlerFactory {

    private final static String DEFAULT = "/main";
    private final static String LOGIN = "/login";
    private final static String ORDER = "/order";
    private final static String FLIGHTS = "/flights";
    private final static String FLIGHT_PRICES = "/flightprice";
    private final static String TIMETABLE = "/timetable";

    public FlashAttributeHandler create(String URI) {
        switch (URI) {
            case DEFAULT: {
                return new DefaultFlashAttributeHandler();
            }
            case LOGIN: {
                return new LoginFlashAttributeHandler();
            }
            case ORDER: {
                return new OrderFlashAttributeHandler();
            }
            case FLIGHTS: {
                return new FlightsFlashAttributeHandler();
            }
            case FLIGHT_PRICES: {
                return new FlightPriceFlashAttributeHandler();
            }
            case TIMETABLE: {
                return new TimetableFlashAttributeHandler();
            }
        }
        return null;
    }
}
