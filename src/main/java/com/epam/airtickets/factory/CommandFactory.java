package com.epam.airtickets.factory;

import com.epam.airtickets.command.*;
import com.epam.airtickets.command.admin.flight.AddFlightCommand;
import com.epam.airtickets.command.admin.flight.DeleteFlightCommand;
import com.epam.airtickets.command.admin.flight.RecoverFlightCommand;
import com.epam.airtickets.command.admin.flight.ShowFlightsCommand;
import com.epam.airtickets.command.admin.flightprice.AddFlightPriceCommand;
import com.epam.airtickets.command.admin.flightprice.DeleteFlightPriceCommand;
import com.epam.airtickets.command.admin.flightprice.RecoverFlightPriceCommand;
import com.epam.airtickets.command.admin.flightprice.ShowFlightPricesCommand;
import com.epam.airtickets.command.admin.timetable.AddTimetableCommand;
import com.epam.airtickets.command.admin.timetable.DeleteTimetableCommand;
import com.epam.airtickets.command.admin.timetable.RecoverTimetableCommand;
import com.epam.airtickets.command.admin.timetable.ShowTimetableCommand;
import com.epam.airtickets.command.authentication.LogInCommand;
import com.epam.airtickets.command.authentication.LogOutCommand;
import com.epam.airtickets.command.authentication.ShowLoginCommand;
import com.epam.airtickets.command.user.ShowAccount;
import com.epam.airtickets.command.user.flight.SearchFlightsCommand;
import com.epam.airtickets.command.user.order.MakeOrderCommand;
import com.epam.airtickets.command.user.order.ShowOrderCommand;

public class CommandFactory {
    private final static String SHOW_LOGIN = "showLogin";
    private final static String LOG_IN = "logIn";
    private final static String LOG_OUT = "logOut";

    private final static String ADD_FLIGHT = "addFlight";
    private final static String SHOW_FLIGHTS = "showFlights";
    private final static String DELETE_FLIGHT = "deleteFlight";
    private final static String RECOVER_FLIGHT = "recoverFlight";

    private final static String ADD_TIMETABLE = "addTimetable";
    private final static String SHOW_TIMETABLE = "showTimetable";
    private final static String DELETE_TIMETABLE = "deleteTimetable";
    private final static String RECOVER_TIMETABLE = "recoverTimetable";

    private final static String ADD_FLIGHT_PRICE= "addFlightPrice";
    private final static String SHOW_FLIGHT_PRICE = "showFlightPrice";
    private final static String DELETE_FLIGHT_PRICE = "deleteFlightPrice";
    private final static String RECOVER_FLIGHT_PRICE = "recoverFlightPrice";

    private final static String SEARCH_FLIGHTS = "searchFlights";
    private final static String SHOW_ORDER = "showOrder";
    private final static String MAKE_ORDER = "makeOrder";

    private final static String SHOW_ACCOUNT = "showAccount";


    public Command getCommandByName(String name) {
        switch (name) {
            case SHOW_LOGIN:{
                return new ShowLoginCommand();
            }
            case LOG_IN: {
                return new LogInCommand();
            }
            case LOG_OUT:{
                return new LogOutCommand();
            }
            case ADD_FLIGHT: {
                return new AddFlightCommand();
            }
            case SHOW_FLIGHTS: {
                return new ShowFlightsCommand();
            }
            case DELETE_FLIGHT: {
                return new DeleteFlightCommand();
            }
            case RECOVER_FLIGHT: {
                return new RecoverFlightCommand();
            }
            case ADD_TIMETABLE: {
                return new AddTimetableCommand();
            }
            case SHOW_TIMETABLE: {
                return new ShowTimetableCommand();
            }
            case DELETE_TIMETABLE: {
                return new DeleteTimetableCommand();
            }
            case RECOVER_TIMETABLE: {
                return new RecoverTimetableCommand();
            }
            case ADD_FLIGHT_PRICE:{
                return new AddFlightPriceCommand();
            }
            case SHOW_FLIGHT_PRICE:{
                return new ShowFlightPricesCommand();
            }
            case DELETE_FLIGHT_PRICE:{
                return new DeleteFlightPriceCommand();
            }
            case RECOVER_FLIGHT_PRICE:{
                return new RecoverFlightPriceCommand();
            }
            case SEARCH_FLIGHTS:{
                return new SearchFlightsCommand();
            }
            case SHOW_ORDER:{
                return new ShowOrderCommand();
            }
            case MAKE_ORDER:{
                return new MakeOrderCommand();
            }
            case SHOW_ACCOUNT:{
                return new ShowAccount();
            }
            default: {
                return new EmptyCommand();
            }
        }
    }
}
