package com.epam.airtickets.factory;

import com.epam.airtickets.handler.resultset.*;


public class ResultSetHandlerFactory {

    private final static String ORIGIN_CITY_ID = "origin_city_id";
    private final static String ORIGIN_CITY_NAME = "origin_city_name";
    private final static String DESTINATION_CITY_ID = "destination_city_id";
    private final static String DESTINATION_CITY_NAME = "destination_city_name";

    private final static String ORIGIN_AIRPORT_ID = "origin_airport_id";
    private final static String ORIGIN_AIRPORT_CODE = "origin_airport_code";
    private final static String ORIGIN_AIRPORT_NAME = "origin_airport_name";
    private final static String DESTINATION_AIRPORT_ID = "destination_airport_id";
    private final static String DESTINATION_AIRPORT_CODE = "destination_airport_code";
    private final static String DESTINATION_AIRPORT_NAME = "destination_airport_name";

    public CityResultSetHandler getOriginCityResultSetHandler() {
        return new CityResultSetHandler(ORIGIN_CITY_ID, ORIGIN_CITY_NAME);
    }

    private CityResultSetHandler getDestinationCityResultSetHandler() {
        return new CityResultSetHandler(DESTINATION_CITY_ID, DESTINATION_CITY_NAME);
    }

    public AirportResultSetHandler getOriginAirportResultSetHandler() {
        CityResultSetHandler originCityResultSetHandler = getOriginCityResultSetHandler();
        return new AirportResultSetHandler(ORIGIN_AIRPORT_ID, ORIGIN_AIRPORT_CODE, ORIGIN_AIRPORT_NAME,
                originCityResultSetHandler);
    }

    private AirportResultSetHandler getDestinationAirportResultSetHandler() {
        CityResultSetHandler destinationCityResultSetHandler = getDestinationCityResultSetHandler();
        return new AirportResultSetHandler(DESTINATION_AIRPORT_ID, DESTINATION_AIRPORT_CODE, DESTINATION_AIRPORT_NAME,
                destinationCityResultSetHandler);
    }

    public FlightResultSetHandler getFlightResultSetHandler() {
        AirportResultSetHandler originAirportResultSetHandler = getOriginAirportResultSetHandler();
        AirportResultSetHandler destinationAirportResultSetHandler = getDestinationAirportResultSetHandler();
        return new FlightResultSetHandler(originAirportResultSetHandler, destinationAirportResultSetHandler);
    }

    public TimetableResultSetHandler getTimetableResultSetHandler() {
        FlightResultSetHandler flightResultSetHandler = getFlightResultSetHandler();
        return new TimetableResultSetHandler(flightResultSetHandler);
    }

    public FlightPriceResultSetHandler getFlightPriceResultSetHandler() {
        FlightResultSetHandler flightResultSetHandler = getFlightResultSetHandler();
        return new FlightPriceResultSetHandler(flightResultSetHandler);
    }

    public UserResultSetHandler getUserResultSetHandler() {
        return new UserResultSetHandler();
    }

    public OrderResultSetHandler getOrderResultSetHandler() {
        UserResultSetHandler userResultSetHandler = getUserResultSetHandler();
        return new OrderResultSetHandler(userResultSetHandler);
    }

    public TicketResultSetHandler getTicketResultSetHandler() {
        TimetableResultSetHandler timetableResultSetHandler = getTimetableResultSetHandler();
        return new TicketResultSetHandler(timetableResultSetHandler);
    }
}
