package com.epam.airtickets.factory;

import com.epam.airtickets.validator.*;

public class ValidatorFactory {

    public final static String FLIGHT_VALIDATOR = "flightValidator";
    public final static String TIMETABLE_VALIDATOR = "timetableValidator";
    public final static String FLIGHT_PRICE_VALIDATOR = "flightPriceValidator";
    public final static String LOGIN_VALIDATOR = "loginValidator";
    public final static String ORDER_VALIDATOR = "orderValidator";

    public Validator getValidator(String name) {
        switch (name) {
            case FLIGHT_VALIDATOR: {
                return new FlightValidator();
            }
            case TIMETABLE_VALIDATOR: {
                return new TimetableValidator();
            }
            case FLIGHT_PRICE_VALIDATOR: {
                return new FlightPriceValidator();
            }
            case LOGIN_VALIDATOR: {
                return new LoginValidator();
            }
            case ORDER_VALIDATOR: {
                return new OrderValidator();
            }
        }
        return null;
    }
}
