package com.epam.airtickets.filter;


import com.epam.airtickets.factory.FlashAttributeHandlerFactory;
import com.epam.airtickets.handler.flash.FlashAttributeHandler;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

public class FlashScopeFilter implements Filter {

    private final static String FLASH_SCOPE = "flashScope";

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        Object flashAttributes = session.getAttribute(FLASH_SCOPE);

        Map<String, Object> flashAttributesMap;
        if (flashAttributes != null) {
            flashAttributesMap = (Map<String, Object>) flashAttributes;
            moveFlashAttributesToRequest(request, flashAttributesMap);
            flashAttributesMap.clear();
        } else {
            flashAttributesMap = new HashMap<>();
            session.setAttribute(FLASH_SCOPE, flashAttributesMap);
        }

        filterChain.doFilter(servletRequest, servletResponse);

        Map map = request.getParameterMap();
        int amountOfParameters = map.size();
        if (amountOfParameters > 1) {
            String URI = request.getRequestURI();
            FlashAttributeHandlerFactory flashAttributeHandlerFactory = new FlashAttributeHandlerFactory();
            FlashAttributeHandler flashAttributeHandler = flashAttributeHandlerFactory.create(URI);
            flashAttributeHandler.handle(request);
        }

    }

    private void moveFlashAttributesToRequest(HttpServletRequest request, Map<String, Object> flashAttributesMap) {
        Set<Map.Entry<String, Object>> flashAttributesSet = flashAttributesMap.entrySet();
        for (Map.Entry<String, Object> flashAttribute : flashAttributesSet) {
            String name = flashAttribute.getKey();
            Object attribute = flashAttribute.getValue();
            request.setAttribute(name, attribute);
        }
    }

    @Override
    public void destroy() {

    }
}
