package com.epam.airtickets.filter;


import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.entity.user.UserRole;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    private final static String USER = "user";
    private final static String FLIGHTS = "/flights";
    private final static String FLIGHT_PRICE = "/flightprice";
    private final static String TIMETABLE = "/timetable";

    private final static String LOGIN = "/login";
    private final static String MAIN = "/main";
    private final static String DEFAULT = "/";


    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
        HttpSession session = httpRequest.getSession();

        boolean isLoggedIn = session !=null && session.getAttribute(USER) != null;
        User user;
        UserRole userRole = null;
        if (isLoggedIn) {
            user = (User) session.getAttribute(USER);
            userRole = user.getUserRole();
        }

        String requestURI = httpRequest.getRequestURI();
        boolean loginRequired = isLoginRequired(requestURI);
        boolean isAdminPrivilegeRequired = isAdminPrivilegeRequired(requestURI);

        if (loginRequired && !isLoggedIn || (isAdminPrivilegeRequired && userRole != UserRole.ADMIN)) {
            httpResponse.sendRedirect(DEFAULT);
        } else {
            filterChain.doFilter(httpRequest, httpResponse);
        }
    }

    private boolean isLoginRequired(String requestURI) {
        return !(requestURI.equals(LOGIN) || requestURI.equals(DEFAULT) || requestURI.equals(MAIN));
    }
    private boolean isAdminPrivilegeRequired(String requestURI){
        return requestURI.equals(FLIGHTS) || requestURI.equals(FLIGHT_PRICE) || requestURI.equals(TIMETABLE);
    }

    @Override
    public void destroy() {

    }
}
