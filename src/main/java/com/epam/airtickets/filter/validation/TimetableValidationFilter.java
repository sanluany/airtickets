package com.epam.airtickets.filter.validation;

import com.epam.airtickets.factory.ValidatorFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class TimetableValidationFilter extends AbstractValidationFilter {

    private final static String ADD_FLIGHT = "addTimetable";
    private final static String FLIGHT_ID = "flight_id";
    private final static String REDIRECT_URL_PREFIX = "?command=showTimetable&flight_id=";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String flightId = servletRequest.getParameter(FLIGHT_ID);
        String redirectURL = REDIRECT_URL_PREFIX + flightId;

        setValidatorName(ValidatorFactory.TIMETABLE_VALIDATOR);
        setCommandName(ADD_FLIGHT);
        setRedirectURL(redirectURL);
        super.doFilter(servletRequest, servletResponse, filterChain);
    }
}
