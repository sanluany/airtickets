package com.epam.airtickets.filter.validation;

import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.factory.ValidatorFactory;
import com.epam.airtickets.utils.MessageHelper;
import com.epam.airtickets.utils.SessionHelper;
import com.epam.airtickets.validator.ValidationError;
import com.epam.airtickets.validator.Validator;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;


public abstract class AbstractValidationFilter implements Filter {

    private final static String COMMAND = "command";
    private final static String RESULT_MESSAGE = "resultMessage";

    private String validatorName;
    private String commandName;
    private String redirectURL;

    private MessageDto getResultMessage(ValidationError validationError, Locale locale) {
        String parameterBundleKey = validationError.getParameterBundleKey();
        String validationBundleKey = validationError.getValidationBundleKey();

        StringBuilder result = new StringBuilder();
        String parameterMessage = null;
        if (parameterBundleKey != null) {
            parameterMessage = MessageHelper.getMessage(MessageHelper.BUNDLE_PARAMETERS, parameterBundleKey, locale);
            result.append(parameterMessage);
            result.append(" ");
        }

        if (validationBundleKey != null) {
            String validationMessage = MessageHelper.getMessage(MessageHelper.BUNDLE_VALIDATION, validationBundleKey, locale, parameterMessage);
            result.append(validationMessage);
        }
        return new MessageDto(true, result.toString());
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String command = servletRequest.getParameter(COMMAND);
        if (command != null && command.equals(commandName)) {
            ValidationError validationError = new ValidationError();

            ValidatorFactory validatorFactory = new ValidatorFactory();
            Validator validator = validatorFactory.getValidator(validatorName);

            boolean isValid = validator.validate(servletRequest, validationError);
            if (!isValid) {
                HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
                Locale locale = SessionHelper.getUserLocale(httpServletRequest);

                MessageDto resultMessage = getResultMessage(validationError, locale);
                SessionHelper.setFlashAttribute(httpServletRequest, RESULT_MESSAGE, resultMessage);

                HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
                httpServletResponse.sendRedirect(redirectURL);
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void destroy() {

    }

    void setValidatorName(String validatorName) { //package-private
        this.validatorName = validatorName;
    }

    void setCommandName(String commandName) { //package-private
        this.commandName = commandName;
    }

    void setRedirectURL(String redirectURL) {  //package-private
        this.redirectURL = redirectURL;
    }

}
