package com.epam.airtickets.filter.validation;

import com.epam.airtickets.factory.ValidatorFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class LoginValidationFilter extends AbstractValidationFilter {

    private final static String LOG_IN = "logIn";
    private final static String REDIRECT_URL = "/login?command=showLogin";


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        setCommandName(LOG_IN);
        setRedirectURL(REDIRECT_URL);
        setValidatorName(ValidatorFactory.LOGIN_VALIDATOR);
        super.doFilter(servletRequest, servletResponse, filterChain);
    }
}
