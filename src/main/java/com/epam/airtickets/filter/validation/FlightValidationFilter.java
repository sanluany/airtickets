package com.epam.airtickets.filter.validation;


import com.epam.airtickets.factory.ValidatorFactory;

import javax.servlet.*;
import java.io.IOException;

public class FlightValidationFilter extends AbstractValidationFilter {

    private final static String ADD_FLIGHT = "addFlight";
    private final static String REDIRECT_URL = "flights?command=showFlights";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        setValidatorName(ValidatorFactory.FLIGHT_VALIDATOR);
        setCommandName(ADD_FLIGHT);
        setRedirectURL(REDIRECT_URL);
        super.doFilter(servletRequest, servletResponse, filterChain);
    }

}
