package com.epam.airtickets.filter.validation;

import com.epam.airtickets.factory.ValidatorFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class FlightPriceValidationFilter extends AbstractValidationFilter {

    private final static String ADD_PRICE= "addFlightPrice";
    private final static String FLIGHT_ID = "flight_id";
    private final static String REDIRECT_URL_PREFIX = "?command=showFlightPrice&flight_id=";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String flightId = servletRequest.getParameter(FLIGHT_ID);
        String redirectURL = REDIRECT_URL_PREFIX + flightId;

        setValidatorName(ValidatorFactory.FLIGHT_PRICE_VALIDATOR);
        setCommandName(ADD_PRICE);
        setRedirectURL(redirectURL);
        super.doFilter(servletRequest, servletResponse, filterChain);
    }
}
