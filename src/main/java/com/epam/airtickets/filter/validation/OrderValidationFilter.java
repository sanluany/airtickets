package com.epam.airtickets.filter.validation;

import com.epam.airtickets.factory.ValidatorFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class OrderValidationFilter extends AbstractValidationFilter {

    private final static String MAKE_ORDER = "makeOrder";
    private final static String OFFER_ID = "offer_id";
    private final static String ORDER = "/order?offer_id=";
    private final static String COMMAND = "&command=showOrder";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        setValidatorName(ValidatorFactory.ORDER_VALIDATOR);
        setCommandName(MAKE_ORDER);
        String offerId = servletRequest.getParameter(OFFER_ID);
        String redirectURL = ORDER + offerId + COMMAND;
        setRedirectURL(redirectURL);
        super.doFilter(servletRequest, servletResponse, filterChain);
    }
}

