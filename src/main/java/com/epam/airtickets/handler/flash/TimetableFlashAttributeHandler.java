package com.epam.airtickets.handler.flash;

import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

public class TimetableFlashAttributeHandler implements FlashAttributeHandler {

    private final static String FIRST_FLIGHT_DATE_PARAMETER = "first_flight_date";
    private final static String LAST_FLIGHT_DATE_PARAMETER = "last_flight_date";
    private final static String DEPARTURE_TIME_PARAMETER = "departure_time";
    private final static String ARRIVAL_TIME_PARAMETER = "arrival_time";
    private final static String DAY_PARAMETER = "day";

    private final static String FIRST_FLIGHT_DATE_ATTRIBUTE = "firstFlightDate";
    private final static String LAST_FLIGHT_DATE_ATTRIBUTE = "lastFlightDate";
    private final static String DEPARTURE_TIME_ATTRIBUTE  = "departureTime";
    private final static String ARRIVAL_TIME_ATTRIBUTE = "arrivalTime";
    private final static String DAY_ATTRIBUTE  = "day";

    @Override
    public void handle(HttpServletRequest request) {
        String firstDateAsString = request.getParameter(FIRST_FLIGHT_DATE_PARAMETER);
        SessionHelper.setFlashAttribute(request,FIRST_FLIGHT_DATE_ATTRIBUTE,firstDateAsString);

        String lastDateAsString = request.getParameter(LAST_FLIGHT_DATE_PARAMETER);
        SessionHelper.setFlashAttribute(request,LAST_FLIGHT_DATE_ATTRIBUTE,lastDateAsString);

        String departureTimeAsString = request.getParameter(DEPARTURE_TIME_PARAMETER);
        SessionHelper.setFlashAttribute(request,DEPARTURE_TIME_ATTRIBUTE,departureTimeAsString);

        String arrivalTimeAsString = request.getParameter(ARRIVAL_TIME_PARAMETER);
        SessionHelper.setFlashAttribute(request,ARRIVAL_TIME_ATTRIBUTE,arrivalTimeAsString);

        String[] days = request.getParameterValues(DAY_PARAMETER);
        SessionHelper.setFlashAttribute(request,DAY_ATTRIBUTE,days);

    }

}
