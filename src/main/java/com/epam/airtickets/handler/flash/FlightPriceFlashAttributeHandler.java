package com.epam.airtickets.handler.flash;

import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

public class FlightPriceFlashAttributeHandler implements FlashAttributeHandler {

    private final static String TRAVEL_CLASS_PARAMETER = "travel_class";
    private final static String PRICE_PARAMETER = "price";
    private final static String START_DATE_PARAMETER = "start_date";
    private final static String END_DATE_PARAMETER = "end_date";

    private final static String TRAVEL_CLASS_ATTRIBUTE = "travelClass";
    private final static String PRICE_ATTRIBUTE= "price";
    private final static String START_DATE_ATTRIBUTE= "startDate";
    private final static String END_DATE_ATTRIBUTE= "endDate";

    @Override
    public void handle(HttpServletRequest request) {

        String travelClassAsString = request.getParameter(TRAVEL_CLASS_PARAMETER);
        SessionHelper.setFlashAttribute(request,TRAVEL_CLASS_ATTRIBUTE,travelClassAsString);

        String priceAsString = request.getParameter(PRICE_PARAMETER);
        SessionHelper.setFlashAttribute(request,PRICE_ATTRIBUTE,priceAsString);

        String startDateAsString = request.getParameter(START_DATE_PARAMETER);
        SessionHelper.setFlashAttribute(request,START_DATE_ATTRIBUTE,startDateAsString);

        String endDateAsString = request.getParameter(END_DATE_PARAMETER);
        SessionHelper.setFlashAttribute(request,END_DATE_ATTRIBUTE,endDateAsString);

    }
}
