package com.epam.airtickets.handler.flash;

import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

public class DefaultFlashAttributeHandler implements FlashAttributeHandler {
    private final static String ORIGIN_CITY_PARAMETER = "origin_city";
    private final static String DESTINATION_CITY_PARAMETER = "destination_city";
    private final static String DEPARTURE_DATE_PARAMETER = "departure_date";

    private final static String ORIGIN_CITY= "originCity";
    private final static String DESTINATION_CITY= "destinationCity";
    private final static String DEPARTURE_DATE= "departureDate";


    @Override
    public void handle(HttpServletRequest request) {
        String originCityIdAsString = request.getParameter(ORIGIN_CITY_PARAMETER);
        SessionHelper.setFlashAttribute(request,ORIGIN_CITY,originCityIdAsString);

        String destinationCityIdAsString = request.getParameter(DESTINATION_CITY_PARAMETER);
        SessionHelper.setFlashAttribute(request,DESTINATION_CITY,destinationCityIdAsString);

        String departureDateAsString = request.getParameter(DEPARTURE_DATE_PARAMETER);
        SessionHelper.setFlashAttribute(request,DEPARTURE_DATE,departureDateAsString);
    }
}
