package com.epam.airtickets.handler.flash;

import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

public class OrderFlashAttributeHandler implements FlashAttributeHandler {

    private final static String TICKET_TRAVEL_CLASS_PARAMETERS = "ticket_travel_class";
    private final static String PASSENGER_FIRST_NAME_PARAMETERS = "passenger_first_name";
    private final static String PASSENGER_LAST_NAME_PARAMETERS = "passenger_last_name";
    private final static String PASSENGER_PASSPORT_NUMBER_PARAMETERS = "passenger_passport_number";
    private final static String PASSENGER_DATE_OF_BIRTH_PARAMETERS = "passenger_date_of_birth";
    private final static String AMOUNT_OF_TICKETS_PARAMETER = "amount_of_tickets";

    private final static String TICKET_TRAVEL_CLASS_ARRAY = "travelClassArray";
    private final static String PASSENGER_FIRST_NAME_ARRAY = "passengerFirstNameArray";
    private final static String PASSENGER_LAST_NAME_ARRAY= "passengerLastNameArray";
    private final static String PASSENGER_PASSPORT_NUMBER_ARRAY = "passengerPassportNumberArray";
    private final static String PASSENGER_DATE_OF_BIRTH_ARRAY= "passengerDateOfBirthArray";
    private final static String AMOUNT_OF_TICKETS= "amountOfTickets";

    @Override
    public void handle(HttpServletRequest request) {
        String[] travelClassesAsString = request.getParameterValues(TICKET_TRAVEL_CLASS_PARAMETERS);
        SessionHelper.setFlashAttribute(request, TICKET_TRAVEL_CLASS_ARRAY, travelClassesAsString);

        String[] passengerFirstNames = request.getParameterValues(PASSENGER_FIRST_NAME_PARAMETERS);
        SessionHelper.setFlashAttribute(request, PASSENGER_FIRST_NAME_ARRAY, passengerFirstNames);

        String[] passengerLastNames = request.getParameterValues(PASSENGER_LAST_NAME_PARAMETERS);
        SessionHelper.setFlashAttribute(request, PASSENGER_LAST_NAME_ARRAY, passengerLastNames);

        String[] passengerPassportNumbers = request.getParameterValues(PASSENGER_PASSPORT_NUMBER_PARAMETERS);
        SessionHelper.setFlashAttribute(request, PASSENGER_PASSPORT_NUMBER_ARRAY, passengerPassportNumbers);

        String[] passengerDatesOfBirthAsString = request.getParameterValues(PASSENGER_DATE_OF_BIRTH_PARAMETERS);
        SessionHelper.setFlashAttribute(request, PASSENGER_DATE_OF_BIRTH_ARRAY, passengerDatesOfBirthAsString);

        String amountOfTickets = request.getParameter(AMOUNT_OF_TICKETS_PARAMETER);
        SessionHelper.setFlashAttribute(request, AMOUNT_OF_TICKETS, amountOfTickets);
    }
}
