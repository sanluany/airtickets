package com.epam.airtickets.handler.flash;

import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;

public class LoginFlashAttributeHandler implements FlashAttributeHandler {

    private final static String USERNAME = "username";

    @Override
    public void handle(HttpServletRequest request) {
        String username = request.getParameter(USERNAME);
        SessionHelper.setFlashAttribute(request,USERNAME,username);
    }
}
