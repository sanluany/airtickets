package com.epam.airtickets.handler.flash;

import javax.servlet.http.HttpServletRequest;

public interface FlashAttributeHandler {
    void handle(HttpServletRequest request);
}
