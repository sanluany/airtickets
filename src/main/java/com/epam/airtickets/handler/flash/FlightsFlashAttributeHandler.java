package com.epam.airtickets.handler.flash;

import com.epam.airtickets.utils.SessionHelper;

import javax.servlet.http.HttpServletRequest;


public class FlightsFlashAttributeHandler implements FlashAttributeHandler {

    private final static String FLIGHT_NUMBER_PARAMETER = "flight_number";
    private final static String FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT_PARAMETER = "flight_economy_class_tickets_amount";
    private final static String FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT_PARAMETER = "flight_business_class_tickets_amount";
    private final static String FLIGHT_FIRST_CLASS_TICKETS_AMOUNT_PARAMETER = "flight_first_class_tickets_amount";
    private final static String ORIGIN_AIRPORT_PARAMETER = "origin_airport";
    private final static String DESTINATION_AIRPORT_PARAMETER = "destination_airport";

    private final static String FLIGHT_NUMBER = "number";
    private final static String ORIGIN_AIRPORT = "airportOfOriginId";
    private final static String DESTINATION_AIRPORT = "destinationAirportId";
    private final static String FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT = "economyClassTicketsAmount";
    private final static String FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT = "businessClassTicketsAmount";
    private final static String FLIGHT_FIRST_CLASS_TICKETS_AMOUNT = "firstClassTicketsAmount";

    @Override
    public void handle(HttpServletRequest request) {
        String number = request.getParameter(FLIGHT_NUMBER_PARAMETER);
        SessionHelper.setFlashAttribute(request,FLIGHT_NUMBER,number);

        String airportOfOriginIdAsString = request.getParameter(ORIGIN_AIRPORT_PARAMETER);
        SessionHelper.setFlashAttribute(request,ORIGIN_AIRPORT,airportOfOriginIdAsString);

        String destinationAirportIdAsString = request.getParameter(DESTINATION_AIRPORT_PARAMETER);
        SessionHelper.setFlashAttribute(request,DESTINATION_AIRPORT,destinationAirportIdAsString);

        String economyClassTicketsAmountAsString = request.getParameter(FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT_PARAMETER);
        SessionHelper.setFlashAttribute(request,FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT,economyClassTicketsAmountAsString);

        String businessClassTicketsAmountAsString = request.getParameter(FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT_PARAMETER);
        SessionHelper.setFlashAttribute(request,FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT,businessClassTicketsAmountAsString);

        String firstClassTicketsAmountAsString = request.getParameter(FLIGHT_FIRST_CLASS_TICKETS_AMOUNT_PARAMETER);
        SessionHelper.setFlashAttribute(request,FLIGHT_FIRST_CLASS_TICKETS_AMOUNT,firstClassTicketsAmountAsString);
    }
}
