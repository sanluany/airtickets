package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.entity.Identifiable;

import java.sql.ResultSet;
import java.sql.SQLException;


public interface ResultSetHandler<T extends Identifiable> {
    T handle(ResultSet resultSet) throws SQLException;
}
