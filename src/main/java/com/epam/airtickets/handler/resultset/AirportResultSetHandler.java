package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.airport.AirportBuilder;
import com.epam.airtickets.builder.airport.AirportBuilderImpl;
import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.flight.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AirportResultSetHandler implements ResultSetHandler {

    private final String airportId;
    private final String airportCode;
    private final String airportName;

    private final CityResultSetHandler cityResultSetHandler;
    private final AirportBuilder airportBuilder = new AirportBuilderImpl();

    public AirportResultSetHandler(String airportId, String airportCode, String airportName, CityResultSetHandler cityResultSetHandler) {
        this.airportId = airportId;
        this.airportCode = airportCode;
        this.airportName = airportName;
        this.cityResultSetHandler = cityResultSetHandler;
    }

    @Override
    public Identifiable handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(airportId);
        String code = resultSet.getString(airportCode);
        String name = resultSet.getString(airportName);
        City city = (City) cityResultSetHandler.handle(resultSet);
        return airportBuilder.build(id, code, name, city);
    }
}
