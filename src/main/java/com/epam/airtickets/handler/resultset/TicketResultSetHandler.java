package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.ticket.TicketBuilder;
import com.epam.airtickets.builder.ticket.TicketBuilderImpl;
import com.epam.airtickets.entity.Identifiable;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.utils.DateUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class TicketResultSetHandler implements ResultSetHandler {

    private final static String TICKET_ID = "ticket_id";
    private final static String TICKET_TRAVEL_CLASS = "ticket_travel_class";
    private final static String TICKET_PASSENGER_FIRST_NAME = "ticket_passenger_first_name";
    private final static String TICKET_PASSENGER_LAST_NAME = "ticket_passenger_last_name";
    private final static String TICKET_PASSENGER_PASSPORT_NUMBER = "ticket_passenger_passport_number";
    private final static String TICKET_PASSENGER_DATE_OF_BIRTH = "ticket_passenger_date_of_birth";

    private final TicketBuilder ticketBuilder = new TicketBuilderImpl();
    private final TimetableResultSetHandler timetableResultSetHandler;

    public TicketResultSetHandler(TimetableResultSetHandler timetableResultSetHandler) {
        this.timetableResultSetHandler = timetableResultSetHandler;
    }

    @Override
    public Identifiable handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(TICKET_ID);
        Timetable timetable = timetableResultSetHandler.handle(resultSet);

        String travelClassAsString = resultSet.getString(TICKET_TRAVEL_CLASS);
        TravelClass travelClass = TravelClass.getByName(travelClassAsString);

        String passengerFirstName = resultSet.getString(TICKET_PASSENGER_FIRST_NAME);
        String passengerLastName = resultSet.getString(TICKET_PASSENGER_LAST_NAME);
        String passengerPassportNumber = resultSet.getString(TICKET_PASSENGER_PASSPORT_NUMBER);
        String passengerDateOfBirthAsString = resultSet.getString(TICKET_PASSENGER_DATE_OF_BIRTH);
        LocalDate passengerDateOfBirth = LocalDate.from(DateUtils.parseDate(passengerDateOfBirthAsString));

        return ticketBuilder.build(id, timetable, travelClass, passengerFirstName, passengerLastName,
                passengerPassportNumber, passengerDateOfBirth);
    }

}
