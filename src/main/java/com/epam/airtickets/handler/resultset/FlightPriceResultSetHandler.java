package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.flightprice.FlightPriceBuilder;
import com.epam.airtickets.builder.flightprice.FlightPriceBuilderImpl;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.utils.DateUtils;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


public class FlightPriceResultSetHandler implements ResultSetHandler {

    private final static String FLIGHT_PRICE_ID = "flight_price_id";
    private final static String FLIGHT_PRICE_TRAVEL_CLASS = "flight_price_travel_class";
    private final static String FLIGHT_PRICE_PRICE = "flight_price_price";
    private final static String FLIGHT_PRICE_START_DATE = "flight_price_start_date";
    private final static String FLIGHT_PRICE_END_DATE = "flight_price_end_date";
    private final static String FLIGHT_PRICE_IS_DELETED = "flight_price_is_deleted";

    private final FlightPriceBuilder flightPriceBuilder = new FlightPriceBuilderImpl();
    private final FlightResultSetHandler flightResultSetHandler;

    public FlightPriceResultSetHandler(FlightResultSetHandler flightResultSetHandler) {
        this.flightResultSetHandler = flightResultSetHandler;
    }

    @Override
    public FlightPrice handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(FLIGHT_PRICE_ID);
        Flight flight = flightResultSetHandler.handle(resultSet);

        String travelClassAsString = resultSet.getString(FLIGHT_PRICE_TRAVEL_CLASS);
        TravelClass travelClass = TravelClass.getByName(travelClassAsString);

        BigDecimal price = resultSet.getBigDecimal(FLIGHT_PRICE_PRICE);

        String startDateAsString = resultSet.getString(FLIGHT_PRICE_START_DATE);
        LocalDate startDate = DateUtils.parseDate(startDateAsString);

        String endDateAsString = resultSet.getString(FLIGHT_PRICE_END_DATE);
        LocalDate endDate = DateUtils.parseDate(endDateAsString);

        Boolean isDeleted = resultSet.getBoolean(FLIGHT_PRICE_IS_DELETED);
        return flightPriceBuilder.build(id, flight, travelClass, price, startDate, endDate, isDeleted);
    }

}
