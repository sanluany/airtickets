package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.order.OrderBuilder;
import com.epam.airtickets.builder.order.OrderBuilderImpl;
import com.epam.airtickets.entity.order.Order;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.utils.DateUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class OrderResultSetHandler implements ResultSetHandler {

    private final static String ORDER_ID = "order_id";
    private final static String ORDER_DATE = "order_date";

    private final OrderBuilder orderBuilder = new OrderBuilderImpl();
    private final UserResultSetHandler userResultSetHandler;

    public OrderResultSetHandler(UserResultSetHandler userResultSetHandler) {
        this.userResultSetHandler = userResultSetHandler;
    }

    @Override
    public Order handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(ORDER_ID);
        User user = userResultSetHandler.handle(resultSet);

        String orderDateAsString = resultSet.getString(ORDER_DATE);
        LocalDate orderDate = DateUtils.parseDate(orderDateAsString);
        return orderBuilder.build(id, user, orderDate);
    }
}
