package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.timetable.TimetableBuilder;
import com.epam.airtickets.builder.timetable.TimetableBuilderImpl;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.utils.DateUtils;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;


public class TimetableResultSetHandler implements ResultSetHandler {

    private final static String TIMETABLE_ID = "timetable_id";
    private final static String TIMETABLE_DEPARTURE_DATE = "timetable_departure_date";
    private final static String TIMETABLE_ARRIVAL_DATE = "timetable_arrival_date";
    private final static String TIMETABLE_IS_DELETED = "timetable_is_deleted";

    private final TimetableBuilder timetableBuilder = new TimetableBuilderImpl();
    private final FlightResultSetHandler flightResultSetHandler;

    public TimetableResultSetHandler(FlightResultSetHandler flightResultSetHandler) {
        this.flightResultSetHandler = flightResultSetHandler;
    }

    @Override
    public Timetable handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(TIMETABLE_ID);
        Flight flight = flightResultSetHandler.handle(resultSet);

        String departureDateAsString = resultSet.getString(TIMETABLE_DEPARTURE_DATE);
        LocalDateTime departureDate = DateUtils.parseDateTime(departureDateAsString);

        String arrivalDateAsString = resultSet.getString(TIMETABLE_ARRIVAL_DATE);
        LocalDateTime arrivalDate = DateUtils.parseDateTime(arrivalDateAsString);

        Boolean isDeleted = resultSet.getBoolean(TIMETABLE_IS_DELETED);
        return timetableBuilder.build(id,flight,departureDate,arrivalDate,isDeleted);

    }

}
