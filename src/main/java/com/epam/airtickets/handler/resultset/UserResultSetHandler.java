package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.user.UserBuilder;
import com.epam.airtickets.builder.user.UserBuilderImpl;
import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.entity.user.UserRole;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserResultSetHandler implements ResultSetHandler {

    private final static String USER_ID = "user_id";
    private final static String USER_USERNAME= "user_username";
    private final static String USER_PASSWORD= "user_password";
    private final static String USER_ROLE= "user_role";
    private final static String USER_BALANCE= "user_balance";

    private final UserBuilder userBuilder = new UserBuilderImpl();

    @Override
    public User handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(USER_ID);
        String username = resultSet.getString(USER_USERNAME);
        String password = resultSet.getString(USER_PASSWORD);

        String userRoleAsString = resultSet.getString(USER_ROLE);
        UserRole userRole = UserRole.getByName(userRoleAsString);

        BigDecimal balance = resultSet.getBigDecimal(USER_BALANCE);
        return userBuilder.build(id,username,password,userRole,balance);
    }
}
