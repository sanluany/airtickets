package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.flight.FlightBuilder;
import com.epam.airtickets.builder.flight.FlightBuilderImpl;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.Flight;


import java.sql.ResultSet;
import java.sql.SQLException;

public class FlightResultSetHandler implements ResultSetHandler {

    private final static String FLIGHT_ID = "flight_id";
    public final static String FLIGHT_NUMBER = "flight_number";
    public final static String FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT = "flight_economy_class_tickets_amount";
    public final static String FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT = "flight_business_class_tickets_amount";
    public final static String FLIGHT_FIRST_CLASS_TICKETS_AMOUNT = "flight_first_class_tickets_amount";
    private final static String FLIGHT_IS_DELETED = "flight_is_deleted";

    public final static String ORIGIN_AIRPORT = "origin_airport";
    public final static String DESTINATION_AIRPORT = "destination_airport";

    private final FlightBuilder flightBuilder = new FlightBuilderImpl();
    private final AirportResultSetHandler originAirportResultSetHandler;
    private final AirportResultSetHandler destinationAirportResultSetHandler;

    public FlightResultSetHandler(AirportResultSetHandler originAirportResultSetHandler,
                                  AirportResultSetHandler destinationAirportResultSetHandler) {
        this.originAirportResultSetHandler = originAirportResultSetHandler;
        this.destinationAirportResultSetHandler = destinationAirportResultSetHandler;
    }

    @Override
    public Flight handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(FLIGHT_ID);
        String number = resultSet.getString(FLIGHT_NUMBER);

        Airport originAirport = (Airport) originAirportResultSetHandler.handle(resultSet);
        Airport destinationAirport = (Airport) destinationAirportResultSetHandler.handle(resultSet);

        Integer economyClassTicketsAmount = resultSet.getInt(FLIGHT_ECONOMY_CLASS_TICKETS_AMOUNT);
        Integer businessClassTicketsAmount = resultSet.getInt(FLIGHT_BUSINESS_CLASS_TICKETS_AMOUNT);
        Integer firstClassTicketsAmount = resultSet.getInt(FLIGHT_FIRST_CLASS_TICKETS_AMOUNT);

        Boolean isDeleted = resultSet.getBoolean(FLIGHT_IS_DELETED);
        return flightBuilder.build(id, number, originAirport, destinationAirport, economyClassTicketsAmount,
                businessClassTicketsAmount, firstClassTicketsAmount, isDeleted);
    }
}
