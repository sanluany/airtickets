package com.epam.airtickets.handler.resultset;

import com.epam.airtickets.builder.city.CityBuilder;
import com.epam.airtickets.builder.city.CityBuilderImpl;
import com.epam.airtickets.entity.Identifiable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityResultSetHandler implements ResultSetHandler {

    private final String cityId;
    private final String cityName;

    private final CityBuilder cityBuilder = new CityBuilderImpl();

    public CityResultSetHandler(String cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    @Override
    public Identifiable handle(ResultSet resultSet) throws SQLException {
        Integer id = resultSet.getInt(cityId);
        String name = resultSet.getString(cityName);
        return cityBuilder.build(id, name);
    }
}
