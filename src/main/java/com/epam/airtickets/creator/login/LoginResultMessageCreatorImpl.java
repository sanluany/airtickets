package com.epam.airtickets.creator.login;

import com.epam.airtickets.creator.AbstractResultMessageCreator;
import com.epam.airtickets.dto.MessageDto;

import java.util.Locale;

public class LoginResultMessageCreatorImpl extends AbstractResultMessageCreator implements LoginResultMessageCreator {

    private final static String LOGIN_FAIL = "login.fail";

    @Override
    public MessageDto createForLogInCommand(Locale locale) {
        return create(ERROR, locale, LOGIN_FAIL);
    }
}
