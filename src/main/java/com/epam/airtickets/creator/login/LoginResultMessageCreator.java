package com.epam.airtickets.creator.login;

import com.epam.airtickets.dto.MessageDto;

import java.util.Locale;

public interface LoginResultMessageCreator {
    MessageDto createForLogInCommand(Locale locale);
}
