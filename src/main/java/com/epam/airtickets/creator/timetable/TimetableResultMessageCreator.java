package com.epam.airtickets.creator.timetable;

import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Flight;

import java.util.List;
import java.util.Locale;

public interface TimetableResultMessageCreator {
    MessageDto createForAddCommand(int result, Locale locale, Flight Flight, List<Timetable> timetables);
    MessageDto createForDeleteCommand(int result, Locale locale, Timetable timetable);
    MessageDto createForRecoverCommand(int result, Locale locale, Timetable timetable);
}
