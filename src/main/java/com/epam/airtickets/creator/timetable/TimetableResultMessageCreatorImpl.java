package com.epam.airtickets.creator.timetable;

import com.epam.airtickets.creator.AbstractResultMessageCreator;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Timetable;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.utils.DateUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

public class TimetableResultMessageCreatorImpl extends AbstractResultMessageCreator implements TimetableResultMessageCreator {

    private final static String ADD_TIMETABLE_ERROR = "add.timetable.error";
    private final static String ADD_TIMETABLE_MESSAGE = "add.timetable.message";
    private final static String DELETE_TIMETABLE_ERROR = "delete.timetable.error";
    private final static String DELETE_TIMETABLE_MESSAGE = "delete.timetable.message";
    private final static String RECOVER_TIMETABLE_ERROR = "recover.timetable.error";
    private final static String RECOVER_TIMETABLE_MESSAGE = "recover.timetable.message";

    @Override
    public MessageDto createForAddCommand(int result, Locale locale, Flight flight, List<Timetable> timetables) {
        int size = timetables.size();
        Timetable firstTimetable = timetables.get(0);
        Timetable lastTimetable = timetables.get(size - 1);

        String flightNumber = flight.getNumber();

        Airport originAirport = flight.getOriginAirport();
        String originAirportCode = originAirport.getCode();
        City originCity = originAirport.getCity();

        Airport destinationAirport = flight.getDestinationAirport();
        String destinationAirportCode = destinationAirport.getCode();
        City destinationCity = destinationAirport.getCity();

        LocalDateTime firstFlightDeparture = firstTimetable.getDepartureDate();
        LocalDateTime firstFlightArrival = firstTimetable.getArrivalDate();
        LocalDateTime lastFlightDeparture = lastTimetable.getDepartureDate();

        String firstFlightAsString = DateUtils.getFormattedDate(firstFlightDeparture);
        String lastFlightAsString = DateUtils.getFormattedDate(lastFlightDeparture);

        String departureTimeAsString = DateUtils.getFormattedTime(firstFlightDeparture);
        String arrivalTimeAsString = DateUtils.getFormattedTime(firstFlightArrival);

        return create(result, locale, ADD_TIMETABLE_ERROR, ADD_TIMETABLE_MESSAGE,
                flightNumber,
                originAirportCode,
                originCity.toString(),
                destinationAirportCode,
                destinationCity.toString(),
                firstFlightAsString,
                lastFlightAsString,
                departureTimeAsString,
                arrivalTimeAsString);
    }

    @Override
    public MessageDto createForDeleteCommand(int result, Locale locale, Timetable timetable) {
        Integer id = timetable.getId();
        String idAsString = String.valueOf(id);
        return create(result, locale, DELETE_TIMETABLE_ERROR, DELETE_TIMETABLE_MESSAGE, idAsString);
    }

    @Override
    public MessageDto createForRecoverCommand(int result, Locale locale, Timetable timetable) {
        Integer id = timetable.getId();
        String idAsString = String.valueOf(id);
        return create(result, locale, RECOVER_TIMETABLE_ERROR, RECOVER_TIMETABLE_MESSAGE, idAsString);
    }

}
