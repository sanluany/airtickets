package com.epam.airtickets.creator.flightprice;

import com.epam.airtickets.creator.AbstractResultMessageCreator;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.TravelClass;
import com.epam.airtickets.entity.flight.Flight;
import com.epam.airtickets.entity.flight.FlightPrice;
import com.epam.airtickets.utils.DateUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Locale;

public class FlightPriceResultMessageCreatorImpl extends AbstractResultMessageCreator implements FlightPriceResultMessageCreator {

    private final static String ADD_FLIGHT_PRICE_ERROR = "add.flight.price.error";
    private final static String ADD_FLIGHT_PRICE_MESSAGE = "add.flight.price.message";
    private final static String DELETE_FLIGHT_PRICE_ERROR = "delete.flight.price.error";
    private final static String DELETE_FLIGHT_PRICE_MESSAGE = "delete.flight.price.message";
    private final static String RECOVER_FLIGHT_PRICE_ERROR = "recover.flight.price.error";
    private final static String RECOVER_FLIGHT_PRICE_MESSAGE = "recover.flight.price.message";

    @Override
    public MessageDto createForAddCommand(int result, Locale locale, FlightPrice flightPrice) {
        TravelClass travelClass = flightPrice.getTravelClass();
        BigDecimal price = flightPrice.getPrice();
        Flight flight = flightPrice.getFlight();
        String flightNumber = flight.getNumber();

        LocalDate startDate = flightPrice.getStartDate();
        LocalDate endDate = flightPrice.getEndDate();
        String startDateAsString = DateUtils.getFormattedDate(startDate);
        String endDateAsString = DateUtils.getFormattedDate(endDate);

        return create(result, locale, ADD_FLIGHT_PRICE_ERROR, ADD_FLIGHT_PRICE_MESSAGE,
                travelClass.name(),
                price.toString(),
                flightNumber,
                startDateAsString,
                endDateAsString);
    }

    @Override
    public MessageDto createForDeleteCommand(int result, Locale locale, FlightPrice flightPrice) {
        Integer id = flightPrice.getId();
        String idAsString = String.valueOf(id);
        return create(result,locale,DELETE_FLIGHT_PRICE_ERROR,DELETE_FLIGHT_PRICE_MESSAGE,idAsString);
    }

    @Override
    public MessageDto createForRecoverCommand(int result, Locale locale, FlightPrice flightPrice) {
        Integer id = flightPrice.getId();
        String idAsString = String.valueOf(id);
        return create(result,locale,RECOVER_FLIGHT_PRICE_ERROR,RECOVER_FLIGHT_PRICE_MESSAGE,idAsString);
    }
}
