package com.epam.airtickets.creator.flightprice;

import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.FlightPrice;

import java.util.Locale;

public interface FlightPriceResultMessageCreator {
    MessageDto createForAddCommand(int result, Locale locale, FlightPrice flightPrice);
    MessageDto createForDeleteCommand(int result, Locale locale, FlightPrice flightPrice);
    MessageDto createForRecoverCommand(int result, Locale locale, FlightPrice flightPrice);
}
