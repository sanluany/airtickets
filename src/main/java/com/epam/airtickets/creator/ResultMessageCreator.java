package com.epam.airtickets.creator;

import com.epam.airtickets.dto.MessageDto;

import java.util.Locale;

public interface ResultMessageCreator {
    int ERROR = 0;
    int SUCCESS = 1;
    MessageDto create(int result, Locale locale, String errorMessageKey, String successMessageKey, String... parameters);
    MessageDto create(int result, Locale locale, String messageKey);
}
