package com.epam.airtickets.creator.flight;

import com.epam.airtickets.creator.AbstractResultMessageCreator;
import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Airport;
import com.epam.airtickets.entity.flight.City;
import com.epam.airtickets.entity.flight.Flight;

import java.util.Locale;

public class FlightResultMessageCreatorImpl extends AbstractResultMessageCreator implements FlightResultMessageCreator {

    private final static String ADD_FLIGHT_ERROR = "add.flight.error";
    private final static String ADD_FLIGHT_MESSAGE = "add.flight.message";

    private final static String DELETE_FLIGHT_ERROR = "delete.flight.error";
    private final static String DELETE_FLIGHT_MESSAGE = "delete.flight.message";

    private final static String RECOVER_FLIGHT_ERROR = "recover.flight.error";
    private final static String RECOVER_FLIGHT_MESSAGE = "recover.flight.message";

    private final static String FLIGHTS_SEARCH_FAILED = "flights.search.failed";

    @Override
    public MessageDto createForAddCommand(int result, Locale locale, Flight flight) {
        Airport originAirport = flight.getOriginAirport();
        City originCity = originAirport.getCity();
        Airport destinationAirport = flight.getDestinationAirport();
        City destinationCity = destinationAirport.getCity();

        String flightNumber = flight.getNumber();
        String originAirportCode = originAirport.getCode();
        String destinationAirportCode = destinationAirport.getCode();
        Integer economyClassTicketsAmount = flight.getEconomyClassTicketsAmount();
        Integer businessClassTicketsAmount = flight.getBusinessClassTicketsAmount();
        Integer firstClassTicketsAmount = flight.getFirstClassTicketsAmount();

        return create(result, locale, ADD_FLIGHT_ERROR, ADD_FLIGHT_MESSAGE,
                flightNumber,
                originAirportCode,
                originCity.toString(),
                destinationAirportCode,
                destinationCity.toString(),
                economyClassTicketsAmount.toString(),
                businessClassTicketsAmount.toString(),
                firstClassTicketsAmount.toString());
    }

    @Override
    public MessageDto createForDeleteCommand(int result, Locale locale, Flight flight) {
        return create(result, locale, DELETE_FLIGHT_ERROR, DELETE_FLIGHT_MESSAGE, flight.getNumber());
    }

    @Override
    public MessageDto createForRecoverCommand(int result, Locale locale, Flight flight) {
        return create(result, locale, RECOVER_FLIGHT_ERROR, RECOVER_FLIGHT_MESSAGE, flight.getNumber());
    }

    @Override
    public MessageDto createForSearchCommand(Locale locale) {
        return create(ERROR, locale, FLIGHTS_SEARCH_FAILED);
    }
}
