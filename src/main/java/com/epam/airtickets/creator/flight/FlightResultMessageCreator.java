package com.epam.airtickets.creator.flight;

import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.entity.flight.Flight;

import java.util.Locale;

public interface FlightResultMessageCreator {
    MessageDto createForAddCommand(int result, Locale locale, Flight flight);
    MessageDto createForDeleteCommand(int result, Locale locale, Flight flight);
    MessageDto createForRecoverCommand(int result, Locale locale, Flight flight);
    MessageDto createForSearchCommand(Locale locale);
}
