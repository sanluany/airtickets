package com.epam.airtickets.creator.order;

import com.epam.airtickets.dto.MessageDto;

import java.util.Locale;

public interface OrderResultMessageCreator {
    MessageDto createForMakeOrderCommand(String messageKey, Locale locale);
    MessageDto createForSuccessfullyPaidOrder(Locale locale);
}
