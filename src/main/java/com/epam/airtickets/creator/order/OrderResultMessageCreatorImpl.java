package com.epam.airtickets.creator.order;

import com.epam.airtickets.creator.AbstractResultMessageCreator;
import com.epam.airtickets.dto.MessageDto;

import java.util.Locale;

public class OrderResultMessageCreatorImpl extends AbstractResultMessageCreator implements OrderResultMessageCreator {

    private final static String ORDER_PAY_SUCCESSFUL = "order.pay.successful";

    @Override
    public MessageDto createForMakeOrderCommand(String messageKey, Locale locale) {
        return create(ERROR, locale, messageKey);
    }

    @Override
    public MessageDto createForSuccessfullyPaidOrder(Locale locale) {
        return create(SUCCESS, locale, ORDER_PAY_SUCCESSFUL);
    }
}
