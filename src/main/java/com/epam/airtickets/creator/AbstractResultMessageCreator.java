package com.epam.airtickets.creator;

import com.epam.airtickets.dto.MessageDto;
import com.epam.airtickets.utils.MessageHelper;

import java.util.Locale;

public class AbstractResultMessageCreator implements ResultMessageCreator {

    public MessageDto create(int result, Locale locale, String errorMessageKey, String successMessageKey,
                             String... parameters) {
        boolean isError = false;
        String message;
        if (result == 0) {
            isError = true;
            message = MessageHelper.getMessage(MessageHelper.BUNDLE_MESSAGES, errorMessageKey, locale);
        } else {
            message = MessageHelper.getMessage(MessageHelper.BUNDLE_MESSAGES, successMessageKey, locale, parameters);
        }
        return new MessageDto(isError, message);
    }

    @Override
    public MessageDto create(int result, Locale locale, String messageKey) {
        String message = MessageHelper.getMessage(MessageHelper.BUNDLE_MESSAGES, messageKey, locale);
        if (result == 0) {
            return new MessageDto(true, message);
        }
        return new MessageDto(false, message);
    }
}
