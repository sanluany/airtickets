CREATE SCHEMA `airtickets`;
use  `airtickets`;
create table users(
  id int auto_increment primary key,
  username varchar(255) not null unique,
  password varchar(255) not null,
  role enum('USER','ADMIN') not null,
  balance decimal(18,2)
);

create table cities(
  id int auto_increment primary key,
  name varchar(255) not null unique
);
create table airports (
  id int auto_increment primary key,
  code char(3) not null,
  name varchar(255) not null,
  city_id int not null,
  foreign key (city_id) references cities (id)
);

create table flights (
  id int auto_increment primary key,
  number varchar(255) not null unique,
  origin_airport_id int not null,
  destination_airport_id int not null,
  economy_class_tickets_amount smallint not null,
  business_class_tickets_amount smallint not null,
  first_class_tickets_amount smallint not null,
  is_deleted bit not null default 0,
  foreign key (origin_airport_id) references airports (id),
  foreign key (destination_airport_id) references airports (id)
);

create table flight_prices(
  id int auto_increment primary key,
  flight_id int not null,
  travel_class enum('ECONOMY','BUSINESS','FIRST')  not null,
  price decimal(18,2) not null,
  start_date date not null,
  end_date date not null,
  is_deleted bit not null default 0,
  foreign key (flight_id) references flights (id)
);

create table orders(
  id int auto_increment primary key,
  user_id int not null,
  order_date Date not null,
  foreign key (user_id) references users (id)
);

create table timetables(
  id int auto_increment primary key,
  flight_id int not null,
  departure_date datetime not null,
  arrival_date datetime not null,
  is_deleted bit not null default 0,
  foreign key (flight_id) references flights (id)
);

create table tickets(
  id int auto_increment primary key,
  order_id int not null,
  timetable_id int not null,
  travel_class enum('ECONOMY','BUSINESS','FIRST') not null,
  passenger_first_name varchar(255) not null,
  passenger_last_name varchar(255) not null,
  passenger_passport_number varchar(255) not null,
  passenger_date_of_birth date not null,
  foreign key (timetable_id) references timetables (id),
  foreign key (order_id) references orders(id)
);

insert into cities (name) values ("Minsk");
insert into cities (name) values ("Moscow");
insert into cities (name) values ("Vilnus");
insert into cities (name) values ("Warsaw");

insert into users (username,password,role,balance) values ('Dima',md5('qwerty'),'USER',750.00);
insert into users (username,password,role,balance) values ('Admin',md5('qwerty'),'ADMIN',0);

insert into airports (code,name,city_id) values ("MSQ","Minsk National Airport", 1);
insert into airports (code,name,city_id) values ("SVO","Sheremetyevo International Airport", 2);
insert into airports (code,name,city_id) values ("DME","Moscow Domodedovo Airport", 2);
insert into airports (code,name,city_id) values ("VNO","Vilnius Airport", 3);
insert into airports (code,name,city_id) values ("WAW","Warsaw Chopin Airport", 4);

