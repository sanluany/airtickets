var action;
var selectedOfferId;

$( document ).ready(function() {
    var isLoggedIn = $("#isLoggedIn").val();
    var requestedPage = sessionStorage.getItem('requestedPage');
    if(isLoggedIn == 'true' && requestedPage != null){
        sessionStorage.removeItem("requestedPage");
        window.location.href = requestedPage;
    }
});

function onButtonClick(button) {
    action = button.id;
    $("#action_form").submit();
}

var deleteRexExp = new RegExp("Delete\\d+");
var recoverRexExp = new RegExp("Recover\\d+");

function onSubmitFlightActionForm(form) {
    var showTimetableRegExp = new RegExp("ShowTimetable\\d+");
    var showFlightPriceRegExp = new RegExp("ShowFlightPrice\\d+");
    var id;
    if (deleteRexExp.test(action)) {
        id = action.substr(6);
        form.action = '/flights?command=deleteFlight&id=' + id;
    } else if (recoverRexExp.test(action)) {
        id = action.substr(7);
        form.action = '/flights?command=recoverFlight&id=' + id;
    } else if (showTimetableRegExp.test(action)) {
        id = action.substr(13);
        form.action = '/timetable';
        form.method = 'GET';
        document.getElementById('command').value = 'showTimetable';
        document.getElementById('flight_id').value = id;
    } else if (showFlightPriceRegExp.test(action)) {
        id = action.substr(15);
        form.action = '/flightprice';
        form.method = 'GET';
        document.getElementById('command').value = 'showFlightPrice';
        document.getElementById('flight_id').value = id;
    }
}

function onSubmitTimetableActionForm(form) {
    var id;
    if (deleteRexExp.test(action)) {
        id = action.substr(6);
        form.action = '/timetable?command=deleteTimetable&id=' + id;
    } else if (recoverRexExp.test(action)) {
        id = action.substr(7);
        form.action = '/timetable?command=recoverTimetable&id=' + id;
    }
}

function onSubmitFlightPriceActionForm(form) {
    var id;
    if (deleteRexExp.test(action)) {
        id = action.substr(6);
        form.action = '/flightprice?command=deleteFlightPrice&id=' + id;
    } else if (recoverRexExp.test(action)) {
        id = action.substr(7);
        form.action = '/flightprice?command=recoverFlightPrice&id=' + id;
    }
}

function onOfferSelection(button) {
    selectedOfferId = button.id.substr(5);
    $("#offer_id").val(selectedOfferId);

    var url = "/order?offer_id=" + selectedOfferId + "&command=showOrder";

    var isLoggedIn = $("#isLoggedIn").val();
    if (isLoggedIn == 'false') {
        sessionStorage.setItem('requestedPage',url);
        showModalById('login-modal');
    } else {
        proceedToOrder(url);
    }
}

function proceedToOrder(url) {
    window.location.href = url;
}

function changeLanguage() {
    var language = $('#language-list').find(":selected").val();
    switch (language) {
        case 'ru': {
            document.cookie = 'locale=ru';
            break;
        }
        case 'en': {
            document.cookie = 'locale=en';
            break;
        }
        case 'by': {
            document.cookie = 'locale=by';
            break;
        }
    }
    location.reload();
}

