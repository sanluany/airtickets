var id = $("#current_tickets_index").val() - 1;
var MAX_TICKET_ID = 4;
var MIN_TICKET_ID = 0;

var ECONOMY_CLASS_PRICE;
var BUSINESS_CLASS_PRICE;
var FIRST_CLASS_PRICE;

var passengerAmount = document.getElementById('passenger_amount');
var tickets = document.getElementById('tickets');

$(document).ready(function () {
    if ($("#economy_class_price_holder").length) {
        ECONOMY_CLASS_PRICE = $("#economy_class_price_holder").val();
    }
    if ($("#business_class_price_holder").length) {
        BUSINESS_CLASS_PRICE = $("#business_class_price_holder").val();
    }
    if ($("#first_class_price_holder").length) {
        FIRST_CLASS_PRICE = $("#first_class_price_holder").val();
    }
});

function addTicket() {
    if (id < MAX_TICKET_ID) {
        id++;
        var div = document.createElement('div');
        div.innerHTML = getTicketHtml();
        div.setAttribute('class', 'ticket');
        div.style.display = 'flex';
        div.id = 'ticket' + id;

        var passengerFirstName = div.getElementsByClassName("passenger-first-name")[0];
        passengerFirstName.id =passengerFirstName.id+ id;

        var passengerLastName = div.getElementsByClassName('passenger-last-name')[0];
        passengerLastName.id += id;

        var passengerPassportNumber = div.getElementsByClassName('passenger-passport-number')[0];
        passengerPassportNumber.id += id;

        var passengerDateOfBirth= div.getElementsByClassName('passenger-date-of-birth')[0];
        passengerDateOfBirth.id += id;

        var ticketTravelClass = div.getElementsByClassName('travel-class')[0];
        ticketTravelClass.id += id;

        var ticketPrice = div.getElementsByClassName('price')[0];
        ticketPrice.id += id;

        tickets.appendChild(div);
        passengerAmount.innerHTML = id + 1;
    }
    calculateTotalPrice();
}

function removeTicket() {
    if (id > MIN_TICKET_ID) {
        var ticket = document.getElementById('ticket' + id);
        ticket.parentNode.removeChild(ticket);
        id--;
        passengerAmount.innerHTML = id + 1;
    }
    calculateTotalPrice();
}

function onTravelClassChange(list) {
    var chosenTravelClass = list.value;
    var localId = list.id.substr(19);
    $("#ticket_price" + localId).html(getPrice(chosenTravelClass));
    calculateTotalPrice();
}

function calculateTotalPrice() {
    $("#amount_of_tickets").val(id + 1);

    var chosenTravelClass = $("#ticket_travel_class0").val();
    var totalPrice = parseInt(getPrice(chosenTravelClass));

    var additionalTicketsPriceTotal = parseInt(calculateAdditionalTickets());

    totalPrice += additionalTicketsPriceTotal;

    $("#total_price").html(totalPrice);
}

function calculateAdditionalTickets() {
    var calculatedPrice = 0;
    if (id > 0) {
        for (var i = 1; i < id + 1; i++) {
            var ticketElement = document.getElementById('ticket_travel_class' + i);
            var chosenTravelClass = ticketElement.value;
            var ticketPrice = parseInt(getPrice(chosenTravelClass));
            calculatedPrice += ticketPrice;
        }
    }
    return calculatedPrice;
}

function getPrice(travelClass) {
    switch (travelClass) {
        case 'ECONOMY': {
            return ECONOMY_CLASS_PRICE;
        }
        case 'BUSINESS': {
            return BUSINESS_CLASS_PRICE;
        }
        case 'FIRST': {
            return FIRST_CLASS_PRICE;
        }
    }
}


function getTicketHtml() {
    return $("#ticket_template").html();
}