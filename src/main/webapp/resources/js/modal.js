var currentModal;
window.onclick = function(event) {
    if (event.target == currentModal) {
        hideCurrentModalWindow();
    }
};
function onClickShowModalLoginForm() {
    var modal = document.getElementById('modal');
    modal.style.display = "block";
    event.preventDefault();
}
function showModalById(id) {
    currentModal = $('#'+id);
    currentModal.show();
}
function hideCurrentModalWindow() {
    currentModal.hide();
}