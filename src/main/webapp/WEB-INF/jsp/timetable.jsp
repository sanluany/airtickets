<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<html>
<head>
    <title>Timetable page</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" href="../../resources/css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<c:set var="flightId" value="${flight.getId()}"/>
<c:set var="flightNumber" value="${flight.getNumber()}"/>
<div class="nav-line-container text-major">
    <div class="nav-line">
        <div class="nav-link">
            <a href="/"><fmt:message key="header.main"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="/flights?command=showFlights"><fmt:message key="header.flights"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="#"><fmt:message key="timetable.timetable"/>(<fmt:message
                    key="timetable.flight"/> ${flightNumber})</a>
        </div>
    </div>
</div>
<div class="new-flight-container">
    <div class="text-major text-center">
        <fmt:message key="timetable.add"/>
    </div>
    <form method="post" class="no-margin" action="/timetable?command=addTimetable&flight_id=${flightId}">
        <div class="new-flight-row">
            <div class="new-flight-column">
                <label for="first_flight_date"><fmt:message key="timetable.flight.first"/></label>
                <input type="date" name="first_flight_date" id="first_flight_date" required
                       value="${requestScope.firstFlightDate}">
            </div>
            <div class="new-flight-column">
                <label for="last_flight_date"><fmt:message key="timetable.flight.last"/></label>
                <input type="date" name="last_flight_date" id="last_flight_date" required
                       value="${requestScope.lastFlightDate}">
            </div>
        </div>
        <div class="new-flight-row">
            <div class="new-flight-column">
                <label for="departure_time"><fmt:message key="timetable.time.departure"/></label>
                <input type="time" name="departure_time" id="departure_time" required
                       value="${requestScope.departureTime}">
            </div>
            <div class="new-flight-column">
                <label for="arrival_time"><fmt:message key="timetable.time.arrival"/></label>
                <input type="time" name="arrival_time" id="arrival_time" required value="${requestScope.arrivalTime}">
            </div>
        </div>
        <div class="text-major text-center"><fmt:message key="timetable.days"/></div>
        <div class="new-flight-row">
            <c:forEach begin="1" end="7" varStatus="loop">
                <div class="new-flight-column">
                    <label class="checkbox-label">
                        <c:if test="${loop.index == 1}"><fmt:message key="timetable.monday"/></c:if>
                        <c:if test="${loop.index == 2}"><fmt:message key="timetable.tuesday"/></c:if>
                        <c:if test="${loop.index == 3}"><fmt:message key="timetable.wednesday"/></c:if>
                        <c:if test="${loop.index == 4}"><fmt:message key="timetable.thursday"/></c:if>
                        <c:if test="${loop.index == 5}"><fmt:message key="timetable.friday"/></c:if>
                        <c:if test="${loop.index == 6}"><fmt:message key="timetable.saturday"/></c:if>
                        <c:if test="${loop.index == 7}"><fmt:message key="timetable.sunday"/></c:if>
                        <input type="checkbox" class="input-checkbox" name="day"
                               value="${loop.index}" ${requestScope.day[loop.index] == loop.index ? 'checked' : ''} >
                        <span class="checkbox-span"></span>
                    </label>
                </div>
            </c:forEach>
        </div>
        <div class="new-flight-row block">
            <div class="text-center">
                <input type="submit" class="button bg-color-green" value="<fmt:message key="button.add"/>">
            </div>
        </div>
    </form>
</div>
<c:if test="${resultMessage !=null}">
    <div class="result-message text-major ${resultMessage.getError() ? 'error' : 'no-error'}">${resultMessage.getMessage()}</div>
</c:if>
<div class="table">
    <form method="post" onsubmit="onSubmitTimetableActionForm(this)" id="action_form">
        <table cellspacing="0">
            <caption><fmt:message key="timetable.flight.timetable"/> ${flight.getNumber()}</caption>
            <thead>
            <tr>
                <th>ID</th>
                <th><fmt:message key="timetable.date.departure"/></th>
                <th><fmt:message key="timetable.date.arrival"/></th>
                <th><fmt:message key="table.status"/></th>
                <th colspan="2"><fmt:message key="table.action"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${timetableDtoList}" var="timetableDto" varStatus="loop">
                <c:set var="timetable" value="${timetableDto.getTimetable()}"/>
                <c:set var="timetableId" value="${timetable.getId()}"/>
                <c:set var="departureDate" value="${timetable.getDepartureDate()}"/>
                <c:set var="arrivalDate" value="${timetable.getArrivalDate()}"/>
                <c:set var="isDeleted" value="${timetable.getDeleted()}"/>
                <tr>
                    <td>${timetable.getId()}</td>
                    <td><custom:date temporal="${departureDate}" patternType="DATE_TIME"/>
                    </td>
                    <td><custom:date temporal="${arrivalDate}" patternType="DATE_TIME"/>
                    </td>
                    <td><c:choose>
                        <c:when test="${isDeleted}"><fmt:message key="timetable.status.deleted"/> </c:when>
                        <c:otherwise><fmt:message key="timetable.status.active"/></c:otherwise>
                    </c:choose>
                    </td>
                    <c:choose>
                        <c:when test="${!isDeleted}">
                            <td><a href="#" title="<fmt:message key="action.delete"/>" name="button+"
                                   id="Delete${timetableId}"
                                   onclick="onButtonClick(this)">
                                <i class="fas fa-times"></i></a></td>
                        </c:when>
                        <c:otherwise>
                            <td><a href="#" title="<fmt:message key="action.recover"/>" name="button+"
                                   id="Recover${timetableId}"
                                   onclick="onButtonClick(this)">
                                <i class="fas fa-undo-alt"></i></a></td>
                        </c:otherwise>
                    </c:choose>
                    <td><a href="#" title="Статистика" name="button+"
                           onclick="showModalById('timetable-modal${loop.index}')">
                        <i class="fas fa-exclamation"></i></a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="hidden" name="flight_id" id="flight_id" value="${flightId}">
    </form>
</div>
<c:forEach items="${timetableDtoList}" var="timetableDto" varStatus="loop">
    <c:set var="timetable" value="${timetableDto.getTimetable()}"/>
    <c:set var="flight" value="${timetable.getFlight()}"/>
    <c:set var="flightNumber" value="${flight.getNumber()}"/>
    <div class="modal" id="timetable-modal${loop.index}">
        <div class="modal-window modal-content">
            <div class="modal-controls text-major">
                <span><fmt:message key="timetable.statistics"/> ${flightNumber}</span>
                <a href="#" title="<fmt:message key="action.close"/>" onclick="hideCurrentModalWindow()">
                    <i class="fas fa-times"></i></a>
            </div>
            <div class="modal-window-content">
                <div class="table no-margin">
                    <table cellspacing="0">
                        <caption><fmt:message key="flights.flight" />${flightNumber}</caption>
                        <thead>
                        <tr>
                            <th colspan="3"> <fmt:message key="flights.tickets.amount.general" /></th>
                        </tr>
                        <tr>
                            <th colspan="3"> <fmt:message key="timetable.statistics.sold.total" /></th>
                        </tr>
                        <tr>
                            <th><fmt:message key="travel.class.economy" /></th>
                            <th><fmt:message key="travel.class.business" /></th>
                            <th><fmt:message key="travel.class.first" /></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:set var="economySold"
                               value="${timetableDto.getSoldTicketsAmountByTravelClassMap().get('ECONOMY')}"/>
                        <c:set var="businessSold"
                               value="${timetableDto.getSoldTicketsAmountByTravelClassMap().get('BUSINESS')}"/>
                        <c:set var="firstSold"
                               value="${timetableDto.getSoldTicketsAmountByTravelClassMap().get('FIRST')}"/>
                        <tr>
                            <td>${economySold == null ? '0' : economySold }
                                /${flight.getEconomyClassTicketsAmount()}</td>
                            <td>${businessSold == null ? '0' : businessSold }
                                /${flight.getBusinessClassTicketsAmount()}</td>
                            <td>${firstSold == null ? '0' : firstSold } /${flight.getFirstClassTicketsAmount()}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</c:forEach>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
<div class="overlay"></div>
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/modal.js"></script>
</body>
</html>
