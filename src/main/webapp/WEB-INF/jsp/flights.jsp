<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Administrator page</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" href="../../resources/css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<div class="nav-line-container text-major">
    <div class="nav-line">
        <div class="nav-link">
            <a href="/"><fmt:message key="header.main"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="/flights?command=showFlights"><fmt:message key="header.flights"/></a>
        </div>
    </div>
</div>
<div class="new-flight-container ">
    <div class="text-major text-center"><fmt:message key="flights.add.flight"/></div>
    <form method="post" action="/flights?command=addFlight">
        <div class="new-flight">
            <div class="new-flight-column">
                <div class="text-major text-center">
                    <fmt:message key="flights.info.main"/>
                </div>
                <div class="new-flight-row">
                    <label for="flight_number"><fmt:message key="flights.flight"/> </label>
                    <input type="text" id="flight_number" name="flight_number" required value="${requestScope.number}">
                </div>
                <div class="new-flight-row">
                    <label for="origin_airport"><fmt:message key="flights.from"/> </label>
                    <select id="origin_airport" name="origin_airport">
                        <c:forEach items="${airports}" var="originAirport">
                            <c:set var="originAirportId" value="${originAirport.getId()}"/>
                            <c:set var="originAirportCode" value="${originAirport.getCode()}"/>
                            <c:set var="originAirportCity" value="${originAirport.getCity()}"/>
                            <c:choose>
                                <c:when test="${requestScope.airportOfOriginId == originAirportId}">
                                    <option value="${originAirportId}" selected> ${originAirportCode}
                                        (${originAirportCity})
                                    </option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${originAirportId}">${originAirportCode}
                                        (${originAirportCity})
                                    </option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="new-flight-row">
                    <label for="destination_airport"><fmt:message key="flights.to"/> </label>
                    <select id="destination_airport" name="destination_airport">
                        <c:forEach items="${airports}" var="destinationAirport">
                            <c:set var="destinationAirportId" value="${destinationAirport.getId()}"/>
                            <c:set var="destinationAirportCode" value="${destinationAirport.getCode()}"/>
                            <c:set var="destinationAirportCity" value="${destinationAirport.getCity()}"/>
                            <c:choose>
                                <c:when test="${requestScope.destinationAirportId == destinationAirportId}">
                                    <option value="${destinationAirportId}" selected> ${destinationAirportCode}
                                        (${destinationAirportCity})
                                    </option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${destinationAirportId}">${destinationAirportCode}
                                        (${destinationAirportCity})
                                    </option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="new-flight-column">
                <div class="text-major text-center">
                    <fmt:message key="flights.tickets.amount"/>
                </div>
                <div class="new-flight-row">
                    <label for="flight_economy_class_tickets_amount"><fmt:message key="flights.class.economy"/></label>
                    <input type="number" id="flight_economy_class_tickets_amount"
                           name="flight_economy_class_tickets_amount" required min="0"
                           value="${requestScope.economyClassTicketsAmount}">

                </div>
                <div class="new-flight-row">
                    <label for="flight_business_class_tickets_amount"><fmt:message
                            key="flights.class.business"/> </label>
                    <input type="number" id="flight_business_class_tickets_amount"
                           name="flight_business_class_tickets_amount" required min="0"
                           value="${requestScope.businessClassTicketsAmount}">
                </div>
                <div class="new-flight-row">
                    <label for="flight_first_class_tickets_amount"><fmt:message key="flights.class.first"/></label>
                    <input type="number" id="flight_first_class_tickets_amount" name="flight_first_class_tickets_amount"
                           required min="0" value="${requestScope.firstClassTicketsAmount}">
                </div>
            </div>
        </div>
        <div class="text-center">
            <input type="submit" class="button bg-color-green" value="<fmt:message key="button.add"/>">
        </div>
    </form>
</div>
<c:if test="${resultMessage !=null}">
    <div class="result-message text-major ${resultMessage.getError() ? 'error' : 'no-error'}">${resultMessage.getMessage()}</div>
</c:if>

<div class="table">
    <form method="post" onsubmit="onSubmitFlightActionForm(this)" id="action_form">
        <table cellspacing="0">
            <caption><fmt:message key="flights.flights"/></caption>
            <thead>
            <tr>
                <th rowspan="2">ID</th>
                <th rowspan="2"><fmt:message key="flights.flight"/></th>
                <th rowspan="2"><fmt:message key="flights.from"/></th>
                <th rowspan="2"><fmt:message key="flights.to"/></th>
                <th colspan="3"><fmt:message key="flights.tickets.amount.general"/></th>
                <th rowspan="2"><fmt:message key="table.status"/></th>
                <th colspan="3" rowspan="2"><fmt:message key="table.actions"/></th>
            </tr>
            <tr>
                <th><fmt:message key="flights.economy"/></th>
                <th><fmt:message key="flights.business"/></th>
                <th><fmt:message key="flights.first"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${flights}" var="flight">
                <c:set var="flightId" value="${flight.getId()}"/>
                <c:set var="flightNumber" value="${flight.getNumber()}"/>
                <c:set var="originAirport" value="${flight.getOriginAirport()}"/>
                <c:set var="originCity" value="${originAirport.getCity()}"/>
                <c:set var="originAirportCode" value="${originAirport.getCode()}"/>
                <c:set var="originAirportName" value="${originAirport.getName()}"/>
                <c:set var="destinationAirport" value="${flight.getDestinationAirport()}"/>
                <c:set var="destinationCity" value="${destinationAirport.getCity()}"/>
                <c:set var="destinationAirportCode" value="${destinationAirport.getCode()}"/>
                <c:set var="destinationAirportName" value="${destinationAirport.getName()}"/>
                <c:set var="economyClassTicketsAmount" value="${flight.getEconomyClassTicketsAmount()}"/>
                <c:set var="businessClassTicketsAmount" value="${flight.getBusinessClassTicketsAmount()}"/>
                <c:set var="firtsClassTicketsAmount" value="${flight.getFirstClassTicketsAmount()}"/>
                <c:set var="isDeleted" value="${flight.getDeleted()}"/>
                <tr>
                    <td>${flightId}</td>
                    <td>${flightNumber}</td>
                    <td>${originAirportCode} (${originCity})</td>
                    <td>${destinationAirportCode} (${destinationCity})</td>
                    <td>${economyClassTicketsAmount}</td>
                    <td>${businessClassTicketsAmount}</td>
                    <td>${firtsClassTicketsAmount}</td>
                    <td>
                        <c:choose>
                            <c:when test="${isDeleted}"><fmt:message key="flights.status.deleted"/> </c:when>
                            <c:otherwise><fmt:message key="flights.status.active"/></c:otherwise>
                        </c:choose>
                    </td>
                    <c:choose>
                        <c:when test="${!isDeleted}">
                            <td><a href="#" title="<fmt:message key="action.delete"/>" name="button+"
                                   id="Delete${flightId}" onclick="onButtonClick(this)">
                                <i class="fas fa-times"></i></a></td>
                        </c:when>
                        <c:otherwise>
                            <td><a href="#" title="<fmt:message key="action.recover"/>" name="button+"
                                   id="Recover${flightId}" onclick="onButtonClick(this)">
                                <i class="fas fa-undo-alt"></i></a></td>
                        </c:otherwise>
                    </c:choose>
                    <td>
                        <a href="#" title="<fmt:message key="flights.timetable"/>" id="ShowTimetable${flightId}"
                           onclick="onButtonClick(this)"><i class="fas fa-table"></i></a>
                    </td>
                    <td>
                        <a href="#" title="<fmt:message key="flights.prices"/>" id="ShowFlightPrice${flightId}"
                           onclick="onButtonClick(this)"> <i class="fas fa-dollar-sign"></i></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="hidden" name="command" id="command" value="">
        <input type="hidden" name="flight_id" id="flight_id" value="">
    </form>
</div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/main.js"></script>
</body>
</html>
