<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Hello Web</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">
</head>
<body>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale" />
<div class="nav-line-container text-major">
    <div class="nav-line">
        <div class="nav-link">
            <a href="/"><fmt:message key="header.main"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="/login?command=showLogin"><fmt:message key="header.login"/></a>
        </div>
    </div>
</div>
<div class="main-container">
    <div class="login-header">
        <div class="login-header-text text-major"><fmt:message key="login.login"/></div>
    </div>
    <div class="login-container">
        <div class="login-body">
            <div class="login-input">
                <form method="post" action="/login?command=logIn">
                    <div class="new-flight-row">
                        <input class="user-text-field" type="text" placeholder="<fmt:message key="login.username"/>" name="username" required value="${requestScope.username}">
                    </div>
                    <div class="new-flight-row">
                    <input class="user-text-field" type="password" placeholder="<fmt:message key="login.password"/>" name="password" required value="">
                    </div>

                    <c:if test="${resultMessage !=null}">
                    <div class="new-flight-row">
                        <div class="result-message text-major width-auto ${resultMessage.getError() ? 'error' : 'no-error'}">${requestScope.resultMessage.getMessage()}</div>
                    </div>
                    </c:if>
                    <div class="new-flight-row">
                    <input class="button bg-color-green text-major" type="submit" value="<fmt:message key="login.log.in"/>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
</body>
</html>
