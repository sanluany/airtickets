<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale" />
<c:set var="user" value="${user}" />
<c:set var="role" value="${user.userRole}"/>
<div class="header">
    <div class="options">
        <a href="/"><fmt:message key="header.main" /></a>
        <c:if test="${user !=null}">
            <c:choose>
                <c:when test="${role == 'USER'}">
                    <a href="/account?command=showAccount"><fmt:message key="header.account" /></a>
                </c:when>
                <c:when test="${role == 'ADMIN'}">
                    <a href="/flights?command=showFlights"><fmt:message key="header.flights" /></a>
                </c:when>
            </c:choose>
        </c:if>
        <c:choose>
            <c:when test="${user != null}">
                <a href="/login?command=logOut"><fmt:message key="header.log.out" /></a>
            </c:when>
            <c:otherwise>
                <a href="/login?command=showLogin"><fmt:message key="header.log.in" /></a>
            </c:otherwise>
        </c:choose>
        <select class="language-list" id="language-list" onchange="changeLanguage()">
            <option value="ru" ${language == 'ru' ? 'selected' : ''}>Русский</option>
            <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
            <option value="by" ${language == 'by' ? 'selected' : ''}>Беларуская</option>
        </select>
    </div>
    <div class="main-image">

    </div>
</div>
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/main.js"></script>
</body>
</html>
