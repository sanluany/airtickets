<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Error page</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<div class="error-container">
    <div class="error-title">
        <fmt:message key="error.title"/>
    </div>
    <div class="error-message">
        <fmt:message key="error.message"/> <a href="/"><fmt:message key="error.main"/> </a>
    </div>
</div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
</body>
</html>
