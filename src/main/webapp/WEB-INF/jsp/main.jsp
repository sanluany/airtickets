<%@ page contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<html>
<head>
    <title>Welcome</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/css/ticket.css"/>
    <link rel="stylesheet" href="../../resources/css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<div class="service-line">
    <div class="search-box">
        <form method="post" action="/main?command=searchFlights">
            <div class="select-box">
                <label for="origin_city"><fmt:message key="main.from"/></label>
                <select name="origin_city" id="origin_city" class="select-box" required>
                    <option selected disabled hidden value=""><fmt:message key="select.choose"/></option>
                    <c:forEach items="${cities}" var="city">
                        <option value="${city.getId()}" ${requestScope.originCity == city.getId() ? 'selected' : ''} >${city}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="select-box">
                <label for="destination_city"><fmt:message key="main.to"/></label>
                <select name="destination_city" id="destination_city" class="select-box" required>
                    <option selected disabled hidden value=""><fmt:message key="select.choose"/></option>
                    <c:forEach items="${cities}" var="city">
                        <option value="${city.getId()}" ${requestScope.destinationCity == city.getId() ? 'selected' : ''} >${city}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="data-picker-box">
                <label for="departure_date"><fmt:message key="main.departure.date"/></label>
                <input type="date" name="departure_date" id="departure_date" required
                       value="${requestScope.departureDate}">
            </div>
            <div class="submit-box">
                <input type="submit" value="<fmt:message key="button.search"/>" class="button">
            </div>
        </form>
    </div>
</div>
<c:if test="${resultMessage !=null}">
    <div class="result-message text-major ${requestScope.resultMessage.getError() ? 'error' : 'no-error'}">${requestScope.resultMessage.getMessage()}</div>
</c:if>
<div class="tickets">
    <c:forEach var="offerDto" items="${requestScope.offerDtoListForRequest}" varStatus="loop">
        <c:set var="timetable" value="${offerDto.getTimetable()}"/>
        <c:set var="flight" value="${timetable.getFlight()}"/>
        <c:set var="flightNumber" value="${flight.getNumber()}"/>

        <c:set var="originAirport" value="${flight.getOriginAirport()}"/>
        <c:set var="originAirportCode" value="${originAirport.getCode()}"/>
        <c:set var="originCity" value="${originAirport.getCity()}"/>

        <c:set var="destinationAirport" value="${flight.getDestinationAirport()}"/>
        <c:set var="destinationAirportCode" value="${destinationAirport.getCode()}"/>
        <c:set var="destinationCity" value="${destinationAirport.getCity()}"/>


        <c:set var="departureDate" value="${timetable.getDepartureDate()}"/>
        <c:set var="arrivalDate" value="${timetable.getArrivalDate()}"/>

        <c:set var="flightPricesRemainedTicketsMap" value="${offerDto.getFlightPricesRemainedTicketsMap()}"/>

        <div class="ticket">
            <div class="ticket-main-info">
                <div class="ticket-airline-info text-center text-major text-major text-center">
                    <div>${flightNumber}</div>
                </div>
                <div class="ticket-flight-info">
                    <div class="ticket-place-and-time-info">
                        <div class="text-major">${originAirportCode}</div>
                        <div class="text-minor">
                            (${originCity})
                        </div>
                        <div class="text-normal"><custom:date patternType="DATE" temporal="${departureDate}"/></div>
                        <div class="text-major"><custom:date patternType="TIME" temporal="${departureDate}"/></div>
                    </div>
                    <i class="fas fa-long-arrow-alt-right"></i>
                    <div class="ticket-place-and-time-info">
                        <div class="text-major">${destinationAirportCode}</div>
                        <div class="text-minor">
                            (${destinationCity})
                        </div>
                        <div class="text-normal"><custom:date patternType="DATE" temporal="${arrivalDate}"/></div>
                        <div class="text-major"><custom:date patternType="TIME" temporal="${arrivalDate}"/></div>
                    </div>
                </div>
            </div>

            <c:set var="soldOut" value="true"/>

            <div class="ticket-offers">
                <c:forEach items="${flightPricesRemainedTicketsMap}" var="entry">
                    <c:set var="flightPrice" value="${entry.key}"/>
                    <c:set var="price" value="${flightPrice.getPrice()}"/>
                    <c:set var="travelClass" value="${flightPrice.getTravelClass()}"/>
                    <c:set var="remainedTicketsAmount" value="${entry.value}"/>
                    <div class="ticket-offer">
                        <div class="ticket-info">
                            <div class="ticket-travel-class">
                                <c:choose>
                                    <c:when test="${travelClass == 'ECONOMY'}">
                                        <fmt:message key="travel.class.economy"/>
                                        <fmt:message key="class"/>
                                    </c:when>
                                    <c:when test="${travelClass == 'BUSINESS'}">
                                        <fmt:message key="travel.class.business"/>
                                        <fmt:message key="class"/>
                                    </c:when>
                                    <c:when test="${travelClass == 'FIRST'}">
                                        <fmt:message key="travel.class.first"/>
                                        <fmt:message key="class"/>
                                    </c:when>
                                </c:choose>
                            </div>
                            <c:if test="${remainedTicketsAmount > 0}">
                                <c:set var="soldOut" value="false"/>
                            </c:if>
                            <c:if test="${remainedTicketsAmount<=5 && remainedTicketsAmount >0}">
                                <div class="text-minor color-red">( <fmt:message key="main.tickets.remaining"/>
                                    : ${remainedTicketsAmount})
                                </div>
                            </c:if>
                            <c:if test="${remainedTicketsAmount == 0}">
                                <div class="text-minor">( <fmt:message key="main.ticket.unavailable"/> )</div>
                            </c:if>
                        </div>
                        <c:if test="${remainedTicketsAmount == 0}">
                            <div class="ticket-price bg-color-grey">${price}</div>
                        </c:if>
                        <c:if test="${remainedTicketsAmount > 0}">
                            <div class="ticket-price">${price}</div>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="ticket-order">
                    <c:choose>
                        <c:when test="${soldOut == 'false'}">
                            <input type="submit" value=" <fmt:message key="button.order"/> " class="button"
                                   id="offer${loop.index}"
                                   onclick="onOfferSelection(this)">
                        </c:when>
                        <c:otherwise>
                            <input type="button" value=" <fmt:message key="button.sold.out"/> "
                                   class="button bg-color-grey" disabled>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </c:forEach>
</div>

<div class="modal" id="login-modal">
    <div class="modal-window modal-content width-400">
        <div class="modal-controls text-major">
            <span><fmt:message key="login.login"/></span>
            <a href="#" title="<fmt:message key="action.close"/>" onclick="hideCurrentModalWindow()">
                <i class="fas fa-times"></i></a>
        </div>
        <div class="login-container">
            <div class="login-body">
                <div class="login-input">
                    <form method="post" action="/login?command=logIn" id="loginModalForm">
                        <div class="new-flight-row">
                            <input class="user-text-field" type="text"
                                   placeholder="<fmt:message key="login.username"/>" name="username" required>
                        </div>
                        <div class="new-flight-row">
                            <input class="user-text-field" type="password"
                                   placeholder="<fmt:message key="login.password"/>" name="password" required>
                        </div>
                        <div class="new-flight-row">
                            <input class="button bg-color-green text-major" type="submit"
                                   value="<fmt:message key="login.log.in"/>">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
<input type="hidden" name="isLoggedIn" id="isLoggedIn" value="${sessionScope.user != null}">
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/main.js"></script>
<script src="../../resources/js/modal.js"></script>
</body>
</html>
