<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Flight price page</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" href="../../resources/css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">

</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<c:set var="flight" value="${flight}"/>
<c:set var="flightId" value="${flight.getId()}"/>
<c:set var="number" value="${flight.getNumber()}"/>

<div class="nav-line-container text-major">
    <div class="nav-line">
        <div class="nav-link">
            <a href="/"><fmt:message key="header.main"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="/flights?command=showFlights"><fmt:message key="header.flights"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="#"><fmt:message key="flightprices.flight.prices"/> ${number}</a>
        </div>
    </div>
</div>
<div class="new-flight-container">
    <div class="text-major text-center">
        <fmt:message key="flightprices.add.prices"/>
    </div>
    <form method="post" action="/flightprice?command=addFlightPrice&flight_id=${flightId}">
        <div class="new-flight-row">
            <div class="new-flight-column">
                <label for="start_date"><fmt:message key="flightprices.price.start.date"/> </label>
                <input type="date" name="start_date" id="start_date" required value="${requestScope.startDate}">
            </div>
            <div class="new-flight-column">
                <label for="end_date"><fmt:message key="flightprices.price.end.date"/> </label>
                <input type="date" name="end_date" id="end_date" required value="${requestScope.endDate}">
            </div>
        </div>
        <div class="new-flight-row">
            <div class="new-flight-column">
                <label for="travel_class"><fmt:message key="flightprices.travel.class"/> </label>
                <select id="travel_class" name="travel_class">
                    <c:forEach items="${travelClasses}" var="travelClass">
                        <option value="${travelClass}"  ${requestScope.travelClass == travelClass ? 'selected' : ''} >
                            <c:choose>
                                <c:when test="${travelClass == 'ECONOMY'}">
                                    <fmt:message key="travel.class.economy"/>
                                </c:when>
                                <c:when test="${travelClass == 'BUSINESS'}">
                                    <fmt:message key="travel.class.business"/>
                                </c:when>
                                <c:when test="${travelClass == 'FIRST'}">
                                    <fmt:message key="travel.class.first"/>
                                </c:when>
                            </c:choose>
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="new-flight-column">
                <label for="price"><fmt:message key="flightprices.price"/> :</label>
                <input type="number" name="price" id="price" required min="1" value="${requestScope.price}">
            </div>
        </div>
        <div class="new-flight-row block">
            <div class="text-center">
                <input type="submit" class="button bg-color-green" value="<fmt:message key="button.add"/> ">
            </div>
        </div>
    </form>
</div>
<c:if test="${resultMessage !=null}">
    <div class="result-message text-major ${resultMessage.getError() ? 'error' : 'no-error'}">${resultMessage.getMessage()}</div>
</c:if>
<div class="table">
    <form method="post" onsubmit="onSubmitFlightPriceActionForm(this)" id="action_form">
        <table cellspacing="0">
            <caption><fmt:message key="flightprices.flight.prices"/> ${number}</caption>
            <thead>
            <tr>
                <th>ID</th>
                <th><fmt:message key="flightprices.travel.class"/></th>
                <th><fmt:message key="flightprices.price"/></th>
                <th><fmt:message key="flightprices.price.start.date"/></th>
                <th><fmt:message key="flightprices.price.end.date"/></th>
                <th><fmt:message key="table.status"/></th>
                <th><fmt:message key="table.action"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${flightPrices}" var="flightPrice">
                <c:set var="flightPriceId" value="${flightPrice.getId()}"/>
                <c:set var="flightPriceTravelClass" value="${flightPrice.getTravelClass()}"/>
                <c:set var="price" value="${flightPrice.getPrice()}"/>
                <c:set var="startDate" value="${flightPrice.getStartDate()}"/>
                <c:set var="endDate" value="${flightPrice.getEndDate()}"/>
                <c:set var="isDeleted" value="${flightPrice.getDeleted()}"/>
                <tr>
                    <td>${flightPriceId}</td>
                    <td>${flightPriceTravelClass}</td>
                    <td>${price}</td>
                    <td><custom:date temporal="${startDate}"
                                     patternType="DATE"/>
                    </td>
                    <td><custom:date temporal="${endDate}"
                                     patternType="DATE"/>
                    </td>
                    <td><c:choose>
                        <c:when test="${isDeleted}"><fmt:message
                                key="flightprices.status.deleted"/></c:when>
                        <c:otherwise><fmt:message key="flightprices.status.active"/></c:otherwise>
                    </c:choose>
                    </td>
                    <c:choose>
                        <c:when test="${!isDeleted}">
                            <td><a href="#" title="<fmt:message key="action.delete"/>" name="button+"
                                   id="Delete${flightPriceId}"
                                   onclick="onButtonClick(this)">
                                <i class="fas fa-times"></i></a></td>
                        </c:when>
                        <c:otherwise>
                            <td><a href="#" title="<fmt:message key="action.recover"/>" name="button+"
                                   id="Recover${flightPriceId}"
                                   onclick="onButtonClick(this)">
                                <i class="fas fa-undo-alt"></i></a></td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <input type="hidden" name="flight_id" id="flight_id" value="${flightId}">
    </form>
</div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/main.js"></script>
</body>
</html>
