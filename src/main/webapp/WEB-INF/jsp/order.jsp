<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<html>
<head>
    <title>Order</title>
    <link rel="stylesheet" href="../../resources/css/style.css">
    <link rel="stylesheet" href="../../resources/css/modal.css">
    <link rel="stylesheet" href="../../resources/css/ticket.css">
    <link rel="stylesheet" href="../../resources/css/fontawesome-all.css">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>

<c:set var="timetable" value="${offerDto.getTimetable()}"/>
<c:set var="flight" value="${timetable.getFlight()}"/>
<c:set var="flightNumber" value="${flight.getNumber()}"/>

<c:set var="originAirport" value="${flight.getOriginAirport()}"/>
<c:set var="originCity" value="${originAirport.getCity()}"/>
<c:set var="originAirportCode" value="${originAirport.getCode()}"/>
<c:set var="originAirportName" value="${originAirport.getName()}"/>
<c:set var="destinationAirport" value="${flight.getDestinationAirport()}"/>
<c:set var="destinationCity" value="${destinationAirport.getCity()}"/>
<c:set var="destinationAirportCode" value="${destinationAirport.getCode()}"/>
<c:set var="destinationAirportName" value="${destinationAirport.getName()}"/>

<c:set var="departureDate" value="${timetable.getDepartureDate()}"/>
<c:set var="arrivalDate" value="${timetable.getArrivalDate()}"/>

<div class="nav-line-container text-major">
    <div class="nav-line">
        <div class="nav-link">
            <a href="/"><fmt:message key="header.main"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="#"><fmt:message key="header.ordering"/> ${flightNumber}</a>
        </div>
    </div>
</div>
<div class="flight-info-box">
    <div class="flight-info ">
        <div class="text-center text-major">
            <fmt:message key="order.flight"/> ${flightNumber}
        </div>
        <div class="new-flight-row order-info">
            <div class="new-flight-column">
                <div class="text-major"><fmt:message key="order.info.departure"/></div>

                <div>
                    <fmt:message
                            key="order.airport.departure"/>: ${originAirportCode}
                    (${originCity})
                </div>
                <div>
                    <fmt:message key="order.date.departure"/>: <custom:date
                        temporal="${departureDate}"
                        patternType="DATE"/>
                </div>
                <div>
                    <fmt:message key="order.time.departure"/>: <custom:date
                        temporal="${departureDate}"
                        patternType="TIME"/>
                </div>
            </div>
            <div class="new-flight-column">
                <div class="text-center text-major">
                    <fmt:message key="order.info.arrival"/>
                </div>
                <div>
                    <fmt:message
                            key="order.airport.arrival"/>: ${destinationAirportCode}
                    (${destinationCity})
                </div>
                <div>
                    <fmt:message key="order.date.arrival"/> <custom:date
                        temporal="${arrivalDate}"
                        patternType="DATE"/>
                </div>
                <div>
                    <fmt:message key="order.time.arrival"/>: <custom:date
                        temporal="${arrivalDate}"
                        patternType="TIME"/>
                </div>
            </div>
        </div>

    </div>
</div>
<c:if test="${resultMessage !=null}">
    <div class="result-message text-major width-630 ${resultMessage.getError() ? 'error' : 'no-error'}">${requestScope.resultMessage.getMessage()}</div>
</c:if>
<c:choose>
    <c:when test="${requestScope.amountOfTickets != null}">
        <c:set var="amount" value="${requestScope.amountOfTickets}"/>
    </c:when>
    <c:otherwise>
        <c:set var="amount" value="1"/>
    </c:otherwise>
</c:choose>
<div class="passenger-amount-box">
    <div class="passenger-amount">
        <span class="text-major amount-control"><fmt:message key="order.passengers.amount"/></span>
        <span class="amount-control">
         <button class="square-button" onclick="removeTicket()">-</button>
        <span class="text-major" id="passenger_amount">${amount}</span>
        <button class="square-button" onclick="addTicket()">+</button>
    </span>
    </div>
</div>
<c:set var="flightPricesRemainedTicketsMap" value="${offerDto.getFlightPricesRemainedTicketsMap()}"/>
<form method="post" action="/order?command=makeOrder">
    <div class="tickets" id="tickets">
        <c:forEach begin="0" end="${amount-1}" varStatus="loop">
            <div class="ticket" id="ticket${loop.index}">
                <div class="ticket-main-info">
                    <div class="passenger-info">
                        <div class="passenger-detail">
                            <label for="passenger_first_name${loop.index}"><fmt:message key="order.first.name"/></label>
                            <input type="text" id="passenger_first_name${loop.index}" name="passenger_first_name"
                                   required
                                   value="${requestScope.passengerFirstNameArray[loop.index]}">
                        </div>
                        <div class="passenger-detail">
                            <label for="passenger_last_name${loop.index}"><fmt:message key="order.last.name"/></label>
                            <input type="text" id="passenger_last_name${loop.index}" name="passenger_last_name" required
                                   value="${requestScope.passengerLastNameArray[loop.index]}">
                        </div>
                        <div class="passenger-detail">
                            <label for="passenger_passport_number${loop.index}"><fmt:message
                                    key="order.passport.number"/></label>
                            <input type="text" id="passenger_passport_number${loop.index}"
                                   name="passenger_passport_number" required
                                   value="${requestScope.passengerPassportNumberArray[loop.index]}">
                        </div>
                        <div class="passenger-detail">
                            <label for="passenger_date_of_birth${loop.index}"><fmt:message
                                    key="order.date.birth"/></label>
                            <input type="date" id="passenger_date_of_birth${loop.index}" name="passenger_date_of_birth"
                                   required
                                   value="${requestScope.passengerDateOfBirthArray[loop.index]}">
                        </div>
                    </div>
                </div>
                <div class="ticket-order-info">
                    <div class="ticket-order-travel-class">
                        <label for="ticket_travel_class${loop.index}" class="text-major"><fmt:message
                                key="order.choose.class"/></label>
                        <select id="ticket_travel_class${loop.index}" name="ticket_travel_class"
                                onchange="onTravelClassChange(this)" required>
                            <option selected disabled hidden value=""><fmt:message key="select.choose"/></option>

                            <c:forEach items="${flightPricesRemainedTicketsMap}" var="entry">
                                <c:set var="flightPrice" value="${entry.key}"/>
                                <c:set var="remainingTicketsAmount" value="${entry.value}"/>
                                <c:set var="travelClass" value="${flightPrice.getTravelClass()}"/>
                                <c:if test="${remainingTicketsAmount > 0}">
                                    <c:choose>
                                        <c:when test="${requestScope.ticketTravelClassArray[loop.index] == travelClass }">
                                            <option value="${travelClass}" selected>${travelClass}</option>
                                            <c:set var="price" value="${flightPrice.getPrice()}"/>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${travelClass}">
                                                <c:choose>
                                                    <c:when test="${travelClass == 'ECONOMY'}">
                                                        <fmt:message key="travel.class.economy"/>
                                                    </c:when>
                                                    <c:when test="${travelClass == 'BUSINESS'}">
                                                        <fmt:message key="travel.class.business"/>
                                                    </c:when>
                                                    <c:when test="${travelClass == 'FIRST'}">
                                                        <fmt:message key="travel.class.first"/>
                                                    </c:when>
                                                </c:choose>
                                            </option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="ticket-order-price ">
                        <span><fmt:message key="order.price"/></span>
                        <span id="ticket_price${loop.index}">${price}</span>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <div class="order-info-box">
        <div>
            <span class="text-major"><fmt:message key="order.total"/>:</span>
            <span class="text-major" id="total_price"></span>
            <span class="text-minor">(<fmt:message key="order.available"/> ${user.getBalance()})</span>
        </div>
        <input type="hidden" name="offer_id" value="${offerDto.getId()}">
        <input type="submit" class="button bg-color-red" value="<fmt:message key="button.pay"/>">
    </div>
    <input type="hidden" name="amount_of_tickets" id="amount_of_tickets" value="1">
</form>
<c:forEach items="${flightPricesRemainedTicketsMap}" var="entry">
    <c:set var="flightPrice" value="${entry.key}"/>
    <c:set var="price" value="${flightPrice.getPrice()}"/>
    <c:set var="travelClass" value="${flightPrice.getTravelClass()}"/>
    <c:choose>
        <c:when test="${travelClass == 'ECONOMY'}">
            <input type="hidden" id="economy_class_price_holder" value="${price}">
        </c:when>
        <c:when test="${travelClass == 'BUSINESS'}">
            <input type="hidden" id="business_class_price_holder" value="${price}">
        </c:when>
        <c:when test="${travelClass == 'FIRST'}">
            <input type="hidden" id="first_class_price_holder" value="${price}">
        </c:when>
    </c:choose>
</c:forEach>
<div class="ticket-main-info hidden" id="ticket_template">
    <div class="ticket-main-info">
        <div class="passenger-info">
            <div class="passenger-detail">
                <label for="passenger_first_name${loop.index}"><fmt:message key="order.first.name"/></label>
                <input type="text" id="passenger_first_name${loop.index}" name="passenger_first_name"
                       class="passenger-first-name"
                       required
                       value="${requestScope.passengerFirstNameArray[loop.index]}">
            </div>
            <div class="passenger-detail">
                <label for="passenger_last_name${loop.index}"><fmt:message key="order.last.name"/></label>
                <input type="text" id="passenger_last_name${loop.index}" name="passenger_last_name"
                       class="passenger-last-name"
                       required
                       value="${requestScope.passengerLastNameArray[loop.index]}">
            </div>
            <div class="passenger-detail">
                <label for="passenger_passport_number${loop.index}"><fmt:message
                        key="order.passport.number"/></label>
                <input type="text" id="passenger_passport_number${loop.index}"
                       name="passenger_passport_number" class="passenger-passport-number" required
                       value="${requestScope.passengerPassportNumberArray[loop.index]}">
            </div>
            <div class="passenger-detail">
                <label for="passenger_date_of_birth${loop.index}"><fmt:message
                        key="order.date.birth"/></label>
                <input type="date" id="passenger_date_of_birth${loop.index}" name="passenger_date_of_birth"
                       class="passenger-date-of-birth"
                       required
                       value="${requestScope.passengerDateOfBirthArray[loop.index]}">
            </div>
        </div>
    </div>
    <div class="ticket-order-info">
        <div class="ticket-order-travel-class">
            <label for="ticket_travel_class${loop.index}" class="text-major"><fmt:message
                    key="order.choose.class"/></label>
            <select id="ticket_travel_class${loop.index}" name="ticket_travel_class" class="travel-class"
                    onchange="onTravelClassChange(this)" required>
                <option selected disabled hidden value=""><fmt:message key="select.choose"/></option>

                <c:forEach items="${flightPricesRemainedTicketsMap}" var="entry">
                    <c:set var="flightPrice" value="${entry.key}"/>
                    <c:set var="remainingTicketsAmount" value="${entry.value}"/>
                    <c:set var="travelClass" value="${flightPrice.getTravelClass()}"/>
                    <c:if test="${remainingTicketsAmount > 0}">
                        <c:choose>
                            <c:when test="${requestScope.ticketTravelClassArray[loop.index] == travelClass }">
                                <option value="${travelClass}" selected>${travelClass}</option>
                                <c:set var="price" value="${flightPrice.getPrice()}"/>
                            </c:when>
                            <c:otherwise>
                                <option value="${travelClass}">${travelClass}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:forEach>
            </select>
        </div>
        <div class="ticket-order-price ">
            <span><fmt:message key="order.price"/></span>
            <span class="price" id="ticket_price"></span>
        </div>
    </div>
</div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
<input type="hidden" value="${amount}" id="current_tickets_index">
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/order.js"></script>
</body>
</html>
