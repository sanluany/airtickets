<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>
<html>
<head>
    <title>Account</title>
    <link rel="stylesheet" type="text/css" href="../../resources/css/style.css"/>
    <link rel="stylesheet" href="../../resources/css/modal.css">
    <link rel="stylesheet" type="text/css" href="../../resources/css/fontawesome-all.css">
</head>
<body>
<c:set var="language" value="${not empty cookie.locale.value  ? cookie.locale.value  : 'en'}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="locale"/>
<div id="header">
    <jsp:include page="fragments/header.jsp"/>
</div>
<div class="nav-line-container text-major">
    <div class="nav-line">
        <div class="nav-link">
            <a href="/"><fmt:message key="header.main"/></a>
        </div>
        <div class="nav-delimiter">
            <i class="fas fa-angle-right"></i>
        </div>
        <div class="nav-link">
            <a href="#"><fmt:message key="header.account"/></a>
        </div>
    </div>
</div>
<c:set var="user" value="${sessionScope.user}"/>
<c:set var="username" value="${user.getUsername()}"/>
<c:set var="balance" value="${user.getBalance()}"/>
<c:if test="${resultMessage !=null}">
    <div class="result-message text-major width-630 ${resultMessage.getError() ? 'error' : 'no-error'}">${requestScope.resultMessage.getMessage()}</div>
</c:if>
<div class="account-info-box">
    <div class="account-header">
        <div class="account-username">
            <span><fmt:message key="account.username"/> :</span>
            <span>${username}</span>
        </div>
        <div class="account-balance">
            <span><fmt:message key="account.balance"/>:</span>
            <span>${balance}</span>
        </div>
    </div>
    <div class="table">
        <table cellspacing="0">
            <caption><fmt:message key="account.previous.orders"/></caption>
            <thead>
            <tr>
                <th>ID</th>
                <th><fmt:message key="account.order.date"/></th>
                <th><fmt:message key="account.flight"/></th>
                <th><fmt:message key="account.total"/></th>
                <th colspan="2"><fmt:message key="account.more.info"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orderTicketsMap}" var="orderTicketsEntry" varStatus="loop">
                <c:set var="order" value="${orderTicketsEntry.getKey()}"/>
                <c:set var="orderId" value="${order.getId()}"/>
                <c:set var="orderDate" value="${order.getOrderDate()}"/>
                <c:set var="tickets" value="${orderTicketsEntry.getValue()}"/>
                <c:set var="ticket" value="${tickets.get(0)}"/>
                <c:set var="timetable" value="${ticket.getTimetable()}"/>
                <c:set var="flight" value="${timetable.getFlight()}"/>
                <c:set var="flightNumber" value="${flight.getNumber()}"/>
                <tr>
                    <td>${orderId}</td>
                    <td><custom:date temporal="${orderDate}"
                                     patternType="DATE"/>
                    </td>
                    <td> ${flightNumber} </td>
                    <td> ${orderTotalMap.get(order)} </td>
                    <td>
                        <a href="#" title="<fmt:message key="account.flight"/>" name="button+"
                           onclick="showModalById('flight-modal${loop.index}')">
                            <i class="fas fa-info-circle"></i></a>
                    </td>
                    <td>
                        <a href="#" title="<fmt:message key="account.passengers"/>" name="button+"
                           onclick="showModalById('passenger-modal${loop.index}')">
                            <i class="fas fa-user"></i></a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <c:forEach items="${orderTicketsMap}" var="orderTicketsEntry" varStatus="loop">
        <c:set var="order" value="${orderTicketsEntry.getKey()}"/>
        <c:set var="orderId" value="${order.getId()}"/>
        <c:set var="orderDate" value="${order.getOrderDate()}"/>
        <c:set var="tickets" value="${orderTicketsEntry.getValue()}"/>
        <c:set var="ticket" value="${tickets.get(0)}"/>
        <c:set var="timetable" value="${ticket.getTimetable()}"/>
        <c:set var="flight" value="${timetable.getFlight()}"/>
        <c:set var="flightNumber" value="${flight.getNumber()}"/>
        <c:set var="originAirport" value="${flight.getOriginAirport()}"/>
        <c:set var="originAirportCode" value="${originAirport.getCode()}"/>
        <c:set var="originAirportName" value="${originAirport.getName()}"/>
        <c:set var="destinationAirport" value="${flight.getDestinationAirport()}"/>
        <c:set var="destinationAirportCode" value="${destinationAirport.getCode()}"/>
        <c:set var="destinationAirportName" value="${destinationAirport.getName()}"/>
        <c:set var="departureDate" value="${timetable.getDepartureDate()}"/>
        <c:set var="arrivalDate" value="${timetable.getArrivalDate()}"/>
        <div class="modal" id="flight-modal${loop.index}">
            <div class="modal-window modal-content">
                <div class="modal-controls text-major">
                    <span><fmt:message key="flights.flight"/> ${flightNumber}</span>
                    <a href="#" title="<fmt:message key="action.close"/>" onclick="hideCurrentModalWindow()">
                        <i class="fas fa-times"></i></a>
                </div>
                <div class="modal-window-content">
                    <div class="table no-margin">
                        <table cellspacing="0">
                            <caption><fmt:message key="account.main.info"/></caption>
                            <thead>
                            <tr>
                                <th><fmt:message key="account.departure.info"/></th>
                                <th><fmt:message key="account.arrival.info"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><fmt:message
                                        key="account.departure.airport"/>: ${originAirportCode}
                                    <span class="hidden-info">
                                        <i class="fas fa-question-circle"
                                           title="${originAirportName}"></i>
                                    </span></td>
                                <td><fmt:message
                                        key="account.arrival.airport"/>: ${destinationAirportCode}
                                    <span class="hidden-info">
                                        <i class="fas fa-question-circle"
                                           title="${destinationAirportName}"></i>
                                    </span></td>
                            </tr>
                            <tr>
                                <td><fmt:message key="account.departure.date"/>: <custom:date
                                        temporal="${departureDate}"
                                        patternType="DATE"/>
                                </td>
                                <td><fmt:message key="account.arrival.date"/>: <custom:date
                                        temporal="${arrivalDate}"
                                        patternType="DATE"/>
                                </td>
                            </tr>
                            <tr>
                                <td><fmt:message key="account.departure.time"/>: <custom:date
                                        temporal="${departureDate}"
                                        patternType="TIME"/>
                                </td>
                                <td><fmt:message key="account.arrival.time"/>: <custom:date
                                        temporal="${arrivalDate}"
                                        patternType="TIME"/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="passenger-modal${loop.index}">
            <div class="modal-window modal-content">
                <div class="modal-controls text-major">
                    <span><fmt:message key="flights.flight"/>  ${flightNumber}</span>
                    <a href="#" title="<fmt:message key="action.close"/>" onclick="hideCurrentModalWindow()">
                        <i class="fas fa-times"></i></a>
                </div>
                <div class="modal-window-content">
                    <div class="table no-margin">
                        <table cellspacing="0">
                            <caption><fmt:message key="account.passengers"/></caption>
                            <thead>
                            <tr>
                                <th><fmt:message key="account.first.name"/></th>
                                <th><fmt:message key="account.last.name"/></th>
                                <th><fmt:message key="account.passport.number"/></th>
                                <th><fmt:message key="account.date.birth"/></th>
                                <th><fmt:message key="account.travel.class"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${orderTicketsEntry.getValue()}" var="ticket">
                                <c:set var="passengerFirstName" value="${ticket.getPassengerFirstName()}"/>
                                <c:set var="passengerLastName" value="${ticket.getPassengerLastName()}"/>
                                <c:set var="passengerPassportNumber" value="${ticket.getPassengerPassportNumber()}"/>
                                <c:set var="passengerDateOfBirth" value="${ticket.getPassengerDateOfBirth()}"/>
                                <c:set var="travelClass" value="${ticket.getTravelClass()}"/>
                                <tr>
                                    <td>${passengerFirstName}</td>
                                    <td>${passengerLastName}</td>
                                    <td>${passengerPassportNumber}</td>
                                    <td><custom:date temporal="${passengerDateOfBirth}"
                                                     patternType="DATE"/>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${travelClass == 'ECONOMY'}">
                                                <fmt:message key="travel.class.economy"/>
                                            </c:when>
                                            <c:when test="${travelClass == 'BUSINESS'}">
                                                <fmt:message key="travel.class.business"/>
                                            </c:when>
                                            <c:when test="${travelClass == 'FIRST'}">
                                                <fmt:message key="travel.class.first"/>
                                            </c:when>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
<div class="overlay"></div>
<div id="footer">
    <jsp:include page="fragments/footer.jsp"/>
</div>
<script src="../../resources/js/jquery-3.3.1.js"></script>
<script src="../../resources/js/modal.js"></script>
</body>
</html>
