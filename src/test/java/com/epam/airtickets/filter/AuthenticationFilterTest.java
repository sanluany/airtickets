package com.epam.airtickets.filter;

import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.entity.user.UserRole;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class AuthenticationFilterTest {

    private final static String USER = "user";
    private final static String DEFAULT_PAGE = "/";
    private final static String TIMETABLE_PAGE = "/timetable";

    private final FilterChain filterChain = mock(FilterChain.class);
    private final HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    private final HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);

    @Before
    public void beforeEachTest(){
        when(httpServletRequest.getRequestURI()).thenReturn(TIMETABLE_PAGE);
    }

    @Test
    public void shouldReturnDefaultPageWhenUserNotAuthenticatedAndAuthenticationRequired() throws IOException, ServletException {
        Filter authenticationFilter = new AuthenticationFilter();
        authenticationFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(httpServletResponse).sendRedirect(DEFAULT_PAGE);
    }

    @Test
    public void shouldReturnTimetablePageWhenAdminIsAuthenticatedAndAuthenticationRequired() throws IOException, ServletException {
        User admin = mock(User.class);
        when(admin.getUserRole()).thenReturn(UserRole.ADMIN);

        HttpSession session = mock(HttpSession.class);
        when(httpServletRequest.getSession()).thenReturn(session);
        when(session.getAttribute(USER)).thenReturn(admin);

        Filter authenticationFilter = new AuthenticationFilter();
        authenticationFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
        verify(filterChain).doFilter(httpServletRequest,httpServletResponse);
    }

}