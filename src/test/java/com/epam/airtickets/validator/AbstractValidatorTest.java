package com.epam.airtickets.validator;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;


public class AbstractValidatorTest {

    private final static String EMPTY_STRING = "";
    private final static String NOT_EMPTY_STRING = "string";
    private final static String INVALID_DATE = "20--03-2018";
    private final static String VALID_DATE = "2018-05-05";
    private final static String INVALID_TIME = "20::50";
    private final static String VALID_TIME = "18:45";
    private final static String VALID_INTEGER= "1";
    private final static String INVALID_INTEGER= ".45";

    private final AbstractValidator abstractValidator = Mockito.spy(AbstractValidator.class);
    private final ValidationError validationErrorMock = Mockito.mock(ValidationError.class);

    @Test
    public void shouldBeFalseWhenNotEmptyString(){
        boolean result = abstractValidator.isNullOrEmpty(NOT_EMPTY_STRING,validationErrorMock);
        Assert.assertEquals(false,result);
    }

    @Test
    public void shouldBeTrueWhenEmptyString(){
        boolean result = abstractValidator.isNullOrEmpty(EMPTY_STRING,validationErrorMock);
        Assert.assertEquals(true,result);
    }

    @Test
    public void shouldReturnFalseWhenInvalidDate() {
        boolean result = abstractValidator.isValidDateParameter(INVALID_DATE,validationErrorMock);
        Assert.assertEquals(false,result);
    }

    @Test
    public void shouldReturnTrueWhenValidDate() {
        boolean result = abstractValidator.isValidDateParameter(VALID_DATE,validationErrorMock);
        Assert.assertEquals(true,result);
    }

    @Test
    public void shouldReturnFalseWhenInvalidTime() {
        boolean result = abstractValidator.isValidTimeParameter(INVALID_TIME,validationErrorMock);
        Assert.assertEquals(false,result);
    }

    @Test
    public void shouldReturnTrueWhenValidTime() {
        boolean result = abstractValidator.isValidTimeParameter(VALID_TIME,validationErrorMock);
        Assert.assertEquals(true,result);
    }

    @Test
    public void shouldReturnFalseWhenInvalidInteger(){
        boolean result = abstractValidator.isValidIntegerParameter(INVALID_INTEGER,validationErrorMock);
        Assert.assertEquals(false,result);
    }

    @Test
    public void shouldReturnTrueWhenValidInteger(){
        boolean result = abstractValidator.isValidIntegerParameter(VALID_INTEGER,validationErrorMock);
        Assert.assertEquals(true,result);
    }



}