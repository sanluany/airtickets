package com.epam.airtickets.service.user;

import com.epam.airtickets.entity.user.User;
import com.epam.airtickets.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.omg.CORBA.Any;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UserServiceImplTest {

    private final static String USERNAME = "user";
    private final static String VALID_PASSWORD = "password";
    private final static String INVALID_PASSWORD = "wrong";

    private static UserService userServiceMock = Mockito.spy(UserServiceImpl.class);
    private static User userMock = Mockito.mock(User.class);

    @BeforeClass
    public static void initialization() throws NoSuchAlgorithmException, ServiceException {
        String hashedPassword = hashPassword(VALID_PASSWORD);
        Mockito.when(userMock.getPassword()).thenReturn(hashedPassword);
        Mockito.when(userServiceMock.getUserByUsername(Mockito.anyString())).thenReturn(userMock);
    }

    @Test
    public void shouldReturnNullWhenInvalidPassword() throws ServiceException {
        User user = userServiceMock.logIn(USERNAME,INVALID_PASSWORD);
        Assert.assertNull(user);
    }

    @Test
    public void shouldReturnNotNullWhenValidPassword() throws ServiceException{
        User user = userServiceMock.logIn(USERNAME,VALID_PASSWORD);
        Assert.assertNotNull(user);
    }

    private static String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest);
    }
}